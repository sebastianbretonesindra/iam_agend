require 'rails_helper'

RSpec.describe CreateAdminService, type: :service do

  before(:each) do
    user = create(:user, :admin)
  end

  let(:service) {CreateAdminService.new}

  describe "CALL" do
    it "method CALL" do
      allow(Rails.application.secrets).to receive(:admin_email).and_return('admin@madrid.es')
      allow(Rails.application.secrets).to receive(:admin_password).and_return('12345678')
      expect(service.call).not_to eq(nil)
    end
  end

end