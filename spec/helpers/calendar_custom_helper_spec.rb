require 'rails_helper'

RSpec.describe CalendarCustomHelper, type: :helper do
    it "get_week_first_monday" do   
        expect(helper.get_week_first_monday).not_to eq(nil)      
    end

    it "get_week_first_sunday" do   
        expect(helper.get_week_first_sunday).not_to eq(nil)      
    end

    it "get_week_first_monday_with_hour" do   
        expect(helper.get_week_first_monday_with_hour).not_to eq(nil)      
    end

    

    it "get_out_day" do  
        expect(helper.send(:get_out_day)).not_to eq(nil)      
    end

    
end