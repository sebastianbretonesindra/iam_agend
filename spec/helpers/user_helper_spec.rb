require 'rails_helper'

RSpec.describe UserHelper, type: :helper do
    let!(:user)   { create(:user) }
    let!(:lobby)   { create(:user, :lobby) }

    it "user_by_role" do
      expect(helper.user_by_role(user)).to eq("gestor de agenda")
    end

    it "user_by_role Lobby" do
        expect(helper.user_by_role(lobby)).to eq("Lobby")
    end
end