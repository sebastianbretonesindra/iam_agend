require 'rails_helper'

RSpec.describe OrganizationsHelper, type: :helper do
    let!(:organization) { create(:organization, user: create(:user,:lobby) )}
    let!(:lobbies) { Organization.all }   
    let!(:user) { create(:user)}
    let!(:admin) { create(:user,:admin)}

    it "organizations_index_subtitle" do
        expect(helper.organizations_index_subtitle).not_to eq("")
        params[:order] = '1'

        expect(helper.organizations_index_subtitle).not_to eq("")
    end

    it "organization_represented_entities_url_pattern" do
        expect(helper.organization_represented_entities_url_pattern(:html)).not_to eq("")
    end

    it "organization_agents_url_pattern" do
        expect(helper.organization_agents_url_pattern(:html)).not_to eq("")
    end

    it "organization_category_url_pattern" do
        expect(helper.organization_category_url_pattern).not_to eq(0)
    end

    it "protocol_for_urls" do
        expect(helper.protocol_for_urls).to eq(:http)
    end

    it "search_by_filter?" do
        expect(helper.search_by_filter?).to eq(false)
    end

    it "organization_status" do
        allow(view).to receive(:current_user).and_return(admin)   
        expect(helper.organization_status(organization)).not_to eq(nil)

        organization.renovation_date = Time.zone.now - 2.years + 1.day
        organization.save

        expect(helper.organization_status(organization)).not_to eq(nil)

        organization.invalidated_at = Time.zone.now - 3.years 
        organization.save

        expect(helper.organization_status(organization)).not_to eq(nil)

        allow(view).to receive(:current_user).and_return(user)   
        expect(helper.organization_status(organization)).not_to eq(nil)

        organization.canceled_at = Time.zone.now - 3.years 
        organization.save

        expect(helper.organization_status(organization)).not_to eq(nil)

        organization.renovation_date = Time.zone.now - 3.years
        organization.save
        
        expect(helper.organization_status(organization)).not_to eq(nil)
    end

    it "organization_list_status" do
        expect(helper.organization_list_status(organization)).to eq(nil)

        organization.renovation_date = Time.zone.now - 2.years + 1.day
        organization.save

        expect(helper.organization_list_status(organization)).to eq(nil)

        organization.canceled_at = Time.zone.now - 2.years + 1.day
        organization.save

        expect(helper.organization_list_status(organization)).not_to eq(nil)

        organization.renovation_date = Time.zone.now - 3.years
        organization.save
        
        expect(helper.organization_list_status(organization)).not_to eq(nil)
    end

    it "events_as_lobby_by" do
        expect(helper.events_as_lobby_by(organization)).not_to eq(nil)
    end


    it "organization_back_button" do     
        allow(view).to receive(:current_user).and_return(user)     
        expect(helper.organization_back_button).not_to eq(nil) 
        allow(view).to receive(:current_user).and_return(nil)     
        expect(helper.organization_back_button).not_to eq(nil) 
    end
end