require 'rails_helper'

RSpec.describe EventsHelper, type: :helper do
    let!(:event) { create(:event) }
    let!(:user) { create(:user)}
    let!(:lobby) { create(:user,:lobby)}
    let!(:admin) { create(:user,:admin)}

    it "cancelable_event?" do
        expect(helper.cancelable_event?(event)).to eq(true)        
    end

    it "declinable_or_aceptable_event?" do
        expect(helper.declinable_or_aceptable_event?(event)).to eq(false)        
    end

    it "event_title" do
        expect(helper.event_title(user)).not_to eq("")   
        
        expect(helper.event_title(lobby)).not_to eq("")  
    end

    it "event_new" do
        expect(helper.event_new(user)).not_to eq("")  
        
        expect(helper.event_new(lobby)).not_to eq("") 
    end

    it "event_description" do
        expect(helper.event_description(user)).not_to eq("")    
        
        expect(helper.event_description(lobby)).not_to eq("") 

        expect(helper.event_description(admin)).not_to eq("") 
    end

    it "reason_text" do
        expect(helper.reason_text(event)).not_to eq(nil)  
        
        event.canceled_reasons = "<p>prueba</p>"

        expect(helper.reason_text(event)).not_to eq(nil)  

        event.declined_reasons = "<p>prueba</p>"

        expect(helper.reason_text(event)).not_to eq(nil)  
    end
end