require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
    let!(:holder) { create(:holder)}

    it "show_headline" do   
        expect(helper.show_headline({})).not_to eq(nil)      
    end

    it "show_date" do   
        expect(helper.show_date(Time.zone.now)).not_to eq(nil)      
    end

    it "current_language" do   
        expect(helper.current_language).not_to eq(nil)      
    end

    it "show_agenda_link" do   

        expect(helper.show_agenda_link(holder)).not_to eq(nil)      
    end

    it "get_min_errors" do   

        expect(helper.get_min_errors("prueba.prueba")).not_to eq(nil)      
    end

    it "tooltip" do   

        expect(helper.tooltip("prueba")).not_to eq(nil)      
    end

    it "export_link" do   

        expect(helper.export_link("prueba_path")).not_to eq(nil)      
    end
end