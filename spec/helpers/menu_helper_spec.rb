require 'rails_helper'

RSpec.describe MenuHelper, type: :helper do
    it "current_class" do
        expect(helper.current_class("")).to eq("active")        
    end

    it "admin_current_class" do
        expect(helper.admin_current_class).to eq(nil)
    end
end