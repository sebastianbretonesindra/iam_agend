require 'rails_helper'

RSpec.describe Admin::SidebarHelper, type: :helper do
    let!(:user) { create(:user)}
    let!(:lobby) { create(:user,:lobby)}
    let!(:admin) { create(:user,:admin)}

    it "active_menu" do   
        expect(helper.active_menu(Event)).not_to eq(nil) 
    end

    it "event_fixed_filters" do   
        expect(helper.event_fixed_filters).not_to eq(nil)      
    end

    it "help_by_role" do   
        expect(helper.help_by_role(user)).not_to eq(nil)     
        expect(helper.help_by_role(lobby)).not_to eq(nil)    
        expect(helper.help_by_role(admin)).not_to eq(nil)     
    end
    

    it "current_action?(action)" do   
        expect(helper.send(:current_action?,nil)).not_to eq(nil)      
    end

    it "unfiltered?" do   
        expect(helper.send(:unfiltered?)).not_to eq(nil)      
    end
end