require 'rails_helper'

RSpec.describe Admin::AgentsHelper, type: :helper do
    let!(:attachment) { create(:attachment)}
    let!(:agent) { create(:agent)}

    

    it "agent_attachment_persisted_data" do     
        expect(helper.agent_attachment_persisted_data(attachment)).not_to eq(nil) 
    end

    it "agents_public_assignments_editor" do 

        expect(helper.agents_public_assignments_editor(agent)).not_to eq(nil) 
    end

    it "agent_attachment_persisted_remove_notice" do   
       
        expect(helper.agent_attachment_persisted_remove_notice(attachment)).not_to eq(nil) 
    end

    
end