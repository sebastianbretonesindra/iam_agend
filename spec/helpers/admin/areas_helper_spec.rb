require 'rails_helper'

RSpec.describe Admin::AreasHelper, type: :helper do
    let!(:area) { create(:area)}
    let!(:areas) { build_list :area, 3}

    

    it "parent_label" do     
        expect(helper.parent_label(area)).to eq(nil) 
    end

    it "nested_areas" do 

        expect(helper.nested_areas(areas)).to eq(nil) 
    end
    
end