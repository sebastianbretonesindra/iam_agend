require 'rails_helper'

RSpec.describe ValidationHelper, type: :helper do
    it "valid NIF" do   
        expect(helper.validadorNIF_DNI_NIE({dni: "25459856Y"})).not_to eq(nil)      
    end

    it "valid Pasaport" do   
        expect(helper.validatorPasaport({pasaport: "SPO785764S"})).not_to eq(nil)      
    end
end