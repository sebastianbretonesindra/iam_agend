require 'faker'

FactoryGirl.define do
  factory :newsletters_interest do
    association :interest, factory: :interest
    association :newsletter, factory: :newsletter
  end
end