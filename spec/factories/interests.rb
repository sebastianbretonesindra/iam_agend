FactoryGirl.define do

  factory :interest do
    sequence(:name) { |n| "Interest #{n}" }
    code 5
  end

end
