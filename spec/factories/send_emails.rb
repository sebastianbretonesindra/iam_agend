require 'faker'

FactoryGirl.define do
  factory :send_email do
    auto_send true
    auto_hour "10:00"
    auto_user_type 1
    manual_send false
    auto_frecuence 5
    auto_date Time.zone.now
    manual_hour "10:00"
    manual_from_date Time.zone.now
    manual_to_date Time.zone.now
    manual_user_type 1
  end

end
