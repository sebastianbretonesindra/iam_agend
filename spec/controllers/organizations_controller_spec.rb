require 'rails_helper'

RSpec.describe OrganizationsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end

  describe "GET #index" do
    it 'success 200' do
      create(:interest, name: "Otros", code: "other")
      create(:interest, name: "Asdf", code: "asfas")
      get :index
      expect(response.status).to eq(200)
    end

    it 'params search' do
      create(:interest, name: "Otros", code: "other")
      get :index, {status_type: 1}
      expect(response.status).to eq(200)

      get :index, {status_type: 2}
      expect(response.status).to eq(200)

      get :index, {status_type: 3}
      expect(response.status).to eq(200)

      get :index, {status_type: 4}
      expect(response.status).to eq(200)


      organization = create(:organization)
      create(:event, organization: organization)
      get :index, {status_type: 1, order: 1}
      expect(response.status).to eq(200)

      get :index, {status_type: 1, order: 2}
      expect(response.status).to eq(200)

      get :index, {status_type: 1, order: 3}
      expect(response.status).to eq(200)

      get :index, {status_type: 1, order: 4}
      expect(response.status).to eq(200)

      get :index, {status_type: 1, order: 5}
      expect(response.status).to eq(200)

      get :index, {status_type: 1, order: 6}
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'success 200' do
      create(:interest, name: "Otros", code: "other")
      organization = create(:organization)
      create(:event, organization: organization)
      get :show, id: organization.id
      expect(response.status).to eq(200)
    end
  end  
end

