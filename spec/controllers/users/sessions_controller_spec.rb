require 'rails_helper'

RSpec.describe Users::SessionsController, type: :controller do
  before(:each) do
    user = create(:user, :admin)
    sign_in user
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "Post #create" do
    

    it 'session' do
      post :create
      expect(response.status).to eq(302)
    end
    
  end

end

