require 'rails_helper'

RSpec.describe Admin::AgentsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  

  describe "GET #index" do
    it 'should inherit behavior from Parent' do
      expect(AgentsController.superclass).to eq(AdminController)
    end

    it 'assigns all agents as @agents' do
      organization = create(:organization)
      create(:agent, organization: organization)
      get :index, organization_id: organization.id
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #show" do
   

    it 'status 200' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      get :show,{ id: agent.id, organization_id: organization.id}
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #new" do
   

    it 'status 200' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      get :new,{ organization_id: organization.id}
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #edit" do
   

    it 'status 200' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      get :edit,{ id: agent.id, organization_id: organization.id}
      expect(response.status).to eq(200)
    end
    
  end

  describe "GET #destroy" do
   

    it 'status 302 redirect' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      get :destroy,{ id: agent.id, organization_id: organization.id}
      expect(response.status).to eq(302)
    end
    
  end

  describe "POST #create" do
   

    it 'status 200' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      agent.id = nil
      post :create,{ organization_id: organization.id, agent: agent.attributes}
      expect(response.status).to eq(200)
    end

    
    
  end

  describe "UT #update" do
   

    it 'status 200' do
      organization = create(:organization)
      agent = create(:agent, organization: organization)
      put :update,{ id: agent.id, organization_id: organization.id, agent: agent.attributes}
      expect(response.status).to eq(302)
    end
    
  end
  
end

