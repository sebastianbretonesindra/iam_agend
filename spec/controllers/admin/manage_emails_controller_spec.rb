require 'rails_helper'

RSpec.describe Admin::ManageEmailsController, type: :controller do
  before(:each) do
    sign_in create(:user,:admin)
  end

  describe "GET #edit" do
    it "assigns the requested manage email as @manage email" do
      data_json_cc= "transparenciaydatos@madrid.es"
      data_json_cco ="creaspodhe@madrid.es"
      ManageEmail.create!(id: 1, type_data: "Holder", sender: "transparenciaydatos@madrid.es",
          fields_cc: data_json_cc, 
          fields_cco: data_json_cco,
          subject: I18n.t('mailers.new_holder.subject'), 
          email_body: I18n.t("mailers.new_holder.body")) 

      ManageEmail.create!(id: 2, type_data: 'AuxManager', sender: "transparenciaydatos@madrid.es",
          fields_cc: data_json_cc, 
          fields_cco: data_json_cco,
          subject: I18n.t('mailers.new_aux.subject'), 
          email_body: I18n.t("mailers.new_aux.body")) 
      get :edit, {:id => 1}
      expect(response).to be_ok
    end
  end

  describe "PUT #update" do
    it "assigns the requested manage email as @manage_email" do
      data_json_cc= "transparenciaydatos@madrid.es"
      data_json_cco ="creaspodhe@madrid.es"
      ManageEmail.create!(id: 1, type_data: "Holder", sender: "transparenciaydatos@madrid.es",
          fields_cc: data_json_cc, 
          fields_cco: data_json_cco,
          subject: I18n.t('mailers.new_holder.subject'), 
          email_body: I18n.t("mailers.new_holder.body")) 

      ManageEmail.create!(id: 2, type_data: 'AuxManager', sender: "transparenciaydatos@madrid.es",
          fields_cc: data_json_cc, 
          fields_cco: data_json_cco,
          subject: I18n.t('mailers.new_aux.subject'), 
          email_body: I18n.t("mailers.new_aux.body")) 
      manage_email = ManageEmail.find(1)
      put :update, {:id => manage_email.to_param, :manage_email => manage_email.attributes}
      expect(assigns(:manage_email)).to eq(manage_email)
    end
  end
end

