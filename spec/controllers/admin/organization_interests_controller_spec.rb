require 'rails_helper'

RSpec.describe Admin::OrganizationInterestsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
      interest = create(:interest, name: "Otros", code: "other")
      organization = create(:organization)
      get :index, organization_id: organization.id
      expect(response.status).to eq(200)
    end
    
  end

  describe "PUT #update" do
    it 'status 302' do
      interest = create(:interest, name: "Otros", code: "other")
      organization = create(:organization)
      put :update, {organization_id: organization.id, organization:{communication_term: true, interest_ids: [interest.id]}}
      expect(response.status).to eq(302)
    end
  end

  

end

