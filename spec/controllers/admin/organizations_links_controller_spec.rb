require 'rails_helper'

RSpec.describe Admin::OrganizationsLinksController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
      get :index
      expect(response.status).to eq(200)
    end
  end
end

