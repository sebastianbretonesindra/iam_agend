require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'index' do
      create(:user)

      
      get :index
      expect(assigns(:users).count).to eq(2)
    end

    it 'role 3' do
      create(:user)

      
      get :index, role: 3
      expect(assigns(:users).count).to eq(0)
    end

    it 'role 4' do
      create(:user)

      
      get :index, role: 4
      expect(assigns(:users).count).to eq(0)
    end
  end

  describe "GET #new" do
    it 'new' do
     
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'destroy' do
      user = create(:user, :user)
      get :destroy, id: user.id
      expect(response.status).to eq(302)
    end
  end

  # describe "POST #disable" do
  #   it 'disable' do
  #     user = create(:user,:user)
  #     post :disable, id: user.id
  #     expect(response.status).to eq(302)
  #   end
  # end

  describe "GET #revert" do
    it 'revert' do
      user = create(:user, :user)
      get :revert, id: user.id
      expect(response.status).to eq(302)
    end
  end

  describe "GET #assign_holder" do
    it 'assign_holder' do
      user = create(:user,:user, user_key: 5)
      get :assign_holder, {id: user.id, format: user.id}
      expect(response.status).to eq(302)
    end
  end

  describe "GET #edit" do
    it 'edit' do
      user = create(:user)
      get :edit, id: user.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    

    it 'show ' do
      user = create(:user)

      get :show, id: user.id
      expect(response.status).to eq(200)
    end
  end

  describe "PUT #update" do
    it 'edit' do
      user = create(:user, :admin)
      user.role = nil
      put :update, {id: user.id, user: user.attributes}
      expect(response.status).to eq(302)
    end
  end

  describe "POST #create" do
    it 'create' do
      user = create(:user, :admin)
      user.id = nil
      user.role = nil
      post :create, user: user.attributes
      expect(response.status).to eq(200)
    end
  end

 
end

