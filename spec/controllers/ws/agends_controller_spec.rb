require 'rails_helper'

RSpec.describe Ws::AgendsController, type: :controller do
 
  

  describe "POST #generic" do
    

    it 'assigns all agents as @agents' do
      create(:event)
      post :generic,{ api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: nil}
      expect(response.status).to eq(500)      

      post :generic,{ api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: Time.zone.now, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :generic,{ api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: Time.zone.now, end_date: nil}
      expect(response.status).to eq(500)

      post :generic,{ api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :generic,{ api:  "XXXX", api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :generic,{ api:  nil, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)
    end
    
  end

  describe "POST #holder" do
   

    it 'assigns all agents as @agents' do
      event=create(:event)
      holder = event.position.holder
      holder.personal_code = 123
      holder.save
      post :holder,{personal_code: 123, api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: nil}
      expect(response.status).to eq(500)      

      post :holder,{personal_code: 123, api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: Time.zone.now, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :holder,{personal_code: 123, api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: Time.zone.now, end_date: nil}
      expect(response.status).to eq(500)

      post :holder,{personal_code: 123, api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :holder,{personal_code: 123, api:  "XXXX", api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :holder,{personal_code: 123, api:  nil, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)

      post :holder,{personal_code: " ", api:  Rails.application.secrets.user_trape, api_password: Rails.application.secrets.pass_trape, start_date: nil, end_date: Time.zone.now}
      expect(response.status).to eq(500)
    end
    
  end

  
  
end

