require 'rails_helper'

RSpec.describe VisitorsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :event
  }

  let(:invalid_attributes) {
    attributes_for :event, :invalid
  }

  describe "GET #index" do
    it 'assigns all events as @events' do
      create(:event)
      create(:event)
      create(:event)
      events=Event.all
      get :index, reset: true
      expect(assigns(:paginated_events).count).to eq(events.count)
      area = create(:area)

      get :index, {keyword: 'xx', area: area.id, from: Time.zone.now, to: Time.zone.now}
      expect(assigns(:paginated_events).count).to eq(0)

      get :index, {keyword: 'xx', area: area.id, from: Time.zone.now}
      expect(assigns(:paginated_events).count).to eq(0)

      get :index, {keyword: 'xx', area: area.id, to: Time.zone.now}
      expect(assigns(:paginated_events).count).to eq(0)

      get :index, {tab_calendar: 'day'}
      expect(assigns(:paginated_events).count).to eq(3)

      get :index, {tab_calendar: 'week'}
      expect(assigns(:paginated_events).count).to eq(3)

      get :index, {tab_calendar: 'month'}
      expect(assigns(:paginated_events).count).to eq(3)

      get :index, {tab_calendar: 'all'}
      expect(assigns(:paginated_events).count).to eq(3)

      DataPreference.create(title: "show_calendar",content_data: 'day',type_data: 3)
      get :index, {set_date: '2021-01-01'}
      expect(assigns(:paginated_events).count).to eq(3)

      get :index, {select: {:year => "2021", :month => "01"}, select_calendar: {:year => "2021", :month => "01"}}
      expect(assigns(:paginated_events).count).to eq(0)
    end

    it 'assigns all events as @events' do
      create(:event)
      create(:event)
      create(:event)
      events=Event.all

      get :index, {format: :csv}
      expect(assigns(:paginated_events).count).to eq(3)

     
    end
  end

  describe "GET #show" do
    it 'show event' do
      create(:event)
      create(:event)
      create(:event)
      get :show, id: Event.first.slug
      expect(response.status).to eq(200)
    end
  end

  describe "GET #agenda" do
    it 'agenda' do
      create(:event)
      create(:event)
      create(:event)
      area = create(:area)
      holder = create(:holder)
      get :agenda, {full_name: holder.full_name, holder: holder.id }
      expect(response.status).to eq(200)

      get :agenda,{full_name: holder.full_name, holder: holder.id + 5 }
      expect(response.status).to eq(302)
    end
  end

  # describe "GET #update_holders" do
  #   it 'update_holders' do
  #     create(:event)
  #     create(:event)
  #     create(:event)
  #     area = create(:area)
  #     holder = create(:holder)
  #     get :update_holders, id: area.id , xhr: true
  #     expect(response.status).to eq(200)
  #   end
  # end

end

