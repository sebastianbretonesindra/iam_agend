require 'rails_helper'

RSpec.describe ServicioIntegracionBaregController, type: :controller do


    let(:mensaje) do
        {
            PeticionIniciarExpediente: {
                CodTipoExpediente: '619',
                XmlDatosEntrada: '',
                ListDocs: nil,
                ListFormatos: nil,
                Usuario: 'xx'
            }
        }     
    end

    let(:mensaje_invalid) do
        {
            PeticionIniciarExpediente: {
                CodTipoExpediente: '619',
                XmlDatosEntrada: '',
                ListDocs: nil,
                ListFormatos: nil,
                Usuario: ''
            }
        }     
    end

    let(:datos_insertar) do
        {
            PeticionIniciarExpediente: {
                CodTipoExpediente: '619',
                XmlDatosEntrada: campos_inserccion_xml,
                ListDocs: [{
                    idDocumento: "xadjflafhalghoq123"},{
                    idDocumento: "3458768ihkjfafjahdljkf"
                }],
                ListFormatos: nil,
                Usuario: 'xx'
            }
        }
    end


    let(:datos_actualizar) do
        {
            PeticionIniciarExpediente: {
                CodTipoExpediente: '620',
                XmlDatosEntrada: campos_inserccion_xml,
                ListDocs: nil,
                ListFormatos: nil,
                Usuario: 'xx'
            }
        }
    end

    let(:datos_baja) do
        {
            PeticionIniciarExpediente: {
                CodTipoExpediente: '621',
                XmlDatosEntrada: campos_inserccion_xml,
                ListDocs: nil,
                ListFormatos: nil,
                Usuario: 'xx'
            }
        }
    end

    let(:datos_documentacion_expediente) do
        {
            peticionAportarDocumentacion: {
                idExpediente: 1,
                XmlDatosEntrada: campos_inserccion_xml,
                refExpediente: '1',
                ListDocs: nil,
                ListFormatos: nil,
                usuario: 'xx'
            }
        }
    end 

    let(:campos_inserccion_xml) do
        "<![CDATA[<?xml version='1.0' encoding='utf-8' ?>
        <intercambioIAM>
        <formulario>
            <nombre>876</nombre>
            <datosComunes>
                <datosInteresado>
                    <variables>
                        <variable>
                            <clave>COMUNES_INTERESADO_NUMIDENT</clave>
                            <valor>77777777T</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_PORTAL</clave>
                            <valor>X</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_PROVINCIA</clave>
                            <valor>X</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_PAIS</clave>
                            <valor>X</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_TIPOVIA</clave>
                            <valor>X</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_FINALIDAD</clave>
                            <valor>XXXXX</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_MOVIL</clave>
                            <valor>666454545</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_PLANTA</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_CATEG</clave>
                            <valor>Persona física</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_NUMERO</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_RAZONSOCIAL</clave>
                            <valor>Prueba</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_APELLIDO1</clave>
                            <valor>xprueba</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_CHECKEMAIL</clave>
                            <valor>true</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_APELLIDO2</clave>
                            <valor>xprueba2</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_CODPOSTAL</clave>
                            <valor>28000</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_NOMBRE</clave>
                            <valor>Int nombre</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_MUNICIPIO</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_ESCALERA</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_TELEFONO</clave>
                            <valor>923454545</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_EMAIL</clave>
                            <valor>prueba@xxx.es</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_PUERTA</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_WEB</clave>
                            <valor>www.pruebas.es</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_NOMBREVIA</clave>
                            <valor>calle prueba</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_INTERESADO_CHECKSMS</clave>
                            <valor>true</valor>
                        </variable>
                    </variables>
                </datosInteresado>
                <datosRepresentante>
                    <variables>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_CHECKEMAIL</clave>
                            <valor>true</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_NOMBRE</clave>
                            <valor>Repre prueba</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_NOMBREVIA</clave>
                            <valor>ssss</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_RAZONSOCIAL</clave>
                            <valor>Repre prueba</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_MOVIL</clave>
                            <valor>658474747</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_NUMIDENT</clave>
                            <valor>66666666R</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_EMAIL</clave>
                            <valor>opcion@repre.es</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_TELEFONO</clave>
                            <valor>987454545</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_PUERTA</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_CODPOSTAL</clave>
                            <valor>28000</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_CHECKSMS</clave>
                            <valor>true</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_PAIS</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_TIPOVIA</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_MUNICIPIO</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_PROVINCIA</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_PLANTA</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_ESCALERA</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_NUMERO</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_PORTAL</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_APELLIDO1</clave>
                            <valor>Repre1xx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_REPRESENTANTE_APELLIDO2</clave>
                            <valor>Repre2</valor>
                        </variable>
                    </variables>
                </datosRepresentante>
                <datosNotificacion>
                    <variables>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_EMAIL</clave>
                            <valor>notifica@prueba.es</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_PLANTA</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_MOVIL</clave>
                            <valor>687969696</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_ESCALERA</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_CODPOSTAL</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_MUNICIPIO</clave>
                            <valor>1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_NOMBRE</clave>
                            <valor>notifica1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_PUERTA</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_TELEFONO</clave>
                            <valor>978232323</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_PORTAL</clave>
                            <valor>xxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_PROVINCIA</clave>
                            <valor>xxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_PAIS</clave>
                            <valor>xxxxx</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_APELLIDO1</clave>
                            <valor>notip1</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_RAZONSOCIAL</clave>
                            <valor>noti</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_APELLIDO2</clave>
                            <valor>notip2</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_NUMIDENT</clave>
                            <valor>9898989898Q</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_NOMBREVIA</clave>
                            <valor>x</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_TIPOVIA</clave>
                            <valor>x</valor>
                        </variable>
                        <variable>
                            <clave>COMUNES_NOTIFICACION_NUMERO</clave>
                            <valor>x</valor>
                        </variable>
                    </variables>
                </datosNotificacion>
            </datosComunes>
            <datosEspecificos>
                <variables>
                    <variable>
                        <clave>COMUNES_COMUNICACION_NOMBRE</clave>
                        <valor>xxxxx</valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_OTROS</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_PROPIA_REAYUDAS</clave>
                        <valor>tue</valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_PROPIA_RADIO</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_UE</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_OTROSDESC</clave>
                        <valor>xxxx</valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_TELEFONO</clave>
                        <valor>987456123</valor>
                    </variable>
                    <variable>
                        <clave>FONDOS_REPRESENTA</clave>
                        <valor></valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_CNMC</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>EJERCICIO_REPRESENTA</clave>
                        <valor></valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_PROPIA_CELEBRA</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_RADIO</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_MOVIL</clave>
                        <valor>78569877</valor>
                    </variable>
                    <variable>
                        <clave>componentRefresh</clave>
                        <valor></valor>
                    </variable>
                    <variable>
                        <clave>formulaVersion</clave>
                        <valor>4.1.6</valor>
                    </variable>
                    <variable>
                        <clave>APELLIDO1_REPRESENTA</clave>
                        <valor>xxxx</valor>
                    </variable>
                    <variable>
                        <clave>NOMBRE_REPRESENTA</clave>
                        <valor>xxx</valor>
                    </variable>
                    <variable>
                        <clave>APELLIDO2_REPRESENTA</clave>
                        <valor>ssss</valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_PROPIA_FONDOS</clave>
                        <valor></valor>
                    </variable>
                    <variable>
                        <clave>REGISTRO_INSCRIPCION_GENERAL</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_PROPIA_EJANUAL</clave>
                        <valor></valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_EMAIL</clave>
                        <valor>xxx@xxx.es</valor>
                    </variable>
                    <variable>
                        <clave>ENTIDAD_CON_REPRESENTA</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>ENTIDAD_AYUDA_REPRESENTA</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>ACTIVIDAD_AJENA_RADIO</clave>
                        <valor>true</valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_NUMIDENT</clave>
                        <valor>45236987W</valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_APELLIDO2</clave>
                        <valor>xxxxx</valor>
                    </variable>
                    <variable>
                        <clave>COMUNES_COMUNICACION_APELLIDO1</clave>
                        <valor>xxxx</valor>
                    </variable>
                    <variable>
                        <clave>FECHA_REPRESENTA</clave>
                        <valor>20/03/2020</valor>
                    </variable>
                    <variable>
                        <clave>BORRADOR_DESDE_FORMULARIO</clave>
                        <valor>SI</valor>
                    </variable>
                    <variable>
                        <clave>DNI_REPRESENTA</clave>
                        <valor>78569871V</valor>
                    </variable>
                </variables>
            </datosEspecificos>
        </formulario></intercambioIAM>]]>"
    end

    let(:soap_wsdl_path) { "/servicio_integracion_bareg/wsdl"}
    let(:application_base) { "http://app" }

    render_views # added render_views

    HTTPI.adapter = :rack

    HTTPI::Adapter::Rack.mount 'app', Rails.application
   

    it 'inicioExpediente valid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: mensaje)
        expect(result).not_to eq(nil)
    end

    it 'inicioExpediente blank' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: mensaje_invalid)
        expect(result).not_to eq(nil)
    end

    it 'inicioExpediente invalid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: nil)
        expect(result).not_to eq(nil)
    end

    it 'insertarExpediente' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: datos_insertar)
        expect(result).not_to eq(nil)
        result = client.call(:inicio_expediente, message: datos_insertar)
        expect(result).not_to eq(nil)
    end

    it 'insertarExpediente invalid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: nil)
        expect(result).not_to eq(nil)
        result = client.call(:inicio_expediente, message: nil)
        expect(result).not_to eq(nil)
    end

    it 'actualizarExpediente' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: datos_actualizar)
        expect(result).not_to eq(nil)
    end

    it 'actualizarExpediente invalid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: nil)
        expect(result).not_to eq(nil)
    end

    it 'bajaExpediente' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: datos_baja)
        expect(result).not_to eq(nil)
    end

    it 'bajaExpediente invalid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:inicio_expediente, message: nil)
        expect(result).not_to eq(nil)
    end

    it 'aporteDocumentacionExpediente' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:aporte_documentacion_expediente, message: datos_documentacion_expediente)
        expect(result).not_to eq(nil)
    end

    it 'aporteDocumentacionExpediente invalid' do
        request.host = application_base
        client = Savon::Client.new({:wsdl => application_base + soap_wsdl_path })
        result = client.call(:aporte_documentacion_expediente, message: nil)
        expect(result).not_to eq(nil)
    end
end

