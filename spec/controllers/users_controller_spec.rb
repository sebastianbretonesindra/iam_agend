require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :user
  }

  let(:invalid_attributes) {
    attributes_for :user, :invalid
  }

  context 'with valid params' do
    it 'creates a new user' do
      expect {
        post :create, user: valid_attributes
      }.to change(User, :count).by(0)
    end
  end


  describe "GET #index" do
    it 'assigns all users as @users' do
      users=User.all
      get :index, role: 3
      expect(assigns(:users).count).not_to eq(users.count)
    end


    it 'assigns all users as @users with param role' do
      users=User.all
      get :index, role: 3
      expect(assigns(:users).count).not_to eq(users.count)
    end

  end

end

