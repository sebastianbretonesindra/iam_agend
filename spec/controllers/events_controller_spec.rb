require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end

  let(:valid_attributes) {
    attributes_for :event
  }

  let(:invalid_attributes) {
    attributes_for :event, :invalid
  }

  context 'with valid params' do
    it 'creates a new Event' do
      expect {
        post :create, event: valid_attributes
      }.to change(Event, :count).by(0)

      expect {
        event = build(:event, status: nil)
        event.status = nil
        post :create, event: event.attributes
      }.to change(Event, :count).by(1)
    end

    it 'update a Event' do
      
        event = create(:event, status: nil)
        event.status = nil
        put :update, { id: event.id, event: event.attributes}
        expect(response.status).to eq(302)
    end

    it 'update a decline Event' do
      
      event = create(:event, status: nil, decline: 'true')
      event.status = nil
      event.decline = 'true'
      event.save
      put :update, { id: event.id, event: event.attributes}
      expect(response.status).to eq(302)
    end
    
    it 'update a accept Event' do
      
      event = create(:event, status: nil, accept: 'true')
      event.status = nil
      event.accept = 'true'
      put :update, { id: event.id, event: event.attributes}
      expect(response.status).to eq(302)
    end

    it 'Update event lobby user' do
      organization = create(:organization)
      sign_in create(:user, :lobby, organization: organization)
      event = create(:event, status: nil)
      event.status = nil
      event.cancel = 'true'
      put :update, { id: event.id, event: event.attributes}
      expect(response.status).to eq(302)
    end

    it 'clone event' do
      event = create(:event)
      create(:participant, event_id: event.id)
      create(:attendee, event_id: event.id)
      create(:event_agent, event_id: event.id)
      create(:event_represented_entity, event_id: event.id)
      get :clone, :id => event.slug
      expect(event.clone).to eq(event)
    end
  end


  describe "GET #index" do
    it 'assigns all events as @events' do
      events=Event.all
      get :index
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events' do
      events=Event.all
      get :index, format: :csv
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_district' do
      events=Event.all
      get :index, search_person: "person"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_start_date and search_end_date' do
      events=Event.all
      get :index, search_start_date: "10/12/2019", search_end_date: "11/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_start_date' do
      events=Event.all
      get :index, search_start_date: "10/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_end_date' do
      events=Event.all
      get :index, search_end_date: "11/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_start_date_published and search_end_date_published' do
      events=Event.all
      get :index, search_start_date_published: "10/12/2019", search_end_date_published: "11/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_start_date_published' do
      events=Event.all
      get :index, search_start_date_published: "10/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end

    it 'assigns all events as @events with param search_end_date_published' do
      events=Event.all
      get :index, search_end_date_published: "11/12/2019"
      expect(assigns(:events).count).to eq(events.count)
    end
    
  end


  describe "GET #new" do
    it 'new event' do
      get :new
      expect(response.status).to eq(200)
    end

    it 'new event lobby user' do
      organization = create(:organization)
      sign_in create(:user, :lobby, organization: organization)
      get :new
      expect(response.status).to eq(302)
    end
  end

  describe "GET #edit" do
    it 'edit event' do
      event = create(:event)
      get :edit, id: event.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'destroy event' do
      event = create(:event)
      get :destroy, id: event.id
      expect(response.status).to eq(302)
    end
  end

  describe "GET #export_outlook" do
    it 'export_outlook event' do
      event = create(:event)
      get :export_outlook, id: event.id
      expect(response.status).to eq(200)
    end
  end

  # describe "GET #redirect" do
  #   it 'redirect event' do
  #     event = create(:event)
  #     get :redirect, id: event.id
  #     expect(response.status).to eq(302)
  #   end
  # end

  # describe "GET #callback" do
  #   it 'callback event' do
  #     event = create(:event)
  #     get :callback, id: event.id
  #     expect(response.status).to eq(302)
  #   end
  # end

  describe "GET #download" do
    it 'download event' do
      event = create(:event)
      get :download, id: event.id
      expect(response.status).to eq(302)
    end

    it 'download event xls' do
      event = create(:event)
      get :download, {id: event.id, formato: "xlsx"}
      expect(response.status).to eq(200)
    end
  end
end

