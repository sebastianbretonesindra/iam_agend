require 'rails_helper'

RSpec.describe UwebAccessController, type: :controller do
  describe "GET #uweb_sign_in" do
    it 'no sign_in' do
      get :uweb_sign_in
      expect(response.status).to eq(302)
    end
  end

end

