require 'rails_helper'

describe Category do

  let(:category) { build(:category) }

  it "should be valid" do
    expect(category).to be_valid
  end

  it "categories_sub_categories" do
    load "#{Rails.root}/db/test_seeds.rb"
    expect(Category.categories_sub_categories).not_to eq(nil)
  end

end
