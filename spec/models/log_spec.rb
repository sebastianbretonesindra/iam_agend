require 'rails_helper'

describe Log do

  describe "#activity" do

    it "creates a log entry for an activity" do
      organization = create(:organization)
      newsletter = build(:newsletter)
      interest = create(:interest, name: "Otros", code: "other")
      newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
      newsletter.interest = interest
      newsletter.interests << interest
      newsletter.save

      expect{ Log.activity(organization, :email, newsletter) }.to change { Log.count }.by(1)

      log = Log.last
      expect(log.organization_id).to eq(organization.id)
      expect(log.action).to eq("email")
      expect(log.actionable).to eq(newsletter)
    end

  end

end
