describe User do

  before(:each) { @user = FactoryGirl.create(:user) }

  subject { @user }

  it { should respond_to(:email) }
  it { should respond_to(:name) }
  it { should respond_to(:full_name) }
  it { should respond_to(:full_name_comma) }

  it "should be active when created" do
    expect(@user.active).to be
  end

  it "should have user role when created" do
    expect(@user.role.to_sym).to be :user
  end

  it "should have full name composed by first and last name" do
    expect(@user.full_name).to eq((@user.first_name.to_s+' '+@user.last_name.to_s+' '+@user.second_last_name.to_s).strip)
  end

  it "should have full name comma composed by first and last name" do
    expect(@user.full_name_comma).to eq((@user.first_name.to_s+', '+@user.last_name.to_s+' '+@user.second_last_name.to_s).strip)
  end

  it "should have name composed by first and last name" do
    expect(@user.name).to eq(((@user.last_name.to_s+' '+@user.second_last_name.to_s).strip+', '+@user.first_name.to_s).strip)
  end

  it "should have be valid when created from uweb data array" do
    data = FactoryGirl.build(:uweb_user)

    user = User.create_from_uweb(:user, data)

    expect { user.save }.to change { User.count }.from(1).to(2)
  end

  it "should have be valid when created from uweb data array and user exists by user_key" do
    data = FactoryGirl.build(:uweb_user)
    @user.update(user_key: data["CLAVE_IND"] )

    

    user = User.create_from_uweb(:user, data)

    expect { user.save }.not_to change { User.count }


    data["MAIL"] = nil
    user = User.create_from_uweb(:user, data)
    expect { user.save }.not_to change { User.count }

    data["BAJA_LOGICA"] = "0"
    user = User.create_from_uweb(:user, data)
    expect { user.save }.not_to change { User.count }

    data["BAJA_LOGICA"] = "1"
    user = User.create_from_uweb(:user, data)
    expect { user.save }.not_to change { User.count }


    data["TELEFONO"] = "923454545"
    user = User.create_from_uweb(:user, data)
    expect { user.save }.not_to change { User.count }

    data["TELEFONO"] = "618474747"
    user = User.create_from_uweb(:user, data)
    expect { user.save }.not_to change { User.count }
  end

  it "soft_delete" do
    expect(@user.soft_delete).not_to eq(nil)
  end

  it "role_id" do
    expect(@user.role).not_to eq(nil)
  end

  it "change_role_view" do
    expect(@user.change_role_view).to eq(nil)
  end

  it "user_holder?" do
    expect(@user.user_holder?).not_to eq(nil)
    @user.user_key = "xxx"
    expect(@user.user_holder?).not_to eq(nil)
  end

  it "user_manages?" do
    expect(@user.user_manages?).not_to eq(nil)
    manage = create(:manage)
    holder = create(:holder)
    manage.user = @user
    manage.holder = holder
    manage.save
    @user.manages << manage
    @user.save


    expect(@user.user_manages?).not_to eq(nil)
  end

  it "user_holder_active?" do
    expect(@user.user_holder_active?).not_to eq(nil)
    @user.user_key=12345678
    expect(@user.user_holder_active?).not_to eq(nil)

    holder = create(:holder)
    holder.user_key =  @user.user_key
    holder.save

    expect(@user.user_holder_active?).not_to eq(nil)
  end

  it "email_name" do
    expect(@user.email_name).not_to eq(nil)
  end



end
