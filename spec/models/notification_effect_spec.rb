require 'rails_helper'

RSpec.describe NotificationEffect, type: :model do

  let!(:notification_effect) { create(:notification_effect)}

  it "Should be valid" do
    
    expect(notification_effect).to be_valid
  end

  it "fullname" do
    
    expect(notification_effect.fullname).not_to eq("")
  end


  it "fullname bussiness" do
    notification_effect.business_name = "xxxxxx"
    expect(notification_effect.fullname).not_to eq("")
  end

  it "document_identifier" do
    expect(notification_effect.send(:document_identifier)).to eq(nil)
    notification_effect.identifier_type = "DNI/NIF"
    expect(notification_effect.send(:document_identifier)).to eq(nil)

    notification_effect.identifier_type = "NIE"
    expect(notification_effect.send(:document_identifier)).to eq(nil)

    notification_effect.identifier_type = "Pasaporte"
    expect(notification_effect.send(:document_identifier)).to eq(nil)


    notification_effect.identifier_type = nil
    expect(notification_effect.send(:document_identifier)).not_to eq(nil)
  end

  it "name_entity" do
    expect(notification_effect.send(:name_entity)).to eq(nil)

    notification_effect.business_name = "xxxxxx"
    expect(notification_effect.send(:name_entity)).not_to eq(nil)

    notification_effect.business_name = nil
    notification_effect.name = nil
    expect(notification_effect.send(:name_entity)).to eq(nil)
  end
end
