require 'rails_helper'

RSpec.describe NewslettersInterest, type: :model do

  #let!(:newsletters_interest) { build(:newsletters_interest)}

  it "Should be valid" do
    newsletter = build(:newsletter)
    interest = create(:interest, name: "Otros", code: "other")
    newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
    newsletter.interest = interest
    newsletter.interests << interest
    newsletter.save
    
    expect(newsletters_interest).to be_valid
  end

 
end
