require 'rails_helper'

RSpec.describe ManageEmail, type: :model do
  let(:manage_email) { build(:manage_email) }

  it "should be valid" do
    expect(manage_email).to be_valid
  end


  it "should not be valid" do
    manage_email.sender = "xxxxx"
    expect(manage_email).not_to be_valid
  end

  it "should not be valid cc" do
    manage_email.fields_cc = "xxxxx"
    expect(manage_email).not_to be_valid
  end

  it "should not be valid cco" do
    manage_email.fields_cco = "xxxxx"
    expect(manage_email).not_to be_valid
  end
end
