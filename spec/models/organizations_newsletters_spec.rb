require 'rails_helper'

RSpec.describe OrganizationsNewsletter, type: :model do

  #let!(:newsletters_interest) { build(:newsletters_interest)}

  it "Should be valid" do
    organization = create(:organization)
    newsletter = build(:newsletter)
    interest = create(:interest, name: "Otros", code: "other")
    newsletters_interest = NewslettersInterest.new(interest: interest, newsletter: newsletter)
    newsletter.interest = interest
    newsletter.interests << interest
    newsletter.save

    organization_newsletter = OrganizationsNewsletter.new(organization: organization, newsletter: newsletter)
    
    expect(organization_newsletter).to be_valid

    organization_newsletter.save

    organization_newsletter2 = OrganizationsNewsletter.new(organization: organization, newsletter: newsletter)
    expect(organization_newsletter2).not_to be_valid
  end

 
end
