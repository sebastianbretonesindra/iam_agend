require 'rails_helper'

RSpec.describe EventAgentExistenceValidator, type: :model do
  let(:validator) { EventAgentExistenceValidator.new }

  
  it "validate" do
    event = create(:event)
    expect(validator.validate(event)).to eq(nil)
  end

  it "validate lobby_activity" do
    event = create(:event, lobby_activity: true)
    expect(validator.validate(event)).not_to eq(nil)
  end
end
