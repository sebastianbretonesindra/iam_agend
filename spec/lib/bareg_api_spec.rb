require 'rails_helper'
require 'bareg_api'


describe BaregApi do
  let(:service) { BaregApi }

  #render_views # added render_views

  HTTPI.adapter = :rack
  HTTPI::Adapter::Rack.mount 'localhost:3000', Rails.application

  it "client" do
    aux = service.new

    expect(aux.client).not_to eq(nil)
  end

  it "inicioExpediente" do
    aux = service.new

    expect(aux.inicioExpediente).to eq(nil)
  end

  it "insertarExpediente" do
    aux = service.new

    expect(aux.insertarExpediente).to eq(nil)
  end

  it "actualizarExpediente" do
    aux = service.new

    expect(aux.actualizarExpediente).to eq(nil)
  end

  it "bajaExpediente" do
    aux = service.new

    expect(aux.bajaExpediente).to eq(nil)
  end

  describe "private method" do
   

    it "mensaje" do
        aux = service.new
    
        expect(aux.send(:mensaje)).not_to eq(nil)
    end

    it "datos_insertar" do
        aux = service.new
    
        expect(aux.send(:datos_insertar)).not_to eq(nil)
    end

    it "datos_actualizar" do
        aux = service.new
    
        expect(aux.send(:datos_actualizar)).not_to eq(nil)
    end

    it "datos_baja" do
        aux = service.new
    
        expect(aux.send(:datos_baja)).not_to eq(nil)
    end

    it "campos_inserccion_xml" do
        aux = service.new
    
        expect(aux.send(:campos_inserccion_xml)).not_to eq(nil)
    end
  end
  

end