require 'spec_helper'

require "rake"

describe "madrid rake" do
  before do
    Rake.application.rake_require "tasks/import"
    Rake::Task.define_task(:environment)
  end

  describe "#show_data" do
    let :run_rake_task do
      Rake::Task["madrid:show_data"].reenable
      Rake.application.invoke_task "madrid:show_data"
    end

    context "show_data success" do
      it "show_data" do
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#import" do
    let :run_rake_task do
      Rake::Task["madrid:import"].reenable
      Rake.application.invoke_task "madrid:import"
    end

    context "import success" do
      it "import" do
        create(:holder)
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#holders" do
    let :run_rake_task do
      Rake::Task["madrid:holders"].reenable
      Rake.application.invoke_task "madrid:holders"
    end

    context "holders success" do
      it "holders" do
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#import_holders" do
    let :run_rake_task do
      Rake::Task["madrid:import_holders"].reenable
      Rake.application.invoke_task "madrid:import_holders"
    end

    context "import_holders success" do
      it "import_holders" do
        create(:holder)
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#comprobar" do
    let :run_rake_task do
      #Rake::Task["madrid:comprobar[:data]"].reenable
      Rake.application.invoke_task "madrid:comprobar[xx]"
    end

    context "comprobar success" do
      it "comprobar" do
        holder = create(:holder)
        holder.user_key = 'xx'
        holder.save
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end

  describe "#clean_pos" do
    let :run_rake_task do
      Rake::Task["madrid:clean_pos"].reenable
      Rake.application.invoke_task "madrid:clean_pos"
    end

    context "clean_pos success" do
      it "clean_pos" do
        holder = create(:holder)
        area = create(:area)
        create(:position, holder: holder, title: "pruebas", area: area )
        create(:position, holder: holder, title: "pruebas", area: area )
        run_rake_task

       
        expect(run_rake_task).not_to eq(nil)
      end
    end
  end


  

  

end

