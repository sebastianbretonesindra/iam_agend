require 'spec_helper'

require "rake"

describe "Questions rake" do
  before do
    Rake.application.rake_require "tasks/questions"
    Rake::Task.define_task(:environment)
  end

  describe "#add_questions" do
    let :run_rake_task do
      Rake::Task["questions:add_questions"].reenable
      Rake.application.invoke_task "questions:add_questions"
    end

    context "Add questions success" do
      it "Add questions " do
        run_rake_task

       
        expect(Question.all.count).not_to eq(0)
      end
    end
  end

  

end

