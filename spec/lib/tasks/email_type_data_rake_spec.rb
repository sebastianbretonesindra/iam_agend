require 'spec_helper'

describe "email_type_data rake" do
  before do
    Rake.application.rake_require "tasks/email_type_data"
    Rake::Task.define_task(:environment)
  end

  describe "#emails" do
    let :run_rake_task do
      Rake::Task["email_type_data:emails"].reenable
      Rake.application.invoke_task "email_type_data:emails"
    end

    context "emails success" do
      it "emails" do 
        run_rake_task  

        expect(run_rake_task).not_to eq('')
      end

      it "emails created" do 
        ManageEmail.create!(type_data: "Holder", sender: "transparenciaydatos@madrid.es",
          fields_cc: nil, 
          fields_cco: nil,
          subject: I18n.t('mailers.new_holder.subject'), 
          email_body: I18n.t("mailers.new_holder.body")) 
        run_rake_task  
        
         
        expect(run_rake_task).not_to eq('')
      end
    end
  end
end
