require 'spec_helper'

require "rake"

describe "Update position rake" do
  before do
    Rake.application.rake_require "tasks/update_position"
    Rake::Task.define_task(:environment)
  end

  describe "#show" do
    let :run_rake_task do
      Rake::Task["update_position:show"].reenable
      Rake.application.invoke_task "update_position:show"
    end

    context "Show success" do
      it "show" do
        position = create(:position)
        start = position.start
        run_rake_task

        position = Position.find_by(id: position.id)
        expect(position.start).to eq(start)
      end
    end
  end

  describe "#change" do
    let :run_rake_task do
      Rake::Task["update_position:change"].reenable
      Rake.application.invoke_task "update_position:change"
    end

    context "Change success" do
      it "change" do
        position = create(:position)
        start = position.start
        run_rake_task

        position = Position.find_by(id: position.id)
        expect(position.start).not_to eq(start)
      end

      it "change not" do
        position = create(:position)
        position.start = position.get_previous_time
        position.save
        start = position.start
        run_rake_task

        position = Position.find_by(id: position.id)
        expect(position.start).to eq(start)
      end
    end
  end

end

