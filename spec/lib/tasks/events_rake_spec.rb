require 'spec_helper'

describe "events rake" do
  before do
    Rake.application.rake_require "tasks/events"
    Rake::Task.define_task(:environment)
  end

  describe "#update_event_status" do
    let :run_rake_task do
      Rake::Task["events:update_event_status"].reenable
      Rake.application.invoke_task "events:update_event_status"
    end

    context "update_event_status success" do
      it "update_event_status" do 
        create(:event, scheduled: Time.zone.now - 30.days)
        run_rake_task       
       
        
        expect(run_rake_task).not_to eq('')
      end
    end
  end

  describe "#update_old_event_status" do
    let :run_rake_task do
      Rake::Task["events:update_old_event_status"].reenable
      Rake.application.invoke_task "events:update_old_event_status"
    end

    context "update_old_event_status success" do
      it "update_old_event_status" do 
        create(:event, scheduled: Time.zone.now + 30.days, status: nil)
        create(:event, scheduled: Time.zone.now - 30.days, status: nil)
        run_rake_task       
       
        
        expect(run_rake_task).not_to eq('')
      end
    end
  end
end
