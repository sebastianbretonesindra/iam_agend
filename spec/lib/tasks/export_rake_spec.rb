require 'spec_helper'

describe "export rake" do
  before do
    Rake.application.rake_require "tasks/export"
    Rake::Task.define_task(:environment)
  end

  describe "#organizations" do
    let :run_rake_task do
      Rake::Task["export:organizations"].reenable
      Rake.application.invoke_task "export:organizations"
    end

    context "organizations success" do
      it "check files existence a files contents" do 
        run_rake_task       
       
        csv_file = File.open(Rails.root.to_s + '/public/export/lobbies.csv')
        json_file = File.open(Rails.root.to_s + '/public/export/lobbies.json')
        xls_file = File.open(Rails.root.to_s + '/public/export/lobbies.xlsx')

        expect(File).to exist(csv_file)
        expect(File).to exist(json_file)
        expect(File).to exist(xls_file)
      end

      it "check csv contents" do 
        o = create(:organization)
        o.save!
        run_rake_task       
       
        csv_file = File.open(Rails.root.to_s + '/public/export/lobbies.csv').read.scrub

        csv = CSV.parse(csv_file, headers: true, col_sep: ';')
        expect(csv.first.to_hash.values[0]).to eq(o.identifier)
      end

      it "check json contents" do 
        o = create(:organization)
        o.save!
        run_rake_task       
       
        json_file = File.open(Rails.root.to_s + '/public/export/lobbies.json')

        parsed_json = ActiveSupport::JSON.decode(json_file.read)
        expect(parsed_json.first.values.first).to eq(o.identifier)
      end

      it "check xslx contents" do
        o = create(:organization)
        o.save!
    
        run_rake_task 
        xls_file = File.open(Rails.root.to_s + '/public/export/lobbies.xlsx')
           
        expect(o.identifier).to eq(o.identifier)
      end
    end
  end

  describe "#agendas" do
    let :run_rake_task do
      Rake::Task["export:agendas"].reenable
      Rake.application.invoke_task "export:agendas"
    end

    context "agendas success" do
      it "check files existence a files contents" do
        run_rake_task
    
        csv_file = File.open(Rails.root.to_s + '/public/export/agendas.csv')
        json_file = File.open(Rails.root.to_s + '/public/export/agendas.json')
        xls_file = File.open(Rails.root.to_s + '/public/export/agendas.xlsx')
    
        expect(File).to exist(csv_file)
        expect(File).to exist(json_file)
        expect(File).to exist(xls_file)
      end
    
      it "check csv contents" do
        o = create(:event)
        o.save!
    
        run_rake_task
        csv_file = File.open(Rails.root.to_s + '/public/export/agendas.csv').read.scrub
    
        csv = CSV.parse(csv_file, headers: true, col_sep: ';')
        expect(csv.first.to_hash.values[0]).to eq(o.title)
      end
    
      it "check json contents" do
        o = create(:event)
        o.save!
    
        run_rake_task
        json_file = File.open(Rails.root.to_s + '/public/export/agendas.json')
    
        parsed_json = ActiveSupport::JSON.decode(json_file.read)
        expect(parsed_json.first.values.first).to eq(o.title)
      end
    
      it "check xslx contents" do
        o = create(:event)
        o.save!
    
        run_rake_task
        xls_file = File.open(Rails.root.to_s + '/public/export/agendas.xlsx')
    
       
        expect(o.title).to eq(o.title)
      end
    end
  end
 
end
