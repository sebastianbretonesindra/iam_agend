require 'spec_helper'

describe "import data rake" do
  before do
    Rake.application.rake_require "tasks/import_data"
    Rake::Task.define_task(:environment)
  end

  describe "#all" do
    let :run_rake_task do
      Rake::Task["import_data:all"].reenable
      Rake.application.invoke_task "import_data:all"
    end

    context "all success" do
      it "all" do       
        create(:interest, name: "Actividad normativa y de regulación")
        organization = create(:organization, phones: '923480093;645787879', address: create(:address, phones: '923480093;645787879'))
        create(:legal_representant, organization: organization, phones: '923480093;645787879', address: create(:address, phones: '923480093;645787879'))
        create(:represented_entity, address: create(:address, phones: '923480093;645787879'))
        create(:legal_representant, organization: organization, address: nil)
        create(:organization, address: nil)
        #run_rake_task       
       
        expect(run_rake_task).not_to eq('')
      end
    end
  end

  describe "#renovation_historic" do
    let :run_rake_task do
      Rake::Task["import_data:renovation_historic"].reenable
      Rake.application.invoke_task "import_data:renovation_historic"
    end

    context "renovation_historic success" do
      it "renovation_historic" do       
        create(:organization, renovation_date: Time.zone.now - 2.years)
        organization = create(:organization, renovation_date: Time.zone.now - 2.years)
        create(:organizations_renewal, organization: organization, renovation_date: Time.zone.now - 4.years, active: true)
        #run_rake_task       
       
        expect(run_rake_task).not_to eq('')
      end
    end
  end


  describe "#data_user" do
    let :run_rake_task do
      Rake::Task["import_data:data_user"].reenable
      Rake.application.invoke_task "import_data:data_user"
    end

    context "data_user success" do
      it "data_user" do       
        create(:user, phones: '923480093;645787879', last_name: 'M H')
       
        expect(run_rake_task).not_to eq('')
      end
    end
  end


  describe "#data_user" do
    let :run_rake_task do
      Rake::Task["import_data:address_phones"].reenable
      Rake.application.invoke_task "import_data:address_phones"
    end

    context "address_phones success" do
      it "address_phones" do       
        create(:address, phones: '923480093;645787879')
       
        expect(run_rake_task).not_to eq('')
      end
    end
  end

 
end