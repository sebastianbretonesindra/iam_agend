require 'rails_helper'
require 'uweb_api'
require 'madrid_api'

describe UwebApi do
  let(:service) { UwebApi }

  it "client" do
    aux = service.new

    expect(aux.client).not_to eq(nil)
  end

  it "request" do
    aux = service.new

    expect(aux.request({'xxxx'=>'xx'})).not_to eq(nil)
  end

  it "get users" do
    aux = service.new

    expect(aux.get_users('xxxx')).not_to eq(nil)
  end

  it "get user" do
    aux = service.new

    expect(aux.get_user('xxxx')).not_to eq(nil)
  end

  it "get application status" do
    aux = service.new

    expect(aux.get_application_status).not_to eq(nil)
  end

  it "get user status" do
    aux = service.new

    expect(aux.get_user_status('xxxx', Time.zone.now)).not_to eq(nil)
  end

  

end
