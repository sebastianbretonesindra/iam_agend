require 'rails_helper'
require 'directory_api'
require 'madrid_api'

describe DirectoryApi do
  let(:service) { DirectoryApi }

  it "client" do
    aux = service.new

    expect(aux.client).not_to eq(nil)
  end

  it "request" do
    aux = service.new

    expect(aux.request({'xxxx'=>'xx'})).not_to eq(nil)
  end

  it "get units" do
    aux = service.new

    expect(aux.get_units('xxxx')).to eq(nil)
  end

  it "get unit" do
    aux = service.new

    expect(aux.get_unit('xxxx')).to eq(nil)
  end

  it "create_tree" do
    aux = service.new

    expect(aux.create_tree('xxxx')).not_to eq(nil)

    area = create(:area, active: true)
    data = {}
    data["ID_UNIDAD"] = area.internal_id
    data["DENOMINACION"] =area.title
    expect(aux.create_tree(data)).not_to eq(nil)


    data["DENOMINACION"] ="222333xxxx"
    data["AREA"] = "#{area.internal_id}56"
    expect(aux.create_tree(data)).not_to eq(nil)
  end
  

end
