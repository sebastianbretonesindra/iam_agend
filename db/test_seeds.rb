require 'database_cleaner'
DatabaseCleaner.clean_with :truncation

# Areas
main_area = Area.create(title: 'IAM', active: 1)
area = Area.create(title: 'IAM servicio de portales y contenidos', parent: main_area, active: 1)
sub_area = Area.create(title: 'IAM departamento de gestion de contenidos y documentos', parent: area, active: 1)

# Admin
admin_user = User.create(password: '12345678', email: 'admin@agendas.dev', first_name: 'Admin', last_name: 'Istrator', active: 1)
admin_user.admin!

# Users who do not manage holders, will create the first event
user_without_holders = User.create(password: '12345678', email: 'pepe@agendas.dev', first_name: 'Pepe', last_name: 'Perez', active: 1)
user_without_holders.user!

# Users who manage holders
user_with_holders = User.create(password: '12345678', email: 'catalina@agendas.dev', first_name: 'Catalina', last_name: 'Perez', active: 1)
user_with_holders.user!
user_with_holders2 = User.create(password: '12345678', email: 'angelita@agendas.dev', first_name: 'Angelita', last_name: 'Gomez', active: 1)
user_with_holders2.user!

# Holder
holder_several_positions = Holder.create(first_name: 'Pilar', last_name: 'Lopez')
holder_one_position = Holder.create(first_name: 'Teresa', last_name: 'Ruiz')
holder_one_position2 = Holder.create(first_name: 'Maria José', last_name: 'Hernández')
holder_orphan_position = Holder.create(first_name: 'Carmelina', last_name: 'Cabezas')

# Position
Position.create(title: 'Director/a', to: Time.zone.now - 1.month, area_id: sub_area.id, holder_id: holder_several_positions.id)
sub_director = Position.create(title: 'Subdirector/a', to: nil, area_id: sub_area.id, holder_id: holder_several_positions.id)
Position.create(title: 'Jefe/a de seccion', to: nil, area_id: sub_area.id, holder_id: holder_several_positions.id)
commerce_boss = Position.create(title: 'Jefe negociado', to: nil, area_id: sub_area.id, holder_id: holder_one_position.id)
proyect_boss = Position.create(title: 'Jefa de proyecto', to: nil, area_id: sub_area.id, holder_id: holder_one_position2.id)
secretary = Position.create(title: 'Secretaria', to: nil, area_id: sub_area.id, holder_id: holder_orphan_position.id)

# Event :title, :position, :scheduled, :location, :lobby_activity, :published_at,
open_government = Event.create(location: 'xxxx', title: 'Gobierno Abierto',
                               description: 'El Gobierno Abierto tiene como objetivo que la ciudadanía colabore en la creación y mejora'\
                               ' de servicios públicos y en el robustecimiento de la transparencia y la rendición de cuentas.',
                               scheduled: rand(0..1) == 1 ? Faker::Time.forward(60, :day) : Faker::Time.backward(100, :morning),
                               user: User.first, position: Position.first, lobby_activity: false, published_at: Time.zone.today)
registration_offices = Event.create(location: 'xxxx', title: 'Oficinas de registro',
                                    description: 'Las oficinas de registro son los lugares que utiliza el ciudadano para presentar las'\
                                    ' solicitudes, escritos y comunicaciones que van dirigidos a las Administraciones Públicas. Asimismo,'\
                                    ' es el lugar que utiliza la Administración para registrar los documentos que remite al ciudadano, a'\
                                    ' entidades privadas o a la propia Administración.',
                                    scheduled: rand(0..1) == 1 ? Faker::Time.forward(60, :day) : Faker::Time.backward(100, :morning),
                                    user: user_without_holders, position: commerce_boss, lobby_activity: false, published_at: Time.zone.today)
online_registration = Event.create(location: 'xxxx', title: 'Registro Electrónico',
                                   description: 'El Registro Electrónico es un punto para la presentación de documentos para su'\
                                   ' tramitación con destino a cualquier órgano administrativo de la Administración General del Estado,'\
                                   ' Organismo público o Entidad vinculado o dependiente a éstos, de acuerdo a lo dispuesto en la Ley'\
                                   ' 39/2015 , de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas. ',
                                   scheduled: rand(0..1) == 1 ? Faker::Time.forward(60, :day) : Faker::Time.backward(100, :morning),
                                   user: user_without_holders, position: proyect_boss, lobby_activity: false, published_at: Time.zone.today)
political_transparency = Event.create(location: 'xxxxx', title: 'Transparencia política',
                                      description: 'Transparencia política es la obligación de los gobiernos de dar cuenta a los'\
                                      'ciudadanos de todos sus actos, especialmente del uso del dinero público y prevenir así los casos de'\
                                      ' corrupción',
                                      scheduled: rand(0..1) == 1 ? Faker::Time.forward(60, :day) : Faker::Time.backward(100, :morning),
                                      user: user_without_holders, position: secretary, lobby_activity: false, published_at: Time.zone.today)

# Participant
Participant.create(position_id: commerce_boss.id, event_id: political_transparency.id)
Participant.create(position_id: proyect_boss.id, event_id: political_transparency.id)
Participant.create(position_id: secretary.id, event_id: open_government.id)
Participant.create(position_id: secretary.id, event_id: online_registration.id)
Participant.create(position_id: secretary.id, event_id: registration_offices.id)

# Manage
Manage.create(holder_id: holder_one_position.id, user: user_with_holders)
Manage.create(holder_id: holder_several_positions.id, user: user_with_holders2)

# Attendees
Attendee.create(event: open_government, name: Faker::Name.name, position: Faker::Name.title, company: Faker::Company.name)
Attendee.create(event: online_registration, name: Faker::Name.name, position: Faker::Name.title, company: Faker::Company.name)
Attendee.create(event: registration_offices, name: Faker::Name.name, position: Faker::Name.title, company: Faker::Company.name)

# Attachments
Attachment.create(title: 'PDF Attachment', file: File.open('./spec/fixtures/dummy.pdf'), public: true, event: registration_offices)
Attachment.create(title: 'JPG Attachment', file: File.open('./spec/fixtures/dummy.jpg'), public: true, event: registration_offices)

#Interests
interest_1 = Interest.find_or_create_by(name:"Actividad económica y empresarial", code: 1)
interest_2 = Interest.find_or_create_by(name:"Actividad normativa y de regulación", code: 2)
interest_3 = Interest.find_or_create_by(name:"Administración de personal y recursos humanos", code: 3)
interest_4 = Interest.find_or_create_by(name:"Administración electrónica", code: 4)
interest_5 = Interest.find_or_create_by(name:"Administración económica, financiera y tributaria de la Ciudad", code: 5)
interest_6 = Interest.find_or_create_by(name:"Atención a la ciudadanía", code: 6)
interest_7 = Interest.find_or_create_by(name:"Comercio", code: 7)
interest_8 = Interest.find_or_create_by(name:"Consumo", code: 8)
interest_9 = Interest.find_or_create_by(name:"Cultura (bibliotecas, archivos, museos, patrimonio histórico artístico, etc.)", code: 8)
interest_10 = Interest.find_or_create_by(name:"Deportes", code: 8)
interest_11 = Interest.find_or_create_by(name:"Desarrollo tecnológico", code: 8)
interest_12 = Interest.find_or_create_by(name:"Educación y Juventud", code: 8)
interest_13 = Interest.find_or_create_by(name:"Emergencias y seguridad", code: 8)
interest_14 = Interest.find_or_create_by(name:"Empleo", code: 8)
interest_15 = Interest.find_or_create_by(name:"Medio Ambiente", code: 8)
interest_16 = Interest.find_or_create_by(name:"Medios de comunicación", code: 8)
interest_17 = Interest.find_or_create_by(name:"Movilidad, transporte y aparcamientos", code: 8)
interest_18 = Interest.find_or_create_by(name:"Salud", code: 8)
interest_19 = Interest.find_or_create_by(name:"Servicios sociales", code: 8)
interest_20 = Interest.find_or_create_by(name:"Transparencia y participación ciudadana", code: 8)
interest_21 = Interest.find_or_create_by(name:"Turismo", code: 8)
interest_22 = Interest.find_or_create_by(name:"Urbanismo", code: 8)
interest_23 = Interest.find_or_create_by(name:"Vivienda", code: 8)

# Categories
[
  'Persona física',
  'Entidad sin ánimo de lucro',
  'Entidad con ánimo de lucro'
].each do |name|
  Category.find_or_create_by(name: name, display: true,new_category: true, code: 1)
end
[
  'Entidades privadas sin ánimo de lucro',
  'Entidades representativas de intereses colectivos',
  'Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente',
  'Organizaciones empresariales, colegios profesionales y demás entidades representativas de intereses colectivos',
  'Entidades organizadoras de actos sin ánimo lucrativo',
  'Organizaciones no gubernamentales',
  'Grupos de reflexión e instituciones académicas y de investigación',
  'Organizaciones que representan a comunidades religiosas',
  'Organizaciones que representan a autoridades municipales',
  'Organizaciones que representan a autoridades regionales',
  'Organismos públicos o mixtos'
].each do |name|
  Category.find_or_create_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad sin ánimo de lucro').try(:id))
end
[
  'Empresas y agrupaciones comerciales, empresariales y profesionales',
  'Consultorías profesionales',
  'Asociaciones comerciales, empresariales y profesionales',
  'Coaliciones y estructuras temporales con fines de lucro',
  'Entidades organizadoras de actos con ánimo de lucro',
  'Cualquier otra entidad con ánimo de lucro'
].each do |name|
  Category.find_or_create_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad con ánimo de lucro').try(:id))
end

#Category
category_1 = Category.first
category_2 = Category.second
category_3 = Category.third

# RegisteredLobby
registered_lobbies = ['Ninguno',
  'Comunidad de Madrid',
  'Generalidad Catalunya',
  'CNMC',
  'Castilla-La Mancha',
  'Unión Europea',
  'Otro']

registered_lobbies.each do |name|
  RegisteredLobby.find_or_create_by(name: name, code: 1)
end

DataPreference.find_or_create_by(title: "min_length",content_data: 0,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
DataPreference.find_or_create_by(title: "expired_year",content_data: 2,type_data: 1) if DataPreference.find_by(title: "expired_year").blank?
DataPreference.find_or_create_by(title: "alert_first",content_data: 60,type_data: 1) if DataPreference.find_by(title: "alert_first").blank?
DataPreference.find_or_create_by(title: "alert_second",content_data: 30,type_data: 1) if DataPreference.find_by(title: "alert_second").blank?


# Organization
user_lobby_1 = User.create(password: '12345678', email: 'lobby@agendas.dev', first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_1 = Organization.create(identifier: '21789', name: Faker::Company.name, entity_type: :lobby, inscription_date: Date.current - 1.year, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_1, category: category_1, reference: 'W45Y',
                                     address: Address.create, address_type: "Calle", number: 'xxxxx', gateway: "B", stairs: "C", floor: "2", door: "A", postal_code: 'xxxx', town: 'xxxx', province: 'xxxxxx', neighbourhood: "My neighbourhood",
                                     district: "Distrito A", approach: "Descripción de como llegar", scope: "Ámbito A", country: 'xxxx', phones: "923454545", email: "organization@email.com", description: "Hacer lobby a tope!", web: "http://www.organization.com", registered_lobby_ids: [RegisteredLobby.first.id],
                                     fiscal_year: 2018, range_fund: :range_1, subvention: true, contract: false, associations_count: 200, members_count: 123)

user_lobby_2 = User.create(password: '12345678', email: 'lob_by@agendas.dev', first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_2 = Organization.create(identifier: '02',name: Faker::Company.name, entity_type: :lobby, inscription_date: Date.current - 2.years, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_2, category: category_2)

user_lobby_3 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_3 = Organization.create(identifier: '03',name: Faker::Company.name, entity_type: :lobby, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_3, category: category_3)

user_lobby_4 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_4 = Organization.create(identifier: '04',name: Faker::Company.name, entity_type: :lobby, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_4, category: category_1)

user_lobby_5 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_5 = Organization.create(identifier: '05',name: Faker::Company.name, entity_type: :association, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_5, category: category_2)

user_lobby_6 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_6 = Organization.create(identifier: '06',name: Faker::Company.name, entity_type: :association, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_6, category: category_1)

user_lobby_7 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_7 = Organization.create(identifier: '07',name: Faker::Company.name, entity_type: :association, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_7, category: category_3)

user_lobby_8 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_8 = Organization.create(identifier: '08',name: Faker::Name.name, first_surname: Faker::Name.last_name, second_surname: Faker::Name.last_name, entity_type: :association, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_8, category: category_2)

user_lobby_9 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_9 = Organization.create(identifier: '09',name: Faker::Name.name, first_surname: Faker::Name.last_name, second_surname: Faker::Name.last_name, entity_type: :federation, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_9, category: category_3)

user_lobby_10 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_10 = Organization.create(identifier: '010',name: Faker::Name.name, first_surname: Faker::Name.last_name, second_surname: Faker::Name.last_name, entity_type: :federation, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_10, category: category_1)

user_lobby_11 = User.create(password: '12345678', email: Faker::Internet.email, first_name: 'Pepe', last_name: 'Perez', active: 1, role: 2)
organization_11 = Organization.create(identifier: '011',name: Faker::Name.name, first_surname: Faker::Name.last_name, second_surname: Faker::Name.last_name, entity_type: :federation, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_11, category: category_2)

30.times do
  Organization.create(identifier: '01'+Faker::Name.name,name: Faker::Name.name, first_surname: Faker::Name.last_name, second_surname: Faker::Name.last_name, entity_type: :lobby, inscription_date: Date.yesterday, certain_term: true, code_of_conduct_term: true, gift_term: true, lobby_term: true, public_term: true,  other_term:  true, user: user_lobby_11, category: category_2)
end

#Legal Representant
LegalRepresentant.create(identifier: "43138883z", name: "Name", first_surname: "Surname", email: "email@legal.com", organization: organization_1)

#Agents
Agent.create(identifier: "43138882z", name: "Name1", from: Date.yesterday, organization: organization_1, allow_public_data: true)
Agent.create(identifier: "43138881z", name: "Name2", from: Date.yesterday, organization: organization_1, allow_public_data: true)

#Represented Entities
RepresentedEntity.create(identifier: "43138880z", name: "Name3", from: Date.yesterday, fiscal_year: 2017, organization: organization_1, range_fund: :range_2)
RepresentedEntity.create(identifier: "43138879z", name: "Name4", from: Date.yesterday, fiscal_year: 2017, organization: organization_1, range_fund: :range_3)

#OrganizationInterest
OrganizationInterest.create(organization: organization_1, interest: interest_1)
OrganizationInterest.create(organization: organization_1, interest: interest_2)
OrganizationInterest.create(organization: organization_1, interest: interest_3)

data_json_cc= "transparenciaydatos@madrid.es"
data_json_cco ="creaspodhe@madrid.es"




        ManageEmail.create!(type_data: "Holder", sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_holder.subject'), 
            email_body: I18n.t("mailers.new_holder.body")) 
          
        ManageEmail.create!(type_data: 'AuxManager', sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_aux.subject'), 
            email_body: I18n.t("mailers.new_aux.body"))

        ManageEmail.create!(type_data: 'Events', sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.events.subject'), 
            email_body: I18n.t("mailers.events.body"))
        
        ManageEmail.create!(type_data: 'Organizations', sender: "no-reply@madrid.es",
            fields_cc: "", 
            fields_cco: "registrodelobbies@madrid.es",
            subject: I18n.t('mailers.organizations.subject'), 
            email_body: I18n.t("mailers.organizations.body"))

