class AddColumnCommentsToPositions < ActiveRecord::Migration
  def up
    add_column :positions, :comments, :text
  end

  def down 
    remove_column :positions, :comments, :text
  end
end
