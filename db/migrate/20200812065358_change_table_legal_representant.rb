class ChangeTableLegalRepresentant < ActiveRecord::Migration
  def change
    add_reference :legal_representants, :entity, polymorphic: true, index: true

    execute %"UPDATE legal_representants SET entity_type='Organization', entity_id=organization_id"
  end
end
