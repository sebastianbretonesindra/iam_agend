class AddCodeToRegisteredLobbies < ActiveRecord::Migration
  def change
    add_column :registered_lobbies, :code, :integer
  end
end
