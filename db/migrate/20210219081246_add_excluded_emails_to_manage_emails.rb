class AddExcludedEmailsToManageEmails < ActiveRecord::Migration
  def change
    add_column :manage_emails, :excluded_emails, :text
  end
end
