class CreateNotificationEffects < ActiveRecord::Migration
  def change
    create_table :notification_effects do |t|
      t.text :identifier
      t.text :name
      t.text :first_surname
      t.text :second_surname
      t.text :business_name
      t.references :address, index: true, foreign_key: true
      t.text :email
      t.references :organization, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
