class CreateDataPreferences < ActiveRecord::Migration
  def change
    create_table :data_preferences do |t|
      t.string :title 
      t.string :content_data
      t.integer :type_data  
      t.timestamps null: false
    end
  end
end
