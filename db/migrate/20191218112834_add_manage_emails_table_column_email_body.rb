class AddManageEmailsTableColumnEmailBody < ActiveRecord::Migration
  def change
    add_column :manage_emails, :email_body, :text, :limit => 16777215
  end
end
