class AddDniUsers < ActiveRecord::Migration
  def up
    add_column :users, :identificator, :string
  end

  def down
    remove_column :users, :identificator, :string
  end
end
