class AddColumnsCategories < ActiveRecord::Migration
  def change
    add_column :categories, :code, :string


    [
      {name: "Consultoría profesional y despachos de abogados", code: "PRO" },
      {name: "Empresas", code: "EMP"},
      {name: "Asociaciones/Fundaciones", code: "ASO/FUN"},
      {name: "Sindicatos y organizaciones profesionales", code: "SIN"},
      {name: "Organizaciones empresariales", code: "ORG"},
      {name: "ONGs y plataformas sin personalidad jurídica", code: "ONG"},
      {name: "Universidades y centros de investigación", code: "UNI"},
      {name: "Corporaciones de Derecho Público (colegios profesionales, cámaras oficiales, etc.)", code: "COR"},
      {name: "Iglesia y otras confesiones", code: "IGL"},
      {name: "Otro tipo de sujetos", code: "OTR"}].each do |c|
      cat = Category.find_by(name: c[:name])

      if !cat.blank?
        cat.code = c[:code]
        cat.save
      end
    end

  end
end
