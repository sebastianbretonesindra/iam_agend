class ChangeLegalRepresentants < ActiveRecord::Migration
  def change
    add_reference :legal_representants, :address, :index => true, foreign_key: true
    add_column :legal_representants, :business_name, :text
  end
end
