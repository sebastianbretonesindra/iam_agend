class CreateSendEmails < ActiveRecord::Migration
  def change
    create_table :send_emails do |t|
      t.boolean :auto_send
      t.datetime :auto_date
      t.string :auto_hour
      t.integer :auto_frecuence
      t.integer :auto_user_type
      t.string :auto_excluded_emails
      t.boolean :manual_send
      t.datetime :manual_from_date
      t.datetime :manual_to_date
      t.string :manual_hour
      t.integer :manua_user_type
      t.string :manua_excluded_emails
      
      t.timestamps null: false
    end
  end
end
