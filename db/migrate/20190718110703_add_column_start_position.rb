class AddColumnStartPosition < ActiveRecord::Migration
  def change
    add_column :positions, :start, :datetime, null: false, default: Time.now
  end
end
