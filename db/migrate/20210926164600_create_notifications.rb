class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.datetime :change_status_at
      t.string :status
      t.json :origin_data
      t.json :modification_data
      t.string :type_anotation
      t.text :validations_description
      t.text :reject_reasons
      t.text :recover_reasons
      t.text :observations
      
      t.timestamps null: false
    end
  end
end
