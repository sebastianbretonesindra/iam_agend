class CreateOrganizationsDocuments < ActiveRecord::Migration
  def change
    create_table :organizations_documents do |t|
      t.references :organization, index: true, foreign_key: true
      t.string :id_document
      t.integer :type_document
      
      t.timestamps null: false
    end
  end
end
