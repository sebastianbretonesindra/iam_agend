class CreateRangeSubventions < ActiveRecord::Migration
  def change
    create_table :range_subventions do |t|
      t.references :item, polymorphic: true
      t.float :import
      t.text :entity_name

      t.timestamps null: false
    end
  end
end
