class CreateHolderStatusHistories < ActiveRecord::Migration
  def change
    create_table :holder_status_histories do |t|
      t.string :status, null: false
      t.datetime :status_at, null: false, default: Time.now
      t.references :position, index: true
      t.string :description
      t.timestamps null: false
    end
  end
end
