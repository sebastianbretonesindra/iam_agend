class CreateOrganizationsRenewals < ActiveRecord::Migration
  def change
    create_table :organizations_renewals do |t|
      t.references :organization, index: true, foreign_key: true
      t.datetime :renovation_date
      t.datetime :expiration_date
      t.boolean :active, default: true
      
      t.timestamps null: false
    end
  end
end
