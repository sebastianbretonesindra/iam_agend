class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.text :country
      t.text :province
      t.text :town
      t.text :address_type
      t.text :address
      t.text :number
      t.text :gateway
      t.text :stairs
      t.text :floor
      t.text :door
      t.integer :postal_code
      t.text :email
      t.text :phones
      t.text :phones_alt

      t.timestamps null: false
    end
  end
end
