class AddBusinessName < ActiveRecord::Migration
  def change
    add_column :legal_representants, :business_name, :string
    add_column :legal_representants, :check_mail, :boolean
    add_column :legal_representants, :check_sms, :boolean

    add_column :notification_effects, :business_name, :string
    add_column :notification_effects, :check_mail, :boolean
    add_column :notification_effects, :check_sms, :boolean
  end
end
