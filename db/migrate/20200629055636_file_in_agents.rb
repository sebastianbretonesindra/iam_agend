class FileInAgents < ActiveRecord::Migration
  def up
    add_attachment :agents, :file_allow_data
  end

  def down
    remove_attachment :agents, :file_allow_data
  end
end
