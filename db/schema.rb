# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20220216101239) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.text     "country"
    t.text     "province"
    t.text     "town"
    t.text     "address_type"
    t.text     "address"
    t.text     "number"
    t.text     "gateway"
    t.text     "stairs"
    t.text     "floor"
    t.text     "door"
    t.integer  "postal_code"
    t.text     "email"
    t.text     "phones"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.text     "address_number_type"
    t.string   "movil_phone"
  end

  create_table "agents", force: :cascade do |t|
    t.string   "identifier"
    t.string   "name"
    t.string   "first_surname"
    t.string   "second_surname"
    t.date     "from"
    t.date     "to"
    t.text     "public_assignments"
    t.integer  "organization_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "allow_public_data"
    t.string   "file_allow_data_file_name"
    t.string   "file_allow_data_content_type"
    t.integer  "file_allow_data_file_size"
    t.datetime "file_allow_data_updated_at"
  end

  add_index "agents", ["organization_id"], name: "index_agents_on_organization_id", using: :btree

  create_table "areas", force: :cascade do |t|
    t.integer  "internal_id"
    t.string   "title"
    t.integer  "active"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "ancestry"
    t.string   "internal_code"
  end

  add_index "areas", ["ancestry"], name: "index_areas_on_ancestry", using: :btree

  create_table "attachments", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "event_id"
    t.text     "description"
    t.boolean  "public"
    t.integer  "agent_id"
    t.integer  "organization_id"
    t.integer  "event_agent_id"
  end

  add_index "attachments", ["agent_id"], name: "index_attachments_on_agent_id", using: :btree
  add_index "attachments", ["event_agent_id"], name: "index_attachments_on_event_agent_id", using: :btree
  add_index "attachments", ["event_id"], name: "index_attachments_on_event_id", using: :btree
  add_index "attachments", ["organization_id"], name: "index_attachments_on_organization_id", using: :btree

  create_table "attendees", force: :cascade do |t|
    t.string   "name"
    t.string   "position"
    t.string   "company"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "event_id"
  end

  add_index "attendees", ["event_id"], name: "index_attendees_on_event_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "display"
    t.integer  "category_id"
    t.boolean  "new_category", default: false
    t.integer  "code"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "data_preferences", force: :cascade do |t|
    t.string   "title"
    t.string   "content_data"
    t.integer  "type_data"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "event_agents", force: :cascade do |t|
    t.integer "event_id"
    t.string  "name"
  end

  add_index "event_agents", ["event_id"], name: "index_event_agents_on_event_id", using: :btree

  create_table "event_represented_entities", force: :cascade do |t|
    t.integer "event_id"
    t.string  "name"
  end

  add_index "event_represented_entities", ["event_id"], name: "index_event_represented_entities_on_event_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "scheduled"
    t.integer  "user_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "position_id"
    t.string   "location"
    t.string   "slug"
    t.integer  "status",                  default: 0
    t.boolean  "lobby_activity"
    t.text     "notes"
    t.string   "canceled_reasons"
    t.date     "published_at"
    t.date     "canceled_at"
    t.string   "organization_name"
    t.text     "lobby_scheduled"
    t.text     "general_remarks"
    t.string   "lobby_contact_firstname"
    t.string   "lobby_contact_lastname"
    t.string   "lobby_contact_email"
    t.string   "lobby_contact_phone"
    t.text     "manager_general_remarks"
    t.integer  "organization_id"
    t.date     "declined_at"
    t.string   "declined_reasons"
    t.date     "accepted_at"
    t.boolean  "multi_attendees"
    t.boolean  "old_event_lobby",         default: true
  end

  add_index "events", ["position_id"], name: "index_events_on_position_id", using: :btree
  add_index "events", ["slug"], name: "index_events_on_slug", unique: true, using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "exports", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.jsonb    "fields",        default: {}, null: false
    t.jsonb    "filter_fields", default: {}, null: false
  end

  add_index "exports", ["fields"], name: "index_exports_on_fields", using: :gin
  add_index "exports", ["filter_fields"], name: "index_exports_on_filter_fields", using: :gin

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "holder_status_histories", force: :cascade do |t|
    t.string   "status",                                      null: false
    t.datetime "status_at",   default: '2018-12-17 11:07:46', null: false
    t.integer  "position_id"
    t.string   "description"
    t.string   "email_send"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "holder_status_histories", ["position_id"], name: "index_holder_status_histories_on_position_id", using: :btree

  create_table "holders", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "user_key"
    t.string   "personal_code"
  end

  create_table "interests", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "code"
  end

  create_table "legal_representants", force: :cascade do |t|
    t.string   "identifier"
    t.string   "name"
    t.string   "first_surname"
    t.string   "second_surname"
    t.string   "phones"
    t.string   "email"
    t.integer  "organization_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "address_id"
    t.integer  "entity_id"
    t.string   "entity_type"
    t.string   "business_name"
    t.boolean  "check_mail"
    t.boolean  "check_sms"
    t.string   "identifier_type"
  end

  add_index "legal_representants", ["address_id"], name: "index_legal_representants_on_address_id", using: :btree
  add_index "legal_representants", ["entity_type", "entity_id"], name: "index_legal_representants_on_entity_type_and_entity_id", using: :btree
  add_index "legal_representants", ["organization_id"], name: "index_legal_representants_on_organization_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "action"
    t.integer  "actionable_id"
    t.string   "actionable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "logs", ["actionable_id", "actionable_type"], name: "index_logs_on_actionable_id_and_actionable_type", using: :btree
  add_index "logs", ["organization_id"], name: "index_logs_on_organization_id", using: :btree

  create_table "manage_emails", force: :cascade do |t|
    t.string   "type_data"
    t.string   "sender"
    t.string   "fields_cc"
    t.string   "fields_cco"
    t.text     "subject"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "email_body"
    t.text     "excluded_emails"
    t.integer  "users"
  end

  create_table "manages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "holder_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "manages", ["holder_id"], name: "index_manages_on_holder_id", using: :btree
  add_index "manages", ["user_id"], name: "index_manages_on_user_id", using: :btree

  create_table "newsletters", force: :cascade do |t|
    t.string   "subject"
    t.string   "body"
    t.datetime "sent_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "interest_id", null: false
  end

  add_index "newsletters", ["interest_id"], name: "index_newsletters_on_interest_id", using: :btree

  create_table "newsletters_interests", force: :cascade do |t|
    t.integer  "newsletter_id"
    t.integer  "interest_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "newsletters_interests", ["interest_id"], name: "index_newsletters_interests_on_interest_id", using: :btree
  add_index "newsletters_interests", ["newsletter_id"], name: "index_newsletters_interests_on_newsletter_id", using: :btree

  create_table "notification_effects", force: :cascade do |t|
    t.text     "identifier"
    t.text     "name"
    t.text     "first_surname"
    t.text     "second_surname"
    t.integer  "address_id"
    t.text     "email"
    t.integer  "organization_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "business_name"
    t.boolean  "check_mail"
    t.boolean  "check_sms"
    t.string   "identifier_type"
  end

  add_index "notification_effects", ["address_id"], name: "index_notification_effects_on_address_id", using: :btree
  add_index "notification_effects", ["organization_id"], name: "index_notification_effects_on_organization_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.datetime "change_status_at"
    t.string   "status"
    t.json     "origin_data"
    t.json     "modification_data"
    t.string   "type_anotation"
    t.text     "validations_description"
    t.text     "reject_reasons"
    t.text     "recover_reasons"
    t.text     "observations"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "organization_interests", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "interest_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "organization_interests", ["interest_id"], name: "index_organization_interests_on_interest_id", using: :btree
  add_index "organization_interests", ["organization_id"], name: "index_organization_interests_on_organization_id", using: :btree

  create_table "organization_registered_lobbies", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "registered_lobby_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "organization_registered_lobbies", ["organization_id"], name: "index_organization_registered_lobbies_on_organization_id", using: :btree
  add_index "organization_registered_lobbies", ["registered_lobby_id"], name: "index_organization_registered_lobbies_on_registered_lobby_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "reference"
    t.string   "identifier"
    t.string   "name"
    t.string   "first_surname"
    t.string   "second_surname"
    t.string   "address_type"
    t.string   "old_address"
    t.string   "number"
    t.string   "gateway"
    t.string   "stairs"
    t.string   "floor"
    t.string   "door"
    t.string   "postal_code"
    t.string   "town"
    t.string   "province"
    t.string   "phones"
    t.string   "email"
    t.integer  "category_id"
    t.string   "description",                      default: ""
    t.string   "web"
    t.integer  "registered_lobbies"
    t.integer  "fiscal_year"
    t.integer  "range_fund"
    t.boolean  "subvention",                       default: false
    t.boolean  "contract",                         default: false
    t.boolean  "certain_term"
    t.boolean  "code_of_conduct_term"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "inscription_reference"
    t.datetime "inscription_date"
    t.integer  "entity_type"
    t.string   "neighbourhood"
    t.string   "district"
    t.string   "scope"
    t.integer  "associations_count"
    t.integer  "members_count"
    t.string   "approach"
    t.datetime "invalidated_at"
    t.datetime "canceled_at"
    t.string   "invalidated_reasons"
    t.string   "country"
    t.date     "modification_date"
    t.boolean  "gift_term"
    t.boolean  "lobby_term"
    t.string   "address_number_type"
    t.date     "termination_date"
    t.boolean  "own_lobby_activity",               default: false
    t.boolean  "foreign_lobby_activity",           default: false
    t.integer  "address_id"
    t.datetime "renovation_date"
    t.boolean  "in_group_public_administration",   default: false
    t.text     "text_group_public_administration"
    t.boolean  "subvention_public_administration", default: false
    t.text     "contract_turnover"
    t.text     "contract_total_budget"
    t.text     "contract_breakdown"
    t.text     "contract_financing"
    t.boolean  "public_term",                      default: false
    t.boolean  "communication_term",               default: false
    t.boolean  "other_term",                       default: false
    t.boolean  "pending",                          default: false
    t.string   "other_registered_lobby_desc"
    t.string   "business_name"
    t.boolean  "check_mail"
    t.boolean  "check_sms"
    t.string   "identifier_type"
  end

  add_index "organizations", ["address_id"], name: "index_organizations_on_address_id", using: :btree
  add_index "organizations", ["category_id"], name: "index_organizations_on_category_id", using: :btree

  create_table "organizations_documents", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "id_document"
    t.integer  "type_document"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "organizations_documents", ["organization_id"], name: "index_organizations_documents_on_organization_id", using: :btree

  create_table "organizations_newsletters", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "newsletter_id"
    t.string   "email_send"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "organizations_newsletters", ["newsletter_id"], name: "index_organizations_newsletters_on_newsletter_id", using: :btree
  add_index "organizations_newsletters", ["organization_id"], name: "index_organizations_newsletters_on_organization_id", using: :btree

  create_table "organizations_renewals", force: :cascade do |t|
    t.integer  "organization_id"
    t.datetime "renovation_date"
    t.datetime "expiration_date"
    t.boolean  "active",          default: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "organizations_renewals", ["organization_id"], name: "index_organizations_renewals_on_organization_id", using: :btree

  create_table "participants", force: :cascade do |t|
    t.integer  "position_id"
    t.integer  "event_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "participants", ["event_id"], name: "index_participants_on_event_id", using: :btree
  add_index "participants", ["position_id"], name: "index_participants_on_position_id", using: :btree

  create_table "positions", force: :cascade do |t|
    t.string   "title"
    t.datetime "to"
    t.integer  "area_id"
    t.integer  "holder_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.datetime "start",      default: '2019-07-18 11:08:31', null: false
    t.text     "comments"
  end

  add_index "positions", ["area_id"], name: "index_positions_on_area_id", using: :btree
  add_index "positions", ["holder_id"], name: "index_positions_on_holder_id", using: :btree

  create_table "provinces", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.string   "title"
    t.text     "answer"
    t.integer  "position",   default: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "range_subventions", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "item_type"
    t.string   "import"
    t.text     "entity_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "registered_lobbies", force: :cascade do |t|
    t.text     "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "code"
  end

  create_table "represented_entities", force: :cascade do |t|
    t.string   "identifier"
    t.string   "name"
    t.string   "first_surname"
    t.string   "second_surname"
    t.date     "from"
    t.date     "to"
    t.integer  "organization_id"
    t.integer  "fiscal_year"
    t.integer  "range_fund"
    t.boolean  "subvention",                       default: false
    t.boolean  "contract",                         default: false
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.boolean  "in_group_public_administration",   default: false
    t.text     "text_group_public_administration"
    t.boolean  "subvention_public_administration", default: false
    t.text     "contract_turnover"
    t.text     "contract_total_budget"
    t.text     "contract_breakdown"
    t.text     "contract_financing"
    t.integer  "address_id"
    t.integer  "category_id"
    t.string   "business_name"
    t.boolean  "check_mail"
    t.boolean  "check_sms"
    t.string   "identifier_type"
  end

  add_index "represented_entities", ["address_id"], name: "index_represented_entities_on_address_id", using: :btree
  add_index "represented_entities", ["category_id"], name: "index_represented_entities_on_category_id", using: :btree
  add_index "represented_entities", ["organization_id"], name: "index_represented_entities_on_organization_id", using: :btree

  create_table "road_types", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "send_emails", force: :cascade do |t|
    t.boolean  "auto_send"
    t.string   "auto_hour"
    t.integer  "auto_frecuence"
    t.integer  "auto_user_type"
    t.string   "auto_excluded_emails"
    t.boolean  "manual_send"
    t.datetime "manual_from_date"
    t.datetime "manual_to_date"
    t.string   "manual_hour"
    t.integer  "manual_user_type"
    t.string   "manual_excluded_emails"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "auto_date"
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "role"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "active"
    t.string   "user_key"
    t.string   "phones"
    t.integer  "organization_id"
    t.datetime "deleted_at"
    t.integer  "role_view"
    t.boolean  "frist_init",             default: true
    t.boolean  "changed_password",       default: true
    t.string   "second_last_name"
    t.string   "movil_phone"
    t.string   "user_key_type"
    t.string   "identificator"
    t.string   "positions"
    t.string   "personal_code"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "agents", "organizations"
  add_foreign_key "attachments", "events"
  add_foreign_key "attachments", "organizations"
  add_foreign_key "attendees", "events"
  add_foreign_key "event_agents", "events"
  add_foreign_key "event_represented_entities", "events"
  add_foreign_key "events", "positions"
  add_foreign_key "events", "users"
  add_foreign_key "legal_representants", "addresses"
  add_foreign_key "legal_representants", "organizations"
  add_foreign_key "manages", "holders"
  add_foreign_key "manages", "users"
  add_foreign_key "newsletters", "interests"
  add_foreign_key "newsletters_interests", "interests"
  add_foreign_key "newsletters_interests", "newsletters"
  add_foreign_key "notification_effects", "addresses"
  add_foreign_key "notification_effects", "organizations"
  add_foreign_key "organization_interests", "interests"
  add_foreign_key "organization_interests", "organizations"
  add_foreign_key "organization_registered_lobbies", "organizations"
  add_foreign_key "organization_registered_lobbies", "registered_lobbies"
  add_foreign_key "organizations", "addresses"
  add_foreign_key "organizations", "categories"
  add_foreign_key "organizations_documents", "organizations"
  add_foreign_key "organizations_renewals", "organizations"
  add_foreign_key "participants", "events"
  add_foreign_key "participants", "positions"
  add_foreign_key "positions", "areas"
  add_foreign_key "positions", "holders"
  add_foreign_key "represented_entities", "addresses"
  add_foreign_key "represented_entities", "categories"
  add_foreign_key "represented_entities", "organizations"
  add_foreign_key "users", "organizations"
end
