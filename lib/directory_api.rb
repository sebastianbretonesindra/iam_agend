class DirectoryApi < MadridApi

  def client
    @client = Savon.client(wsdl: Rails.application.secrets.directory_api_endpoint,encoding: 'ISO-8859-1')
  end

  def request(params)
    h=Hash.new
    h[:aplicacion] = Rails.application.secrets.directory_api_app_key
    params.each do |k,v|
      h[k] = v
    end
    h
  end

  def get_units(codOrganico)
    data = data(:buscar_dependencias, {codOrganico: codOrganico})
    Hash.from_xml(data)['UNIDADES_ORGANIZATIVAS']
  rescue
  end

  def get_personal_data(numper)
    data = data(:datos_empleados, {i_pernr: numper})
    Hash.from_xml(data)['EMPLEADOS_ACTIVOS']['EMPLEADO']
  rescue
  end

  def get_unit(idUnidad)
    data = data(:consulta_datos_dependencia, {i_id_unidad: idUnidad})
    Hash.from_xml(data)['UNIDAD_ORGANIZATIVA']
  rescue
  end

  def create_tree (unit)
    exist = Area.find_by(internal_id: unit['ID_UNIDAD'], active: true)

    if !exist.blank?
      if !unit['DENOMINACION'].blank? &&  unit['DENOMINACION'].strip != exist.title
        area = Area.new
        exist.active = false
        unless exist.save
          puts "ERROR: El área con id - #{area.id} no se ha generado correctamente"
        end
      else
        area = exist
      end
    else
      area = Area.new
    end
    
    area.title = unit['DENOMINACION'].strip unless unit['DENOMINACION'].blank?
    area.internal_id = unit['ID_UNIDAD'].strip unless unit['ID_UNIDAD'].blank?
    area.internal_code = unit['COD_ORGANICO'].strip unless unit['COD_ORGANICO'].blank?
    area.active = true

    if !unit['ID_UNIDAD_PADRE'].blank? && unit['ID_UNIDAD_PADRE']!=unit['ID_UNIDAD']
      parent = get_unit(unit['ID_UNIDAD_PADRE'])
      area.parent = create_tree(parent) if !parent.blank?
    end

    unless area.save
      puts "ERROR: El área con id - #{area.id} no se ha guardado correctamente"
    end

    area
  end

end
