namespace :registered_lobby do
    desc "Rellenar códigos de RegisteredLobby"
    task insert_code: :environment do
        arr_name = ['Ninguno','Castilla-La Mancha','CNMC','Unión Europea','Generalidad de Cataluña','Comunidad de Madrid','Otro']
        arr_name.each_with_index do |name,index| 
            RegisteredLobby.find_by(name: name)&.update(code: (index+1))
        end
    end
end
