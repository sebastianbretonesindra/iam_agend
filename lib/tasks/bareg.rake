namespace :bareg do
    desc "pruebas"
    task iniciar_expediente: :environment do
        bareg = BaregApi.new

        bareg.inicioExpediente
    end

    task insertar_expediente: :environment do
        bareg = BaregApi.new

        bareg.insertarExpediente
    end

    task actualizar_expediente: :environment do
        bareg = BaregApi.new

        bareg.actualizarExpediente
    end

    task baja_expediente: :environment do
        bareg = BaregApi.new

        bareg.bajaExpediente
    end

end