namespace :import_data do
    desc "Imports everything"
    task all: ['import_data:settings', 'import_data:registered_lobbies', 'import_data:interests', 'import_data:categories','import_data:address']
  
    desc "Importa lobbies registrados si no existen"
    task :settings => :environment do
        DataPreference.find_or_create_by(title: "min_length",content_data: 0,type_data: 1) if DataPreference.find_by(title: "min_length").blank?
        DataPreference.find_or_create_by(title: "expired_year",content_data: 2,type_data: 1) if DataPreference.find_by(title: "expired_year").blank?
        DataPreference.find_or_create_by(title: "alert_first",content_data: 60,type_data: 1) if DataPreference.find_by(title: "alert_first").blank?
        DataPreference.find_or_create_by(title: "time_frame",content_data: 1,type_data: 3) if DataPreference.find_by(title: "time_frame").blank?
        DataPreference.find_or_create_by(title: "alert_second",content_data: 30,type_data: 1) if DataPreference.find_by(title: "alert_second").blank?
        DataPreference.find_or_create_by(title: "limit_events",content_data: 0,type_data: 4) if DataPreference.find_by(title: "limit_events").blank?
        DataPreference.find_or_create_by(title: "show_admin_limit",content_data: 0,type_data: 5) if DataPreference.find_by(title: "show_admin_limit").blank?
        DataPreference.find_or_create_by(title: "show_calendar",content_data: 'day',type_data: 3) if DataPreference.find_by(title: "show_calendar").blank?

    end

    desc "Importa histórico de renovaciones"
    task :renovation_historic => :environment do
        Organization.where("renovation_date is not null").each do |organization|
            if organization.organizations_renewals.find_by(renovation_date: organization.renovation_date).blank?
                organization.organizations_renewals.each do |ren|
                    ren.active = false
                    ren.save
                end
                renewal= OrganizationsRenewal.new(organization: organization,renovation_date: organization.renovation_date, expiration_date: organization.expired_date, active: true)
                organization.organizations_renewals << renewal
                if renewal.save

                    if organization.save
                        puts "Se ha guardado el histórico de #{organization.id}"
                    end
                else
                    puts "ERROR al guardar el histórico de #{organization.id}"
                end
            end
        end
    end 


    desc "Importa lobbies registrados si no existen"
    task :registered_lobbies_show => :environment do
        RegisteredLobby.where("id not in (?) OR name is null OR name = ''", Organization.joins(:registered_lobbies).select("registered_lobbies.id")).each {|x| x.destroy}

       

        Organization.all.each do |organization|
            other_registered_lobby = organization.registered_lobbies.
                where("name not in ('Ninguno', 'Comunidad de Madrid', 'Generalidad catalunya', 'Generalidad Catalunya', 'CNMC', 'Castilla-La Mancha', 'Unión Europea', 'Otro')").first

            organization.other_registered_lobby_desc = other_registered_lobby.try(:name)
            organization_registered = organization.organization_registered_lobbies.find_by(registered_lobby: other_registered_lobby)
            # organization_registered.destroy if !organization_registered.blank?
            # other_registered_lobby.destroy if !other_registered_lobby.blank?
            # organization.save

            puts "=" *30
            puts organization_registered.attributes
            puts "=" *20
            puts organization.other_registered_lobby_desc
            puts "=" *30
        end

        RegisteredLobby.where("name in ('Generalidad catalunya', 'Generalidad Catalunya')").each do |x|
            
                x.name = 'Generalidad de Cataluña'
                # x.save
                puts "=" *30
                puts x.name
                puts "=" *30
        end



        [
            'Ninguno',
            'Comunidad de Madrid',
            'Generalidad de Cataluña',
            'CNMC',
            'Castilla-La Mancha',
            'Unión Europea',
            'Otro'
        ].each do |name|
            puts "=" *30
            puts name
            puts RegisteredLobby.find_by(name: name).blank?
            puts "=" *30
            
        end

    end


    desc "Importa lobbies registrados si no existen"
    task :registered_lobbies => :environment do
        RegisteredLobby.where("id not in (?) OR name is null OR name = ''", Organization.joins(:registered_lobbies).select("registered_lobbies.id")).each {|x| x.destroy}

       

        Organization.all.each do |organization|
            other_registered_lobby = organization.registered_lobbies.
                where("name not in ('Ninguno', 'Comunidad de Madrid', 'Generalidad catalunya', 'Generalidad Catalunya', 'CNMC', 'Castilla-La Mancha', 'Unión Europea', 'Otro')").first

            organization.other_registered_lobby_desc = other_registered_lobby.try(:name)
            organization_registered = organization.organization_registered_lobbies.find_by(registered_lobby: other_registered_lobby)
            organization_registered.destroy if !organization_registered.blank?
            other_registered_lobby.destroy if !other_registered_lobby.blank?
            organization.save
        end

        RegisteredLobby.where("name in ('Generalidad catalunya', 'Generalidad Catalunya')").each do |x|
            
                x.name = 'Generalidad de Cataluña'
                x.save

        end

        [
            'Ninguno',
            'Comunidad de Madrid',
            'Generalidad de Cataluña',
            'CNMC',
            'Castilla-La Mancha',
            'Unión Europea',
            'Otro'
        ].each do |name|
            RegisteredLobby.find_or_create_by(name: name)
        end

    end

    desc "Importa intereses si no existen"
    task :interests => :environment do
        interest = Interest.find_by(name: "Actividad normativa y de regulación")
        if !interest.blank?
            interest.name = 'Otros'
            if interest.save
                puts "Se ha actualizado correctamente el interés: Otros"
            end
        end

        [
            'Actividad económica y empresarial',
            'Administración económica, financiera y tributaria de la ciudad',
            'Atención a la ciudadanía',
            'Consumo',
            'Cultura (bibliotecas, archivos, museos, patrimonio histórico artístico, etc.)',
            'Desarrollo empresarial y emprendimiento',
            'Distritos',
            'Emergencias, seguridad y protección civil',
            'Medio ambiente y sostenibilidad ambiental',
            'Movilidad, transporte y aparcamientos',
            'Patrimonio e inventario',
            'Salud',
            'Transparencia y participación ciudadana',
            'Urbanismo',
            'Voluntariado',
            'Administración de personal y recursos humanos',
            'Administración electrónica',
            'Comercio',
            'Contratación',
            'Deportes',
            'Desarrollo tecnológico',
            'Educación y juventud',
            'Empleo',
            'Medios de comunicación',
            'Obras e infraestructuras',
            'Política social de vivienda',
            'Servicios sociales',
            'Turismo',
            'Vivienda',
            'Otros'
        ].each do |name|
            x = Interest.find_by("TRANSLATE(UPPER(name), 'ÁÉÍÓÚ','AEIOU') = TRANSLATE(UPPER(?), 'ÁÉÍÓÚ','AEIOU')", name)
            x = Interest.new if x.blank?
            x.name = name
            puts "================================="
            puts x.id
            puts x.name
            puts "================================="
            x.save
        end
    end

    desc "Importa categorias nuevas si no existen"
    task :categories_show => :environment do
        [
            'Persona física',
            'Entidad sin ánimo de lucro',
            'Entidad con ánimo de lucro'
        ].each do |name|
            puts "=" *30
            puts name
            puts Category.find_by(name: name, display: true,new_category: true).blank?
            puts "=" *30
        end

        [
            'Entidades privadas sin ánimo de lucro',
            'Entidades representativas de intereses colectivos',
            'Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente',
            'Organizaciones empresariales, colegios profesionales y demás entidades representativas de intereses colectivos',
            'Entidades organizadoras de actos sin ánimo lucrativo',
            'Organizaciones no gubernamentales',
            'Grupos de reflexión e instituciones académicas y de investigación',
            'Organizaciones que representan a comunidades religiosas',
            'Organizaciones que representan a autoridades municipales',
            'Organizaciones que representan a autoridades regionales',
            'Organismos públicos o mixtos'
        ].each do |name|
            puts "=" *30
            puts name
            puts Category.find_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad sin ánimo de lucro').try(:id)).blank?
            puts "=" *30
        end

        [
            'Empresas y agrupaciones comerciales, empresariales y profesionales',
            'Consultorías profesionales',
            'Asociaciones comerciales, empresariales y profesionales',
            'Coaliciones y estructuras temporales con fines de lucro',
            'Entidades organizadoras de actos con ánimo de lucro',
            'Cualquier otra entidad con ánimo de lucro'
        ].each do |name|
            puts "=" *30
            puts name
            puts Category.find_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad con ánimo de lucro').try(:id)).blank?
            puts "=" *30
        end
    end

    desc "Importa categorias nuevas si no existen"
    task :categories => :environment do
        [
            'Persona física',
            'Entidad sin ánimo de lucro',
            'Entidad con ánimo de lucro'
        ].each do |name|
            Category.find_or_create_by(name: name, display: true,new_category: true)
        end

        [
            'Entidades privadas sin ánimo de lucro',
            'Entidades representativas de intereses colectivos',
            'Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente',
            'Organizaciones empresariales, colegios profesionales y demás entidades representativas de intereses colectivos',
            'Entidades organizadoras de actos sin ánimo lucrativo',
            'Organizaciones no gubernamentales',
            'Grupos de reflexión e instituciones académicas y de investigación',
            'Organizaciones que representan a comunidades religiosas',
            'Organizaciones que representan a autoridades municipales',
            'Organizaciones que representan a autoridades regionales',
            'Organismos públicos o mixtos'
        ].each do |name|
            Category.find_or_create_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad sin ánimo de lucro').try(:id))
        end

        [
            'Empresas y agrupaciones comerciales, empresariales y profesionales',
            'Consultorías profesionales',
            'Asociaciones comerciales, empresariales y profesionales',
            'Coaliciones y estructuras temporales con fines de lucro',
            'Entidades organizadoras de actos con ánimo de lucro',
            'Cualquier otra entidad con ánimo de lucro'
        ].each do |name|
            Category.find_or_create_by(name: name,display: true, new_category: true, category_id: Category.find_by(name: 'Entidad con ánimo de lucro').try(:id))
        end
    end

    desc "División campos usuario"
    task :data_user => :environment do
        User.all.each do |user|
            if !user.phones.blank?
                phones = user.phones.split(';')
                user.phones = ''
                user.movil_phone = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        user.phones = user.phones.blank? ? phone : user.phones.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)                        
                        user.movil_phone = user.movil_phone.blank? ? phone.to_s : user.movil_phone.to_s + ";" +phone.to_s
                    end
                end
            end

            if user.second_last_name.blank?
                surnames = user.last_name.split(' ')
                user.last_name = surnames[0]
                user.second_last_name = surnames[1]
            end

            if user.save
                puts "Se ha actualizado correctamente la el usuario"
            else
                puts "ERROR: No se ha actualizado correctamente el usuario #{user.errors.full_messages}"
            end
        end
    end


    desc "Union campos usuario"
    task :unique_phone => :environment do
        User.all.each do |user|
            user.phones = user.phones.blank? ? user.movil_phone : user.movil_phone.blank? ? nil : user.phones + ";" + user.movil_phone
            user.movil_phone = nil
            if user.save
                puts "Se han unificado los teléfonos del usuario"
            else
                puts "ERROR: No se han unificado los teléfonos del usuario #{user.errors.full_messages}"
            end
        end

        Address.all.each do |x|
            x.phones = x.phones.blank? ? x.movil_phone : x.movil_phone.blank? ? nil : x.phones + ";" + x.movil_phone
            x.movil_phone = nil
            if x.save
                puts "Se han unificado los teléfonos de la dirección"
            else
                puts "ERROR: No se han unificado los teléfonos de la dirección #{x.errors.full_messages}"
            end
        end
    end


    desc "Divide teléfonos"
    task :address_phones => :environment do 
        Address.all.each do |x|
            if !x.phones.blank?
                phones = x.phones.split(';')
                x.phones = ''
                x.movil_phone = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        x.phones = x.phones.blank? ? phone : x.phones.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)
                        x.movil_phone = x.movil_phone.blank? ? phone.to_s : x.movil_phone.to_s + ";" +phone.to_s
                    end
                end

                if x.save
                    puts "Se ha actualizado correctamente la dirección"
                else
                    puts "ERROR: No se ha actualizado correctamente la dirección #{x.errors.full_messages}"
                end
            end
        end
    end

    desc "Importa categorias nuevas si no existen"
    task :address => :environment do
        RepresentedEntity.all.each do |x|
            if x.address.blank?
                x.address = Address.new
                x.save
            end
        end

        LegalRepresentant.all.each do |x|
            if x.address.blank?
                phones = x.phones.blank? ? [] : x.phones.split(';')
                phones_fin = ''
                movil_phone_fin = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        phones_fin = phones_fin.blank? ? phone.to_s : phones_fin.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)
                        movil_phone_fin = movil_phone_fin.blank? ? phone.to_s : movil_phone_fin.to_s + ";" +phone.to_s
                    end
                end


                address = Address.new(email: x.email, phones: phones_fin, movil_phone: movil_phone_fin)

                if address.save
                    x.address = address
                    if x.save
                        puts "Se ha guardado correctamente el representante legal"
                    else
                        puts "No se ha podido guardar el representante legal #{x.errors.full_messages}"
                    end
                else
                    puts "No se ha podido guardar la dirección #{address.errors.full_messages}"
                end

            else
                phones = x.phones.blank? ? [] : x.phones.split(';')
                phones_fin = ''
                movil_phone_fin = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        phones_fin = phones_fin.blank? ? phone.to_s : phones_fin.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)
                        movil_phone_fin = movil_phone_fin.blank? ? phone.to_s : movil_phone_fin.to_s + ";" +phone.to_s
                    end
                end
                x.address.email = x.email
                x.address.phones = phones_fin
                x.address.movil_phone = movil_phone_fin
                if x.save
                    puts "Se ha guardado correctamente el representante legal"
                else
                    puts "No se ha podido guardar el representante legal #{x.errors.full_messages}"
                end
            end
        end



        Organization.all.each do |x|
            if x.address.blank?
                phones = x.phones.blank? ? [] : x.phones.split(';')
                phones_fin = ''
                movil_phone_fin = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        phones_fin = phones_fin.blank? ? phone.to_s : phones_fin.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)
                        movil_phone_fin = movil_phone_fin.blank? ? phone.to_s : movil_phone_fin.to_s + ";" +phone.to_s
                    end
                end
                address = Address.new(country: x.country,province: x.province, town: x.town, address_type: x.address_type,
                    address: x.old_address, number: x.number, gateway: x.gateway, stairs: x.stairs, floor: x.floor, door: x.door,
                    postal_code: x.postal_code, email: x.email, phones: phones_fin, movil_phone: movil_phone_fin
                )

                if address.save
                    x.address = address
                    if x.save
                        puts "Se ha guardado correctamente el lobby"
                    else
                        puts "No se ha podido guardar el lobby #{x.errors.full_messages}"
                    end
                else
                    puts "No se ha podido guardar la dirección #{address.errors.full_messages}"
                end

            else
                phones = x.phones.blank? ? [] : x.phones.split(';')
                phones_fin = ''
                movil_phone_fin = ''
                phones.each do |phone|
                    if phone.match(/\A([9]{1})+([0-9]{8})\z/)
                        phones_fin = phones_fin.blank? ? phone.to_s : phones_fin.to_s + ";" + phone.to_s
                    elsif phone.match(/\A([7,6]{1})+([0-9]{8})\z/)
                        movil_phone_fin = movil_phone_fin.blank? ? phone.to_s : movil_phone_fin.to_s + ";" +phone.to_s
                    end
                end
                x.address.country = x.country
                x.address.province = x.province
                x.address.town= x.town
                x.address.address_type = x.address_type
                x.address.address_number_type = x.address_number_type
                x.address.address = x.old_address
                x.address.number= x.number
                x.address.gateway= x.gateway
                x.address.stairs = x.stairs
                x.address.floor = x.floor
                x.address.door= x.door
                x.address.postal_code = x.postal_code
                x.address.phones = phones_fin
                x.address.movil_phone = movil_phone_fin
                if x.save
                    puts "Se ha guardado correctamente el lobby "
                else
                    puts "No se ha podido guardar el lobby #{x.errors.full_messages}"
                end
            end

        end

    end

    desc "Elimina el time_frame de la tabla data_preferences"
    task :remove_preference => :environment do
        puts "Time frame for email events deleted" if DataPreference.find_by(title: "time_frame").destroy
    end

end