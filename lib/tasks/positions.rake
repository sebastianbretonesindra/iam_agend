namespace :positions do
    task change: :environment do
        id = 4846
        pos = Position.find(id)
        pos.area_id =302

        if pos.save(validate: false)
            puts "Se ha cambiado correctamente el area #{id}"
        else
            puts "ERROR: no se ha cambiado el area #{id}"
        end
    end


    desc "Elimina las repeticiones de posiciones, se queda con el id más pequeño"
    task clean_pos: :environment do
        puts  "="*30
        Holder.all.each do |holder|
            valid_pos = holder.positions.where('"to" IS NULL').last
            if !valid_pos.blank?
                holder.positions.where('"to" IS NULL').each do |pos2|
                    if pos2.id != valid_pos.id
                        if valid_pos.id.to_i != pos2.id.to_i && valid_pos.title.to_s == pos2.title.to_s && valid_pos.area_id == pos2.area_id
                            begin
                                Event.where(position_id: pos2.id).each do |event|
                                    event.position_id = valid_pos.id
                                    if event.save(validate: false)
                                        puts "-"*10
                                        puts "Evento del titular: #{pos2.id} modificado al: #{valid_pos.id}"
                                        puts "-"*10
                                    else
                                        puts "-"*10
                                        puts pos2.id
                                        puts event.errors.full_messages
                                        puts "-"*10
                                    end
                                end
                                Participant.where(position_id: pos2.id).each do |participant|
                                    participant.position_id = valid_pos.id
                                    if participant.save(validate: false)
                                        puts "-"*10
                                        puts "Participación del titular: #{pos2.id} modificado al: #{valid_pos.id}"
                                        puts "-"*10
                                    else
                                        puts "-"*10
                                        puts pos2.id
                                        puts participant.errors.full_messages
                                        puts "-"*10
                                    end
                                end
                                puts "Cargo eliminado: #{pos2.id}"
                                pos2.destroy
                            rescue => e
                            end
                        end
                    end
                end
            end
        end
        puts  "="*30
    end
end