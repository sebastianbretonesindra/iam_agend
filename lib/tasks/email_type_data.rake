namespace :email_type_data do
    desc "Insert email information"
    task emails: :environment do
        data_json_cc= "transparenciaydatos@madrid.es"
        data_json_cco ="creaspodhe@madrid.es"

        email = ManageEmail.find_by(:type_data => "Holder")
        if email.blank?
        ManageEmail.create!(type_data: "Holder", sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_holder.subject'), 
            email_body: I18n.t("mailers.new_holder.body")) 
            puts "creado"
        else
            puts "Ya está creado"
        end

        email = ManageEmail.find_by(:type_data => 'AuxManager')
        if email.blank?
        ManageEmail.create!(type_data: 'AuxManager', sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_aux.subject'), 
            email_body: I18n.t("mailers.new_aux.body"))
            puts "creado"
        else
            puts "Ya está creado"
        end

        email = ManageEmail.find_by(:type_data => 'Events')
        if email.blank?
        ManageEmail.create!(type_data: 'Events', sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.events.subject'), 
            email_body: I18n.t("mailers.events.body"))
            puts "creado"
        else
            puts "Ya está creado"
        end

        email = ManageEmail.find_by(:type_data => 'Organizations')
        if email.blank?
        ManageEmail.create!(type_data: 'Organizations', sender: "no-reply@madrid.es",
            fields_cc: "", 
            fields_cco: "registrodelobbies@madrid.es",
            subject: I18n.t('mailers.organizations.subject'), 
            email_body: I18n.t("mailers.organizations.body"))
            puts "creado"
        else
            puts "Ya está creado"
        end
    end

    desc "Insert email information"
    task update: :environment do
        manage = ManageEmail.find_by(type_data: "Events")
        manage.update(email_body: I18n.t("mailers.events.body"))
        if manage.save!
            puts "Cuerpo del email de eventos actualizado"
        else
            puts manage.errors.full_messages
        end
    end
end
