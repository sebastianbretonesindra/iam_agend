namespace :users do

    task :add => :environment do
        admin_user = User.create(password: '12345678', email: 'admin@agendas.dev', first_name: 'Admin', last_name: 'Istrator', active: 1)
admin_user.admin!
    end

    task :destroy => :environment do
        admin_user = User.find_by( email: 'admin@agendas.dev', first_name: 'Admin', last_name: 'Istrator')
        admin_user.destroy
    end


    desc "Actualizar datos desde directorio"
    task :update => :environment do
        uweb = UwebApi.new
        User.all.each do |user|
            if !user.user_key.blank?
                data = uweb.get_user(user.user_key)
                if !data.blank?
                    user.positions = data['CARGO']
                    if user.save!
                        puts "="*20
                        puts "Cargo actualizado para el usuario #{user.id}."
                        puts "="*20
                    else
                        puts "="*20
                        puts user.errors.full_messages
                        puts "="*20
                    end
                end
            end
        end
    end

end