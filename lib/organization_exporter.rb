include ActionView::Helpers::SanitizeHelper

class OrganizationExporter
  attr_accessor :fields

  ENUMS       = { get_range_fund: "organizations.show.range_fund",
                  range_fund_1: "organizations.show.range_fund",
                  range_fund_2: "organizations.show.range_fund",
                  range_fund_3: "organizations.show.range_fund",
                  range_fund_4: "organizations.show.range_fund",
                  entity_type: "organizations.show.entity_type",
                  status: "organizations.show.statuses" }.freeze

  COLLECTIONS = { registered_lobbies: :name,
                  interests: :name,
                  represented_entities: :fullname,
                  agents: :fullname }.freeze

  TO_STRIP    =  [:description].freeze

  PRIVATE_FIELDS = [ 'reference', 'legal_representant_full_name', 
                    'legal_representant_address_type', 'legal_representant_address', 'legal_representant_address_number_type',
                    'legal_representant_number', 'legal_representant_gateway', 'legal_representant_stairs', 'legal_representant_floor',
                    'legal_representant_door', 'legal_representant_postal_code', 'legal_representant_town', 'legal_representant_province',
                    'legal_representant_country', 'legal_representant_phones', 'legal_representant_email',
                    'notification_effect_full_name', 
                    'notification_effect_address_type', 'notification_effect_address', 'notification_effect_address_number_type',
                    'notification_effect_number', 'notification_effect_gateway', 'notification_effect_stairs', 'notification_effect_floor',
                    'notification_effect_door', 'notification_effect_postal_code', 'notification_effect_town', 'notification_effect_province',
                    'notification_effect_country', 'notification_effect_phones', 'notification_effect_email', 
                    'user_name', 'user_email', 'user_phones' ].freeze

  FIELDS = [ 'reference', 'identifier', 'name', 'first_surname', 'second_surname',
             'address_address_type', 'address_address', 'address_address_number_type', 'address_number', 'address_gateway',
             'address_stairs', 'address_floor', 'address_door', 'address_postal_code', 'address_town', 'address_province', 'address_country',
             'address_phones', 'address_email', 'description', 'web', 'registered_lobbies', 
             'inscription_date', 'renovation_date', 'expired_date','updated_at', 'termination_date','status','entity_type', 
             'get_category', 
             'legal_representant_full_name', 
             'legal_representant_address_type', 'legal_representant_address', 'legal_representant_address_number_type',
             'legal_representant_number', 'legal_representant_gateway', 'legal_representant_stairs', 'legal_representant_floor',
             'legal_representant_door', 'legal_representant_postal_code', 'legal_representant_town', 'legal_representant_province',
             'legal_representant_country', 'legal_representant_phones', 'legal_representant_email',
             'notification_effect_full_name', 
             'notification_effect_address_type', 'notification_effect_address', 'notification_effect_address_number_type',
             'notification_effect_number', 'notification_effect_gateway', 'notification_effect_stairs', 'notification_effect_floor',
             'notification_effect_door', 'notification_effect_postal_code', 'notification_effect_town', 'notification_effect_province',
             'notification_effect_country', 'notification_effect_phones', 'notification_effect_email',             
             'user_name', 'user_email', 'user_phones', 'invalidated?', 
             'self_employed_lobby', 'get_fiscal_year', 'get_range_fund','get_in_group_public_administration', 'get_text_group_public_administration',
             'get_subvention', 'get_subvention_public_administration', 'get_range_1_import','get_range_1_entity_name', 'get_range_2_import','get_range_2_entity_name',
             'get_range_3_import','get_range_3_entity_name','get_range_4_import','get_range_4_entity_name',
             'get_contract', 'get_contract_turnover', 'get_contract_total_budget',
             'get_contract_breakdown', 'get_contract_financing',       'agents', 'employee_lobby',
             'represented_entity_1', 'fiscal_year_1', 'range_fund_1', 'in_group_public_administration_1', 'text_group_public_administration_1',
             'subvention_1', 'subvention_public_administration_1', 'get_range_1_1_import','get_range_1_1_entity_name', 'get_range_1_2_import','get_range_1_2_entity_name',
             'get_range_1_3_import','get_range_1_3_entity_name','get_range_1_4_import','get_range_1_4_entity_name','contract_1', 'contract_turnover_1', 'contract_total_budget_1',
             'contract_breakdown_1', 'contract_financing_1',
             'represented_entity_2', 'fiscal_year_2', 'range_fund_2', 'in_group_public_administration_2', 'text_group_public_administration_2',
             'subvention_2', 'subvention_public_administration_2', 'get_range_2_1_import','get_range_2_1_entity_name', 'get_range_2_2_import','get_range_2_2_entity_name',
             'get_range_2_3_import','get_range_2_3_entity_name','get_range_2_4_import','get_range_2_4_entity_name','contract_2', 'contract_turnover_2', 'contract_total_budget_2',
             'contract_breakdown_2', 'contract_financing_2',
             'represented_entity_3', 'fiscal_year_3', 'range_fund_3', 'in_group_public_administration_3', 'text_group_public_administration_3',
             'subvention_3', 'subvention_public_administration_3', 'get_range_3_1_import','get_range_3_1_entity_name', 'get_range_3_2_import','get_range_3_2_entity_name',
             'get_range_3_3_import','get_range_3_3_entity_name','get_range_3_4_import','get_range_1_4_entity_name','contract_3', 'contract_turnover_3', 'contract_total_budget_3',
             'contract_breakdown_3', 'contract_financing_3',
             'represented_entity_4', 'fiscal_year_4', 'range_fund_4', 'in_group_public_administration_4', 'text_group_public_administration_4',
             'subvention_4', 'subvention_public_administration_4', 'get_range_4_1_import','get_range_4_1_entity_name', 'get_range_4_2_import','get_range_4_2_entity_name',
             'get_range_4_3_import','get_range_4_3_entity_name','get_range_4_4_import','get_range_4_4_entity_name','contract_4', 'contract_turnover_4', 'contract_total_budget_4',
             'contract_breakdown_4', 'contract_financing_4'
            ].freeze

             
  INTERESTS_FIELD = []

  def initialize(extended = false)
    @fields = FIELDS
    @fields = @fields - PRIVATE_FIELDS unless extended
    if INTERESTS_FIELD.blank?
      Interest.all.each do |interest|
        INTERESTS_FIELD.push(interest.name)
      end
    end
    @fields = @fields + INTERESTS_FIELD if extended


  end

  def headers
    @fields.map { |f| 
      unless INTERESTS_FIELD.include?(f)
      I18n.t("organization_exporter.#{f}")
      else
        f
      end  }
  end

  def organization_to_row(organization)
    @fields.map do |field|
      process_field(organization, field)
    end
  end

  def windows_headers
   windows_array headers
  end

  def windows_organization_row(organization)
    windows_array organization_to_row(organization)
  end

  def save_csv(path)
    CSV.open(path, 'w', col_sep: ';', encoding: "ISO-8859-1") do |csv|
      csv << windows_headers
      Organization.find_each do |organization|
        csv << windows_organization_row(organization)
      end
    end
  end

  def save_xlsx(path)
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => "Lobbies") do |sheet|
        headers_transf = []
        color_blue = p.workbook.styles.add_style( :b => true, :fg_color=>"#FF0000FF")
        @fields.map { |f| 
          unless INTERESTS_FIELD.include?(f)
            headers_transf.push("#{I18n.t("organization_exporter."+f.to_s)}")
          else
            headers_transf.push("#{f}")
          end 
        }
        sheet.add_row headers_transf, :style => color_blue
       
        Organization.find_each do |organization|
          content_transf = []
          @fields.map do |field|
            content_transf.push("#{process_field(organization, field)}")
          end
          sheet.add_row content_transf
        end
      end
      p.use_shared_strings = true
      p.serialize(path)
    end
  end


  def save_xls(path)
    Spreadsheet.client_encoding = 'ISO-8859-1'
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet
    sheet.row(0).default_format = Spreadsheet::Format.new color: :blue, weight: :bold
    sheet.row(0).concat windows_headers
    index = 1
    Organization.find_each do |organization|
      sheet.row(index).concat windows_organization_row(organization)
      index += 1
    end

    book.write(path)
  end

  def save_json(path)
    data = []
    h = headers
    Organization.find_each do |organization|
      data << h.zip(organization_to_row(organization)).to_h
    end
    File.open(path, "w") do |f|
      f.write(data.to_json)
    end
  end

  private

    def windows_array(values)
      values.map { |v| v.to_s.encode("ISO-8859-1", invalid: :replace, undef: :replace, replace: '') }
    end

    def process_field(organization, field)
     if ENUMS.keys.include?(field.to_sym)
        I18n.t "#{ENUMS[field.to_sym]}.#{organization.send(field)}" if organization.send(field).present?
      elsif INTERESTS_FIELD.include?(field)
        salida = I18n.t("false")
        organization.interests.each do |f|
          if f.name.to_s == field.to_s
             salida = I18n.t("true")
             break
          end
        end
        salida
      elsif COLLECTIONS.keys.include?(field.to_sym)
        accessor = COLLECTIONS[field.to_sym]
        organization.send(field).collect(&accessor).join(", ")
      elsif organization.send(field).class == TrueClass || organization.send(field).class == FalseClass
        I18n.t "#{organization.send(field)}"
      elsif TO_STRIP.include?(field.to_sym)
        strip_tags(organization.send(field))
      elsif organization.send(field).present? && Organization.columns_hash[field].present? &&
            (Organization.columns_hash[field].type == :date || Organization.columns_hash[field].type == :datetime)
        I18n.l organization.send(field), format: :short
      else
       organization.send(field)
      end
    end
end
