Rails.application.routes.draw do

  resources :range_subventions
  resources :addresses
  resources :notification_effects
  get 'calendar/index'

  # Public resources
  root to: 'homepage#index', format: false
  
  
  get 'authorize' => 'auth#gettoken'
  get '/redirect', to: 'events#redirect', as: 'redirect'
  get '/callback', to: 'events#callback', as: 'callback'
  get '/calendars', to: 'events#calendars', as: 'calendars'

  get '/lang/:lang', to: 'application#change_language', as: 'change_language'
  get '/show/:id', to: 'visitors#show', as: 'show'
  get '/update_holders', to: 'visitors#update_holders', as: 'update_holders'
  get '/inicio.do', to: 'uweb_access#uweb_sign_in'
  get '/agenda/:holder/:full_name', to: 'visitors#agenda', as: 'agenda'

  get '/faq', to: 'questions#index', as: 'faq'
  get '/websitemap', to: 'static_pages#websitemap', as: 'websitemap'

  post '/ws/agends', to: 'ws/agends#generic'
  post '/ws/agends/:personal_code', to: 'ws/agends#holder'

  resources :statistics, only: [:index]
  resources :statistics do
    get :lobbies, on: :collection
  end

  get '/code_of_conduct', to: 'static_pages#code_of_conduct', as: 'code_of_conduct'
  get '/accessibility', to: 'static_pages#accessibility', as: 'accessibility'

  get '/homepage', to: 'homepage#index', as: 'homepage'
  get 'registration_lobbies/index'
  get '/registration_lobbies', to: 'registration_lobbies#index', as: 'registration_lobbies'
  get '/visitors', to: 'visitors#index', as: 'visitors'
  get '/export_events', to: 'visitors#export_events', as: 'visitors_export_events'

  # Admin
  get "/admin", to: 'events#index', as: 'admin'
  
  #namespace :api do
    wash_out :servicio_integracion_bareg
  #end
  

  devise_for :users, controllers: { sessions: "users/sessions" }

  resources :events do
    get :download, on: :collection
    get :clone, on: :member
    get :export_outlook, on: :member
    get :get_calendars, on: :member
  end
  
 
  resources :areas
  resources :activities
  resources :infringement_emails, only: [:new, :create]
  resources :holders do 
    get :active, on: :collection
    get :mail, on: :member
    post :send_mail, on: :member
    get :export_all, on: :collection
  end


  namespace :admin do
    resources :notifications, except: [:index, :new, :create, :destroy] do 
      get :active, on: :collection
      get :historic, on: :collection
      get :pre_recover, on: :member
      post :recover, on: :member
      get :pre_canceled, on: :member
      post :canceled, on: :member
      get :permited, on: :member
      get :manually_permited, on: :member
      get :recover_origin, on: :member
      post :update_notification, on: :member
    end
    resources :exports
    resources :parametrizations do
      post :change_view, on: :collection
      post :length_event_description, on: :collection
      post :send_email, on: :collection
    end
    resources :parametrizations, only: [:index]
    resources :manage_emails
    resources :users do 
      get :revert, on: :member
      get :assign_holder, on: :collection
      get :export_manages, on: :collection
      get :uweb, on: :collection
    end
    get 'passwords/edit', to: 'passwords#edit', as: 'edit_password'
    match 'passwords/update', to: 'passwords#update', as: 'update_password', via: [:patch, :put]
    resources :organizations do
      resources :agents
      resources :organization_interests, only: [:index]
      match 'interests_update', to: 'organization_interests#update', as: 'interests_update', via: [:patch, :put]
    end
    resources :organizations_links
    resources :questions
    post 'order_questions', to: 'questions#order', as: 'order_questions'

    resources :newsletters do
      member do
        post :deliver
      end
      get :users, on: :collection
    end
  end

  resources :positions do
    get :autocomplete_position_title, :on => :collection
  end
  get '/organizations/edit', to: 'organizations#edit', as: 'edit_organization'
  get '/organizations/destroy', to: 'organizations#destroy', as: 'destroy_organization'
  get '/organizations/new', to: 'organizations#new', as: 'new_organization'
  resources :organizations, except: [:new, :edit, :destroy] do
    get :autocomplete_organization_name, :on => :collection
    resources :represented_entities, only: :index, format: :json
    resources :agents, only: :index, format: :json
  end

  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  get '*path' => redirect('/') unless Rails.env.development?

end
