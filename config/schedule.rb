# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
require "./"+ File.dirname(__FILE__) + "/environment.rb"
set :output, File.join(Whenever.path, "log", "cron.log")
# set :output, nil
# set :output, {:error => nil, :standard => nil}

every 1.minute, roles: [:export] do
  command "date > ~/cron-test-export.txt"
end

every 1.minute, roles: [:cron] do
  command "date > ~/cron-test-cron.txt"
end

every 1.day, at: '01:00 am', roles: [:export] do
  rake 'export:agendas'
  rake 'export:organizations'
end

every 1.hour, roles: [:cron] do
  rake 'import_madrid:import'
  rake 'positions:clean_pos'
end

every 1.day, at: '6:00 am', roles: [:cron] do
  rake 'events:update_event_status'
  rake 'organizations:renovation_alert'
end

send_email = SendEmail.all.first
auto_hour = !send_email.try(:auto_hour).blank? ? send_email.try(:auto_hour).to_s : '00:00 am'
every 1.day, at: auto_hour, roles: [:cron] do
    rake 'send_email:auto'
end

manual_send_email = SendEmail.all.first
manual_hour = !manual_send_email.try(:manual_hour).blank? ? manual_send_email.try(:manual_hour).to_s : '00:00 am'
every 1.day, at: manual_hour, roles: [:cron] do
  rake 'send_email:manual'
end