// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require ../dependencies/jquery.treetable
//= require jquery-ui
//= require sortable
//= require jquery.validate
//= require cocoon
//= require ../dependencies/foundation-datepicker
//= require_tree ../dependencies/foundation-datepicker-locales
//= require tinymce
//= require autocomplete-rails
//= require_tree .

$(function() {
  $(document).foundation();

  tinymce.init({
    selector : "textarea:not(.mceNoEditor)",
    menubar: false,
    plugins: "advlist autolink autosave link image lists charmap wordcount media code contextmenu textcolor paste table",
    toolbar1: "undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | bullist numlist | outdent indent blockquote | link image media | forecolor | table | subscript superscript | charmap | code",
    language : 'es',
  });

});


function change_param_parametrize(param) {
  
  data_send="{}"
  data_url=""

  if (param.length !=0) {
    data_send = "{\""+param[0]+"\": "+param[1]+" }";
    data_url = "?"+param[0]+"="+param[1];
  }

  data_send=JSON.parse(data_send)
  $.ajax({
    type: 'get',
    url: '/admin/parametrizations/'+data_url,
    data: data_send,
    success: function (data, status) {
      window.history.pushState(data, "Title",'/admin/parametrizations/'+data_url);
    }});
}

function checkUncheck(checks,valor) {
  for(var i=0; i< checks.length;i++) {
    $("#export_fields_"+checks[i]).prop('checked', valor);
  }
}

function change_expired_date(date,years, expired) {
    var dt = date.val();
    var e = expired
    if (!dt) {
     $("#expired-date").html(e)
    } else {
      var year_now  = 0
      if (dt instanceof Date) {
        year_now = dt.getFullYear()
      }else {
        year_now= parseInt(dt.split(' ')[0].replace(/-/g,'/').split('/')[2])
      }
      dt = dt.replace(""+year_now, ""+(year_now+years))
      $("#expired-date").html(dt);
    }
 
}

// function showHideEmailSend(element){
//   if(element.style.display == "block"){
//     element.style.display = "none";
//   }else{
//     element.style.display = "block"
//   }
// }
