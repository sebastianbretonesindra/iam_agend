class Address < ActiveRecord::Base
    # validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, allow_blank: true
    # validates_format_of :phones, :with => /\A([7,6,9]{1})+([0-9]{8})|([9]{1})+([0-9]{8});([9]{1})+([0-9]{8})+\z/, allow_blank: true
    # validates_format_of :movil_phone, :with => /\A([7,6]{1})+([0-9]{8})|([7,6]{1})+([0-9]{8});([7,6]{1})+([0-9]{8})+\z/, allow_blank: true


    def full_address
        "#{self.address_type} / #{self.address}"
    end

    def full_number
        "#{self.address_number_type} #{self.number}"
    end
end
