class HolderStatusHistory < ActiveRecord::Base
    belongs_to :position

    def self.generateHistory(position, status = nil)
        history = HolderStatusHistory.new
        if status.blank?
          if position.to.blank?
            history.status = I18n.t('backend.active') 
          else
            history.status = I18n.t('backend.inactive') 
          end
        else
          history.status=status
        end
        history.status_at = Time.now
        history.position = position

        history
    end
end
