class Position < ActiveRecord::Base

  belongs_to :area
  belongs_to :holder
  has_many :events
  has_many :holder_status_histories, dependent: :delete_all
  has_many :participants, dependent: :destroy
  has_many :titular_events, class_name: "Event", dependent: :destroy
  has_many :participants_events, through: :participants

  validates :title, :start, presence: true
  validate :validate_unique_title
  scope :current, -> { where(to: nil) }
  scope :previous, -> { where.not(to: nil) }
  scope :previous_time, -> {where.not(to: nil).order(:to)}
  scope :area_filtered, ->(area) { where(area_id: Area.find(area).subtree_ids) if area.present? }
  scope :full_like, lambda { |search_name| where("holder_id IN (?) OR title ILIKE ?", Holder.by_name(search_name).pluck(:id), "%#{search_name}%") }

  def events
    (titular_events + participants_events).uniq
  end

  def finalize
    self.to = Time.now
    self
  end

  def self.holders(user_id)
    holder_ids = Holder.managed_by(user_id).ids
    Position.where(holder_id: holder_ids)
  end

  def get_previous_time
    data_end=nil
    pos_prev = Position.previous_time.where("holder_id = ?", self.holder.id)
    if pos_prev.blank?
      data_end = self.created_at
    else
      data_ant=nil
      pos_prev.each do |p|
        data_ant=p.to if !self.to.blank? && p.to < self.to || self.to.blank? && (data_ant.blank? || data_ant < p.to)
      end

      data_end = data_ant.blank? ? self.created_at : data_ant
    end
    data_end.blank? ? "" : data_end.strftime("%d/%m/%Y")
  end

  def full_name
    holder = Holder.where(id: self.holder_id).first
    (holder.last_name.to_s.delete(',') + ', ' + holder.first_name.to_s.delete(',') + ' - ' + self.title).mb_chars.to_s
  end

  private

  def validate_unique_title
    exist = Position.where(holder: self.holder, area: self.area, title: self.title).where("id != ?", self.id)
    if !exist.blank? 
      self.errors.add(:title, "Ya existe un cargo y area asignado")
    end
  end

end
