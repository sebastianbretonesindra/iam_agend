class Export < ActiveRecord::Base
    extend Admin::ExportsHelper
    validates :fields, presence: true
    def self.main_columns
        %i(id
            name
            fields
            )
    end
    
end
