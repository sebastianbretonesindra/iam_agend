class Category < ActiveRecord::Base

  has_many :organizations, dependent: :destroy
  has_many :represented_entities, dependent: :destroy
  belongs_to :category

  validates :code, presence: true

  scope :visible, -> { where(display: true) }
  scope :categories, -> { visible.where("category_id is null AND 0 >= (Select count(*) from categories as subcat where subcat.category_id = categories.id)").where()}
  scope :sub_categories, -> { visible.where("category_id is not null").where(new_category: true)}

  def self.categories_sub_categories
    aux = []
    Category.where(display: true, new_category: false).each {|c| aux.push([c.name, c.id]) }
    Category.where(display: true, new_category: true, category_id: nil).each do |c|
      if Category.where(category_id: c.id).count > 0
        aux.push([c.name, c.id])
        Category.where(category_id: c.id).each {|ch| aux.push(["--- #{ch.name}", ch.id]) }
      else
        aux.push([c.name, c.id])
      end
    end
    aux
  end

end
