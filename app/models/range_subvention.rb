class RangeSubvention < ActiveRecord::Base
    belongs_to :item, :polymorphic => true, touch: true
end
