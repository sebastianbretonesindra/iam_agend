class EventAgent < ActiveRecord::Base

  belongs_to :event
  has_many :attachments, dependent: :destroy

  validates :name, presence: true

  accepts_nested_attributes_for :attachments, allow_destroy: true


  def get_real_agent
    Agent.find_by("CONCAT(name,' ',first_surname,' ', second_surname) = ?", self.name)
  end
end
