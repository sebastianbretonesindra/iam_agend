class SendEmail < ActiveRecord::Base
    validates_numericality_of :auto_date, presence: :true, if: :auto_enable
    validates :auto_hour, presence: :true, if: :auto_enable
    validates :auto_frecuence, presence: :true, if: :auto_enable
    validates :auto_user_type, presence: :true, if: :auto_enable
    #validates :auto_excluded_emails, presence: :true, if: :auto_enable
    validates :manual_hour, presence: :true, if: :manual_enable
    validates :manual_from_date, presence: :true, if: :manual_enable
    validates :manual_to_date, presence: :true, if: :manual_enable
    validates :manual_user_type, presence: :true, if: :manual_enable
    #validates :manual_excluded_emails, presence: :true, if: :manual_enable

    def auto_enable
        return false if self.auto_send.to_s == "false"
        true
    end

    def manual_enable
        return false if self.manual_send.to_s == "false"
        true
    end
end
