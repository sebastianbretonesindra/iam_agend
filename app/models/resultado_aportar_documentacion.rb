class ResultadoAportarDocumentacion < WashOut::Type
    type_name 'ResultadoAportarDocumentacion'
    map codRetorno: :string, descError: :string
end