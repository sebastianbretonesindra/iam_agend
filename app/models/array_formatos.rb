class ArrayFormatos < WashOut::Type
  type_name 'ArrayFormatos'
  map formato: [:string]
end