class ResultadoIniciarExpediente < WashOut::Type
    type_name 'ResultadoIniciarExpediente'
    map codRetorno: :string, descError: :string, idExpediente: :integer, refExpediente: :string
end