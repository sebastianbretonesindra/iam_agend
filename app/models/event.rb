class Event < ActiveRecord::Base
  extend EventsHelper
  extend Admin::EventExporterHelper
  include PublicActivity::Model

  attr_accessor :cancel, :decline, :accept, :holder_title, :event_start_time
  
  tracked owner: Proc.new { |controller, model| controller.present? ? controller.current_user : model.user }
  tracked title: Proc.new { |controller, model| model.title }

  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  validates_with CancelEventValidator

  validates :title, :position, presence: true
  #validate :valid_scheduled
  validates_inclusion_of :lobby_activity, :in => [true, false]
  validate :participants_uniqueness, :position_not_in_participants, :role_validate_scheduled, :validate_location, :validate_description, :validate_attendees
  validate :lobby_activity_with_organization, if: -> { self.lobby_activity.present? }
  validates :canceled_reasons, presence: { message: I18n.t('backend.lobby_not_allowed_neither_empty_mail') }, allow_blank: false, if: Proc.new { |a| !a.canceled_at.blank? }
  validates :declined_reasons, presence: { message: I18n.t('backend.lobby_not_allowed_neither_empty_mail') }, allow_blank: false, if: Proc.new { |a| !a.declined_at.blank? }
  validate :published_range_validation
  validate :scheduled_range_validation

  before_create :set_status
  before_update :set_published_at
  after_validation :decline_event
  after_validation :cancel_event
  after_validation :accept_event

  belongs_to :user
  belongs_to :position
  belongs_to :organization
  has_many :participants, dependent: :destroy
  has_many :positions, through: :participants
  has_many :attachments, dependent: :destroy
  has_many :attendees, dependent: :destroy
  has_many :event_represented_entities, dependent: :destroy, inverse_of: :event
  has_many :event_agents, dependent: :destroy, inverse_of: :event

  accepts_nested_attributes_for :attendees, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :event_represented_entities, allow_destroy: true
  accepts_nested_attributes_for :event_agents, allow_destroy: true
  accepts_nested_attributes_for :attachments, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :participants, reject_if: :all_blank, allow_destroy: true

  SUPPORTED_FILTERS = [:title, :position_id, :lobby_activity, :status, :organization_id].freeze
  scope :title, lambda {|title| where("title ILIKE ?", "%#{title}%") }
  scope :by_holders, lambda {|holder_ids| joins(:position).where("positions.holder_id IN (?)", holder_ids)  }
  scope :by_participant_holders, lambda {|holder_ids| joins(participants: :position).where("positions.holder_id IN (?)", holder_ids)  }
  scope :by_holder_name, lambda {|name|
    holder_ids = Holder.by_name(name).pluck(:id)
    joins(:position).where("positions.holder_id IN (?)", holder_ids)
  }
  scope :status, ->(status) { where("status IN (#{status})") }
  scope :lobby_activity, ->(lobby_activity){ where(lobby_activity: lobby_activity) }
  scope :position_id, ->(position) { where(position_id: position) }
  scope :organization_id, ->(organization) { where(organization_id: organization) }
  #scope :published, ->{ where("published_at IS NULL OR published_at <= ?", Time.zone.today) }
  enum status: { requested: 0, accepted: 1, done: 2, declined: 3, canceled: 4 }

  def valid_scheduled
    if !self.scheduled.blank?
      errors.add(:base, I18n.t('backend.limit_scheduled_old')) if self.scheduled < Date.today - 5.years || self.scheduled > Date.today + 1.year
    end
  end

  def cancel_event
    return if cancel.to_s != 'true' || !canceled_at.blank?
    if self.lobby_activity && self.organization.present? && self.organization.entity_type == "lobby"
      self.canceled_at = Time.zone.today
      self.status = 'canceled'
    end
  end

  def lobby_expired
    data_year = DataPreference.find_by(title: "expired_year")
    data_year = data_year.blank? ? 2 : data_year.content_data.to_i
    if self.lobby_activity && self.organization.present? && self.organization.entity_type == "lobby"
      if self.old_event_lobby
        "old_event"
      elsif !self.organization.try(:renovation_date).blank? && self.organization.renovation_date + data_year.years < Time.zone.now
        "new_event_expired"
      else
        "new_event"
      end
    else
      nil
    end
  end

  def lobby_expired_formatted
    return I18n.t("expired.#{self.lobby_expired}") unless self.lobby_expired.blank?
    ''
  end

  def start_time
    scheduled
  end

  def decline_event
    return if decline.to_s != 'true' || !declined_at.blank?
    self.declined_at = Time.zone.today
    self.status = 'declined'
  end

  def accept_event
    return if accept.to_s != 'true' || !accepted_at.blank?
    if self.lobby_activity && self.organization.present? && self.organization.entity_type == "lobby"
      self.accepted_at = Time.zone.today
      self.status = 'accepted'
    end
  end

  def holder_name
    position.holder.full_name
  end

  def self.managed_by(user)
    holder_ids = Holder.managed_by(user.id).pluck(:id)
    titular_event_ids = Event.by_holders(holder_ids).pluck(:id)
    participant_event_ids = Event.by_participant_holders(holder_ids).pluck(:id)

    Event.where(id: (titular_event_ids + participant_event_ids).uniq)
  end

  def self.ability_events(user)
    event_ids = []
    event_ids += ability(user, 'titular_events')
    event_ids += ability(user, 'participants_events')
    return event_ids
  end

  def self.ability_titular_events(user)
    ability(user, 'titular_events')
  end

  def self.ability_participants_events(user)
    ability(user, 'participants_events')
  end

  def self.searches(params)
    if params.present?
      events = filter(params)
    else
      events = all
    end
    events
  end

  private_class_method def self.ability(user, method)
    event_ids = []
    user.manages.includes(:holder).each do |manage|
      manage.holder.positions.each do |position|
        event_ids += position.send(method).ids
      end
    end
    return event_ids
  end

  def contributed_by
    manages=""
    if !position.blank? && !position.holder.blank?
      position.holder.manages.each {|manage| manages = manages + (manages.blank? ? manage.user.full_name_comma : " / " +manage.user.full_name_comma) }
    end
    return manages
  end

  def position_names
    names = ''
    positions.each {|position| names += (names.blank? ? position.holder.full_name_comma + ' - ' + position.title : ' / ' + position.holder.full_name_comma + ' - ' + position.title) }
    return names
  end

  def user_name
    user.full_name
  end

  def self.filter(attributes)
    attributes.slice(*SUPPORTED_FILTERS).reduce(all) do |scope, (key, value)|
      value.present? ? scope.send(key, value) : scope
    end
  end

  def holder_position
    self.position.title
  end

  def lobby_user_name
    "#{lobby_contact_firstname} #{lobby_contact_lastname}"
  end

  def status_id
    Event.statuses[self.status]
  end

  private

  

    def participants_uniqueness
      participants = self.participants.reject(&:marked_for_destruction?)
      return unless participants.map(&:position_id).uniq.count != participants.to_a.count
      errors.add(:base, I18n.t('backend.participants_uniqueness'))
    end

    def position_not_in_participants
      participants = self.participants.reject(&:marked_for_destruction?)
      positions_ids = participants.collect{|p| p.position.id }
      return unless position && positions_ids.include?(position.id)
      errors.add(:base, I18n.t('backend.position_not_in_participants'))
    end

    def set_status
      if self.user.lobby?
        self.status = "requested"
      else
        self.status = "accepted"
      end
    end

    def set_published_at
      # self.published_at = Date.current if (!self.user.lobby? && self.published_at.blank?)
    end



    def scheduled_range_validation
      return if self.scheduled.blank? || (self.scheduled >= Time.zone.now - 2.years && self.scheduled <= Time.zone.now + 2.years)
      errors.add(:base, I18n.t('backend.limit_scheduled'))
    end

    def role_validate_scheduled
      return if self.user.lobby? || self.scheduled.present? || reasons_present?
      errors.add(:base, I18n.t('backend.date_event'))
    end

    def validate_location
      return if self.user.lobby? || self.location.present?
      errors.add(:base, I18n.t('backend.place_event'))
    end

    def validate_description
      aux_data = Nokogiri::HTML(self.description).text
      return if DataPreference.find_by(title: "min_length").blank? || aux_data.length  >= DataPreference.find_by(title: "min_length").try(:content_data).to_i || DataPreference.find_by(title: "min_length").try(:content_data).to_i == 0
      errors.add(:base, I18n.t('backend.description_event', data: DataPreference.find_by(title: "min_length").try(:content_data)))
    end

    def validate_attendees
      return if !self.multi_attendees && self.participants.present? || self.multi_attendees || self.attendees.present? || self.event_agents.present?
     
      errors.add(:base, I18n.t('backend.attendees_event'))
    end

    def published_range_validation
      return if self.published_at.blank? || (self.published_at >= Time.zone.now - 2.years && self.published_at <= Time.zone.now + 2.years)
      errors.add(:base, I18n.t('backend.limit_published'))
    end

   

    def lobby_activity_with_organization
      return if self.organization.present?
      errors.add(:base, I18n.t('backend.org_event'))
    end

    def reasons_present?
      self.canceled_reasons.present? || self.declined_reasons.present?
    end

end
