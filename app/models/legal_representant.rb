class LegalRepresentant < ActiveRecord::Base
  include ValidationHelper

  belongs_to :address, dependent: :destroy
  
  belongs_to :entity, :polymorphic => true, touch: true
  belongs_to :organization
  
  accepts_nested_attributes_for :address, allow_destroy: true
  # validates :identifier, presence: true
  # validate :name_entity
  # validate :document_identifier

  def fullname
    if business_name.blank?
      str = name
      str += " #{first_surname}"  if first_surname.present?
      str += " #{second_surname}" if second_surname.present?
    else 
      str = business_name
    end
    str
  end

  private

  def document_identifier
    if identifier_type.to_s =="DNI/NIF" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de DNI/NIF")    
    elsif identifier_type.to_s =="NIE" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de NIE") 
    elsif identifier_type.to_s =="Pasaporte" && !validatorPasaport(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de Pasaporte")   
    end

    if identifier_type.blank?
      self.errors.add(:identifier_type, "Es necesario incluir el tipo de documento del representante legal")  
    end
  end

  def name_entity
    if (name.blank? || first_surname.blank?) && business_name.blank?
      self.errors.add(:name, "Se debe rellenar el nombre y primer apellido o la razón social, son campos obligatorios")
    elsif (!name.blank? || !first_surname.blank?) && !business_name.blank?
      self.errors.add(:name, "Solo puede rellenarse el nombre y primer apellido o la razón social, no ambos")     
    end

    if !business_name.blank? && (!first_surname.blank? || !second_surname.blank?)
      self.errors.add(:name, "Los apellidos solo se pueden rellenar con el nombre")
    end
  end


end
