module CalendarCustomHelper

    def get_week_first_monday
        th_export= ""
        (1..6).each {|day| th_export =  th_export + "<th>#{I18n.t('date.abbr_day_names')[day]}</th>"}
        th_export = th_export + "<th>#{I18n.t('date.abbr_day_names')[0]}</th>"

        th_export.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_week_first_sunday
        th_export= ""
        (0..6).each {|day| th_export =  th_export + "<th>#{I18n.t('date.abbr_day_names')[day]}</th>"}

        th_export.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_week_first_monday_with_hour(current = Time.zone.now.to_date)
        th_export= "<th>#{I18n.t('calendar.hour')}</th>"
        (1..6).each {|day| th_export =  th_export + "<th>#{I18n.t('date.abbr_day_names')[day]} - #{(current - (current.wday - day).day).day}</th>"}
        th_export = th_export + "<th>#{I18n.t('date.abbr_day_names')[0]} - #{(current - (current.wday - 7).day).day}</th>"

        th_export.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_days_week_range(events=[], params_aux= {}, current = Time.zone.now, current_item= :selected)
        start_date = (current - (current.wday-1).day).to_date
        end_date = (current - (current.wday-7).day).to_date
        body = ""
        content = true
        parameters = params_aux.dup
        parameters.merge!(:"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}) 

        events.select{|x| Time.zone.parse(x['scheduled']).to_date >= start_date && Time.zone.parse(x['scheduled']).to_date <= end_date}.each do |event|
            body = body + "<tr>"
            body = body + "<td class='calendar_secondary'><b>#{Time.zone.parse(event['scheduled']).strftime("%H:%Mh")}</b></td>"
            (start_date..end_date).each do |week| 
                if !event['scheduled'].blank? && Time.zone.parse(event['scheduled']).to_date == week
                    body = body + "<td><a style='font-size: 0.8rem !important;' href='#{ agenda_path(event['holder_id'],event['holder_name'].try(:parameterize))}'>#{event['title']}</a></td>"
                else
                    body = body + "<td class='calendar_out'></td>"
                end              
                     
            end
            body = body + "</tr>"
        end
       
        body.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_days_month_range(start_date, end_date, events=[], params_aux= {}, current = Time.zone.now, current_item= :selected, content=false)
        body = ""
        return body if (start_date.blank? || end_date.blank?)
        parameters = params_aux.dup
        parameters.merge!(:"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}) 

        (start_date..end_date).each do |week| 
            if week==start_date ||  week.wday == 1
                body = body + "<tr>"
                if week==start_date
                    if week.wday == 0
                        (1..6).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters, content)
                    else
                        (1..week.wday-1).each {|d|  body = body + get_out_day}
                        body = body + get_day(week, events, parameters, content)
                    end
                elsif week==end_date
                    body = body + get_day(week, events, parameters, content)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters, content)
                    body = body + "</tr>" if  week.wday == 0
                end
            else
                if week==end_date
                    body = body + get_day(week, events, parameters, content)
                    if week.wday == 6
                        body = body + get_out_day
                    elsif week.wday != 0
                        (week.wday+1..7).each {|d|  body = body + get_out_day}
                    end
                    body = body + "</tr>"
                else
                    body = body + get_day(week, events, parameters, content)
                    body = body + "</tr>" if  week.wday == 0
                end
            end
        end

        body.html_safe
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_now_calendar_week(params_aux = {},  item=nil)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        parameters.merge!({:"#{item}" => {:month =>  nil , :year => nil}, :set_date_calendar => nil}) 

        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_now_calendar(params_aux = {},  item=nil)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        parameters.merge!({:"#{item}" => {:month =>  nil , :year => nil}}) 

        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_next_month(params_aux = {},current=Time.zone.now, item=nil)
        parameters = params_aux.dup
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        return current_path_with_query_params(parameters) if current.blank? || item.blank? 
            
        if current.to_date.month.to_i + 1 > 12
            parameters.merge!(:"#{item}" => {:month => 1, :year => (current.to_date.year + 1)}) 
        else
            parameters.merge!(:"#{item}" => {:month =>  (current.to_date.month + 1), :year => current.to_date.year}) 
        end
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_previous_month(params_aux = {},current=Time.zone.now, item=nil)
        parameters = params_aux.dup
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        return current_path_with_query_params(parameters) if current.blank? || item.blank? 
            
        if current.to_date.month.to_i - 1 < 1
            parameters.merge!(:"#{item}" => {:month => 12, :year => (current.to_date.year - 1)}) 
        else
            parameters.merge!(:"#{item}" => {:month =>  (current.to_date.month - 1), :year => current.to_date.year}) 
        end
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_next_week(params_aux = {},current=Time.zone.now, item=nil)
        parameters = params_aux.dup
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        return current_path_with_query_params(parameters) if current.blank? || item.blank? 
            
        new_date = (!parameters[:set_date_calendar].blank? ? Time.zone.parse(parameters[:set_date_calendar]).to_date : Time.zone.now.to_date) + 7.day
        parameters.merge!({:set_date_calendar => new_date, :"#{item}" => {:month =>  new_date.month , :year => new_date.year}}) 

        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_previous_week(params_aux = {},current=Time.zone.now, item=nil)
        parameters = params_aux.dup
        [:month, :year].each {|type| parameters = parameters.tap{|x| x[item].delete(type) if !x[item].blank?}}
        return current_path_with_query_params(parameters) if current.blank? || item.blank? 

        new_date = (!parameters[:set_date_calendar].blank? ? Time.zone.parse(parameters[:set_date_calendar]).to_date : Time.zone.now.to_date) - 7.day
        parameters.merge!({:set_date_calendar => new_date, :"#{item}" => {:month =>  new_date.month , :year => new_date.year}}) 
        
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_tab_calendar(params_aux = {},  item=nil, current=Time.zone.now, current_item=nil)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        if item.to_s=="all"
            parameters.merge!({:tab_calendar => item,:set_date => nil, :"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}}) 
        else
            parameters.merge!({:tab_calendar => item,:set_date => parameters[:set_date] || Time.zone.now.to_date, :"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}}) 
        end
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_tab_agend(params_aux = {},  item=nil, current=Time.zone.now, current_item=nil)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        parameters.merge!({:tab_agend => item, :"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}}) 
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00013: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def set_tab_agend_calendar(params_aux = {},  item=nil, current=Time.zone.now, current_item=nil)
        parameters = params_aux.dup
        return current_path_with_query_params(parameters) if parameters.blank? || item.blank?
        parameters.merge!({:tab_agend_calendar => item, :"#{current_item}" => {:month =>  current.to_date.month , :year => current.to_date.year}}) 
        current_path_with_query_params(parameters)
    rescue => e
        begin
          Rails.logger.error("COD-00014: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    private

    def get_day(week, events, parameters, content=false)
        aux = "<td style='width: 14%; #{"height: 10rem; " if content}' #{"onclick=\"javascript:window.location = '#{current_path_with_query_params(parameters.dup.except!(:set_date).merge!({set_date: week}))}'\"" if !content}"
        if (!events.select{|x| !x['scheduled'].blank? && Time.zone.parse(x['scheduled']).to_date == week}.blank?)
            aux = aux + " class='#{week==Time.zone.now.to_date ? 'calendar_event_now' : (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) ? 'calendar_selected' : 'calendar_event'}'
            >#{"<a class='#{week==Time.zone.now.to_date ? 'calendar_event_now' : (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) ? 'calendar_selected' : 'calendar_event'}' href='#{current_path_with_query_params(parameters.dup.except!(:set_date).merge!({set_date: week}))}'>" if !content}"
        else
            aux = aux + " class='#{week==Time.zone.now.to_date ? 'calendar_now' : (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) ? 'calendar_selected' : 'calendar_without'}'
            >#{"<a class='#{week==Time.zone.now.to_date ? 'calendar_now' : (!parameters[:set_date].blank? && week == parameters[:set_date].to_date) ? 'calendar_selected' : 'calendar_without'}' href='#{current_path_with_query_params(parameters.dup.except!(:set_date).merge!({set_date: week}))}'>" if !content}"
        end
        aux = aux + "#{week.day}#{"</a>" if !content}"
        if content
            aux = aux + "<br><ul>"
            events.select{|x| !x['scheduled'].blank? && Time.zone.parse(x['scheduled']).to_date == week}.each do |event|
                aux = aux + "<li style='font-size: 0.8rem !important;'><a class='calendar_link' href='#{ agenda_path(event['holder_id'],event['holder_name'].try(:parameterize))}'>- <strong>#{ Time.zone.parse(event['scheduled']).strftime("%H:%Mh")}</strong>: <i>#{event['title']}</i></a></li>"
            end
            aux = aux + "</ul>"
        end
        aux = aux + "</td>"
    end

    def get_out_day
        "<td class='calendar_out'></td>"
    end

end