module ValidationHelper
    extend ActiveSupport::Concern
    LETTER_VALUE_DNI = ["T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"].freeze
    LETTER_VALUE_CIF = ["J","A","B","C","D","E","F","G","H","I"].freeze

    def validadorNIF_DNI_NIE(dni)
        valid = dni.match(/^[XYZxyz]?\d{5,8}[A-Za-z]$/)
        if !valid.blank?
            num_aux = dni.gsub(/[A-Za-z]/,'').to_i
            letter_aux = dni.gsub(/[0-9]/,'').to_s.upcase

            result= num_aux % 23

            
            if letter_aux.length > 1 && LETTER_VALUE_DNI[result].to_s == letter_aux[1] || LETTER_VALUE_DNI[result].to_s == letter_aux
                true
            else
                false
            end
        else
            false
        end
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        false
    end

    def validadorCIF(value)
        #corregir en el futuro
            pares = 0
            impares = 0
            uletra = ["J", "A", "B", "C", "D", "E", "F", "G", "H", "I"]
            texto = value.upcase
            regular = /^[ABCDEFGHKLMNPQRS]\d{7}[0-9,A-J]$/#g);
            if regular.match(value).blank?
              false
            else
              ultima = texto[8,1]
        
              [1,3,5,7].collect do |cont|
                xxx = (2 * texto[cont,1].to_i).to_s + "0"
                impares += xxx[0,1].to_i + xxx[1,1].to_i
                pares += texto[cont+1,1].to_i if cont < 7
              end
        
              #xxx = (2 * texto[8,1].to_i).to_s + "0"
              #impares += xxx[0,1].to_i + xxx[1,1].to_i
        
              suma = (pares + impares).to_s
              unumero = suma.last.to_i
              unumero = (10 - unumero).to_s
              unumero = 0 if(unumero.to_i == 10)
              ((ultima.to_i == unumero.to_i) || (ultima == uletra[unumero.to_i]))
            end
        # return true
        # valid = dni.match(/^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/)
        # return false if valid.blank?
        # letter = dni.match(/^([ABCDEFGHJKLMNPQRSUVW])/).to_s
        # number  = dni.match(/(\d{7})/).to_s
        # control = dni.match(/([0-9A-J])$/).to_s
    
        # even_sum = 0
        # odd_sum = 0
        # i = 0
    
        # number.to_s.split('').each do |n|
        #     if i % 2 == 0
        #         n_aux = n.to_i * 2
        #         odd_sum = odd_sum + (n_aux < 10 ? n_aux : n_aux - 9)
        #     else
        #         even_sum = even_sum  + n.to_i
        #     end

        #     i = i + 1
        # end
        
        # control_digit = (10 - (even_sum + odd_sum).to_s[-1].to_i)
        # control_letter = LETTER_VALUE_CIF[control_digit.to_i]   
        
        # if letter.match(/[ABEH]/) 
        #     return control.to_s == control_digit.to_s
        # elsif letter.match(/[KPQS]/)
        #     return control.to_s == control_letter.to_s
        # else
        #     return control.to_s == control_digit.to_s || control.to_s == control_letter.to_s
        # end            
        
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        false
    end

    def validatorPasaport(pasaport)
        valid = pasaport.match(/^[a-zA-Z]{3}[0-9]{6}[a-zA-Z]$/)
        if !valid.blank?
            true
        else
            false
        end
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        false
    end    

end


