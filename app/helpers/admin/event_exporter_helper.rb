module Admin::EventExporterHelper
    extend ActiveSupport::Concern 
    def stream_query_rows_private(export=nil, extended = false)

        return if export.blank?
        filter = export.map(&:id).to_s.gsub(/\[|\]/,"")
        date_year = DataPreference.find_by(title: "expired_year")
        date_year= date_year.blank? ? 2 : date_year.content_data

        query = "COPY (Select distinct 
            events.title as \"#{I18n.t("events_exporter.title")}\",
            #{ Admin::StreamingHelper.transform_html_data("events.description")} as \"#{I18n.t("events_exporter.description")}\",
            COALESCE(CAST(events.scheduled as varchar),'-') as \"#{I18n.t("events_exporter.scheduled")}\",
            #{" COALESCE(CAST(events.updated_at as varchar),'-') as \"#{I18n.t("events_exporter.updated_at")}\", " if extended}
            (Select CONCAT(holders.last_name,' ',holders.first_name) from holders,positions where positions.id=events.position_id AND positions.holder_id = holders.id) as \"#{I18n.t("events_exporter.holder_name")}\",
            (Select positions.title from positions where positions.id=events.position_id ) as \"#{I18n.t("events_exporter.holder_position")}\",
            CONCAT(
            (Select string_agg(CONCAT(holders.last_name,' ', holders.first_name,' (',positions.title,')'),' / ') from positions, participants, holders where holders.id=positions.holder_id
                AND participants.event_id=events.id AND positions.id=participants.position_id),' / ',
                (Select string_agg(CONCAT(attendees.name,' (',attendees.position,')'),' / ') from attendees where attendees.event_id=events.id)                
                ) as \"#{I18n.t("events_exporter.position_names")}\",
            events.location as \"#{I18n.t("events_exporter.location")}\"
            #{", CASE events.status WHEN 0 THEN 'SOLICITADO' 
                WHEN 1 THEN 'ACEPTADO' WHEN 2 THEN 'REALIZADO' WHEN 3 THEN 'RECHAZADO' 
                WHEN 4 THEN 'CANCELADO' ELSE '-' END as \"#{I18n.t("events_exporter.status")}\",
            events.notes as \"#{I18n.t("events_exporter.notes")}\", 
            events.canceled_reasons as \"#{I18n.t("events_exporter.canceled_reasons")}\", 
            COALESCE(CAST(events.published_at as varchar),'-') as \"#{I18n.t("events_exporter.published_at")}\", 
            COALESCE(CAST(events.canceled_at as varchar),'-') as \"#{I18n.t("events_exporter.canceled_at")}\", 
            CASE events.lobby_activity WHEN 't' THEN 'SI' WHEN 'f' THEN 'NO' ELSE '-' END as \"#{I18n.t("events_exporter.lobby_activity")}\", 
            CASE WHEN events.old_event_lobby = 't' AND events.lobby_activity = 't' THEN 'Evento antiguo'
                WHEN events.lobby_activity = 't' AND NOW() > (select organizations.renovation_date + interval '#{date_year} year' from organizations where organizations.id = events.organization_id) THEN 'Evento caducado' 
                WHEN events.lobby_activity = 't' AND NOW() <= (select organizations.renovation_date + interval '#{date_year} year' from organizations where organizations.id = events.organization_id) THEN 'Evento sin caducado'
                ELSE '' END as \"#{I18n.t("events_exporter.lobby_expired_formatted")}\", 
            events.organization_name as \"#{I18n.t("events_exporter.organization_name")}\",
            events.lobby_scheduled as \"#{I18n.t("events_exporter.lobby_scheduled")}\", 
            #{ Admin::StreamingHelper.transform_html_data("events.general_remarks")} as \"#{I18n.t("events_exporter.general_remarks")}\", 
            events.lobby_contact_firstname as \"#{I18n.t("events_exporter.lobby_contact_firstname")}\", 
            COALESCE(CAST(events.accepted_at as varchar),'-') as \"#{I18n.t("events_exporter.accepted_at")}\", 
            events.declined_reasons as \"#{I18n.t("events_exporter.declined_reasons")}\", 
            COALESCE(CAST(events.declined_at as varchar),'-') as \"#{I18n.t("events_exporter.declined_at")}\", 
            events.lobby_contact_lastname as \"#{I18n.t("events_exporter.lobby_contact_lastname")}\", 
            events.lobby_contact_email as \"#{I18n.t("events_exporter.lobby_contact_email")}\", 
            events.lobby_contact_phone as \"#{I18n.t("events_exporter.lobby_contact_phone")}\", 
            events.manager_general_remarks as \"#{I18n.t("events_exporter.manager_general_remarks")}\", 
            (Select string_agg(CONCAT(users.first_name,' ',users.last_name),' / ') from positions, manages, users where positions.id=events.position_id AND manages.holder_id=positions.holder_id AND manages.user_id=users.id) as \"#{I18n.t("events_exporter.contributed_by")}\", 
            COALESCE(CAST(events.created_at as varchar),'-') as \"#{I18n.t("events_exporter.created_at")}\", 
            COALESCE((Select CONCAT(users.first_name,' ',users.last_name) from users where users.id=events.user_id),'-') as \"#{I18n.t("events_exporter.user_name")}\" "  if extended}       
        from events #{ "WHERE events.id IN (#{filter}) " if !filter.blank? }
        ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"     

        Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end