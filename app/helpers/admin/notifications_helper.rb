module Admin::NotificationsHelper
    def get_num_active_notifications
        Notification.pending_view.count
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        0
    end

    def active_notification(status)
        [I18n.t('backend.notification_status.pending'), I18n.t('backend.notification_status.recover')].include?(status)
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def historic_notification(status)
        [I18n.t('backend.notification_status.reject'), I18n.t('backend.notification_status.permit')].include?(status)
    rescue => e
        begin
          Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_types_notification(type="active")
        if type=="active"
            {
                I18n.t('backend.notification_status.pending') => I18n.t('backend.notification_status.pending'),
                I18n.t('backend.notification_status.recover') => I18n.t('backend.notification_status.recover')
            }
        elsif type=="historic"
            {
                I18n.t('backend.notification_status.reject') => I18n.t('backend.notification_status.reject'),
                I18n.t('backend.notification_status.permit') => I18n.t('backend.notification_status.permit'),
                I18n.t('backend.notification_status.manual_permit') => I18n.t('backend.notification_status.manual_permit')
            }
        end
    rescue => e
        begin
          Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end

    def get_types_action_notification
        {
            I18n.t("backend.notification_action.insert") => ["876","619"],
            I18n.t("backend.notification_action.update") => ["877","620"],
            I18n.t("backend.notification_action.delete") => ["878","621"],
            I18n.t("backend.notification_action.reneue") => []
        }
    rescue => e
        begin
          Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end 