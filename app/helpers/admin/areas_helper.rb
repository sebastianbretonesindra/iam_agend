module Admin::AreasHelper

  def parent_label(area)
    'data-tt-parent-id='+area.parent.id.to_s+''.html_safe if area.parent.present?
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def nested_areas(areas)
    areas.map { |area| render(area) + nested_areas(area.children.order(:title)) }.join.html_safe unless areas.blank?
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
