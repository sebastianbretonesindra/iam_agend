module Admin
  module EventsHelper
    def check_filter_lobby_activity
      params[:lobby_activity] == '1'
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def admin_or_mananger_edit?
      (current_user.admin? || current_user.user?) &&
      (params[:action] == "edit" || params[:action] == "update")
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def calculate_firstname(event)
      if event.try(:lobby_contact_firstname).present?
        event.lobby_contact_firstname
      elsif current_user.lobby?
        current_user.first_name
      else
        ""
      end
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def calculate_lastname(event)
      if event.try(:lobby_contact_lastname).present?
        event.lobby_contact_lastname
      elsif current_user.lobby?
        current_user.last_name
      else
        ""
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def calculate_phone(event)
      if event.try(:lobby_contact_phone).present?
        event.lobby_contact_phone
      elsif current_user.lobby?
        current_user.phones
      else
        ""
      end
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def calculate_email(event)
      if event.try(:lobby_contact_email).present?
        event.lobby_contact_email
      elsif current_user.lobby?
        current_user.email
      else
        ""
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def event_expired_search_options(selected)
      rev = {}
      rev.merge!("Todos" => nil)
      ["old_event", "new_event", "new_event_expired"].each {|x| rev.merge!(I18n.t("expired.#{x}") => x)}
      
      options_for_select(rev, selected)
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def event_status_search_options(selected)
      rev = {}
      t("backend.status").each_with_index { |(k, v)| rev[v] = k }
      options_for_select(rev, selected)
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def holder_name_by_position_id(position_id)
      Position.find(position_id).try(:full_name) if position_id.present?
    rescue => e
      begin
        Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def event_attachments_download_dropdown_id(event)
      "event_#{event.try(:id)}_attachments_dropdown"
    rescue => e
      begin
        Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

  end
end
