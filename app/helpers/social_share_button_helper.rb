module SocialShareButtonHelper

  
      def social_share_button_tag(title = "", opts = {})
        opts[:allow_sites] ||= SocialShareButton.config.allow_sites
  
        extra_data = {}
        rel = opts[:rel]
        html = []
        html << "<div class='social-share-button' data-title='#{h title}' data-img='#{opts[:image]}'"
        html << "data-url='#{opts[:url]}' data-desc='#{opts[:desc]}' data-via='#{opts[:via]}'>"
  
        opts[:allow_sites].each do |name|
          extra_data = opts.select { |k, _| k.to_s.start_with?('data') } if name.eql?('tumblr')
          special_data = opts.select { |k, _| k.to_s.start_with?('data-' + name) }
          
          special_data["data-wechat-footer"] = t "social_share_button.wechat_footer" if name == "wechat"
  
          link_title = t "social_share_button.share_to", :name => t("social_share_button.#{name.downcase}")
          html << link_to(array_icons(name, title), array_url(name,title,opts[:url]), { :rel => ["nofollow", rel],
                                     "data-site" => name,
                                     :target => "_blank",
                                     :title => h(link_title) }.merge(extra_data).merge(special_data))
        end
        html << link_to("<i class='fa fa-print fa-2x'><span class='sr-only'>Imprimir pantalla</span></i>".html_safe, array_url("print","",""), { :rel => ["nofollow", rel],
          :title => "Imprimir pantalla" })
        html << "</div>"
        raw html.join("\n")
      end


      private

      def array_url(name,title, url)
        case name
        when "twitter"          
          "https://twitter.com/intent/tweet?url=#{url}&text=#{title}"
        when "facebook"
          "http://www.facebook.com/sharer/sharer.php?u=#{url}"
        when "google_bookmark"
          "https://www.google.com/bookmarks/mark?op=edit&output=popup&bkmk=#{url}&title=#{title}"
        when "print"
          "javascript:print()"
        else
          "#"
        end       
      end

      def array_icons(name, title)
        case name
        when "twitter"
          "<i class='fa fa-twitter-square fa-2x'><span class='sr-only'>#{title}</span></i>".html_safe
        when "facebook"
          "<i class='fa fa-facebook-square fa-2x'><span class='sr-only'>#{title}</span></i>".html_safe
        when "google_bookmark"
          "<i class='fa fa-google-plus-square fa-2x'><span class='sr-only'>#{title}</span></i>".html_safe
        else
          ""
        end       
      end
      
  end