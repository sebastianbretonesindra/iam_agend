module EventsHelper

  def cancelable_event?(event)
    !event.canceled?
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def declinable_or_aceptable_event?(event)
    !event.canceled? && !event.declined? && (!event.accepted? || (event.status == "requested"))
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def event_title(current_user)
    if current_user.lobby?
      t("backend.title_event_lobby")
    else
      t("backend.title_event")
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def event_new(current_user)
    if current_user.lobby?
      t("backend.new_requeseted_event")
    else
      t("backend.new_event")
    end
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def event_description(current_user)
    if current_user.lobby?
      t("backend.description_lobby")
    elsif current_user.user?
      t('backend.description_manager')
    else
      t('backend.description')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def reason_text(event)
    if event.declined_reasons.present?
      event.try(:declined_reasons).try {|x| x.html_safe}
    elsif event.canceled_reasons.present?
      event.try(:canceled_reasons).try {|x| x.html_safe}
    end
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def stream_query_rows(export=nil, url)
    ids = nil
    ids = export.map(&:id).to_s.gsub(/\[|\]/,"") if !export.blank?
    query = "COPY (Select 
    COALESCE(UPPER(events.title),'-') as \"#{I18n.t('events_exporter.title')}\",
    COALESCE((Select regexp_replace(replace(replace(replace(replace(replace(replace(events.description, '&eacute','é'), '&oacute','ó'), '&aacute','á'), '&iacute','í'), '&uacute','ú'), '&ntilde', 'ñ'), '<[^>]*>|[;]*','','g')), '-') as \"#{I18n.t("events_exporter.description")}\",
    COALESCE(CAST(to_char(events.scheduled,'yyyy/MM/dd') as varchar),'-') as \"#{I18n.t('events_exporter.scheduled')}\",
    (Select CONCAT(holders.last_name,' ',holders.first_name) from holders,positions where positions.id=events.position_id AND positions.holder_id = holders.id) as \"#{I18n.t("events_exporter.holder_name")}\",
    (Select positions.title from positions where positions.id=events.position_id ) as \"#{I18n.t("events_exporter.holder_position")}\",
    CONCAT(
      (Select string_agg(CONCAT(holders.last_name,' ', holders.first_name,' (',positions.title,')'),' / ') from positions, participants, holders where holders.id=positions.holder_id
          AND participants.event_id=events.id AND positions.id=participants.position_id),' / ',
          (Select string_agg(CONCAT(attendees.name,' (',attendees.position,')'),' / ') from attendees where attendees.event_id=events.id)                
          ) as \"#{I18n.t("events_exporter.position_names")}\",
      events.location as \"#{I18n.t("events_exporter.location")}\"
    FROM events
     #{"WHERE events.id IN (#{ids})" unless ids.blank?} ORDER BY \"#{I18n.t('events_exporter.scheduled')}\" desc
    ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"
    Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end 
  end

end
