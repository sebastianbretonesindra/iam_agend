module UserHelper
  def user_by_role(user)
    return User.model_name.human if user.admin? || user.user?
    "Lobby"
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def user_stream_query_rows(export=nil, url)
    pos_ids = export.map(&:id).to_s.gsub(/\[|\]/,"") if !export.blank?
    query = "COPY (Select distinct
    COALESCE(UPPER(users.first_name),'-') as \"#{I18n.t('backend.users_first_name')}\",
    COALESCE(UPPER(CONCAT(users.last_name, ' ', users.second_last_name)),'-') as \"#{I18n.t('backend.users_last_name')}\",
    CASE users.role WHEN '0' THEN 'Gestor' WHEN '1' THEN 'Administrador' WHEN '2' THEN 'Lobby' ELSE '-' END as \"#{I18n.t('backend.role')}\",
    COALESCE(users.positions,'-') as \"#{I18n.t('backend.users_position')}\",
    COALESCE(users.email,'-') as \"#{I18n.t('backend.holder_csv.email')}\",
    COALESCE((select string_agg(UPPER(CONCAT(holders.last_name, ', ', holders.first_name)), ' || ') from holders, manages WHERE holders.id = manages.holder_id AND manages.user_id = users.id),'-') as \"#{I18n.t('backend.managed_holders')}\",
    COALESCE('-') as \"#{I18n.t('backend.holder_csv.area')}\"
    FROM users
    #{"WHERE users.id IN (#{pos_ids.to_s.gsub(/\[|\]/,'')})" unless pos_ids.blank?}) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end



  def manage_stream_query_rows(url)
    query = "COPY (Select distinct
    COALESCE(UPPER(users.first_name),'-') as \"#{I18n.t('backend.users_first_name')}\",
    COALESCE(UPPER(CONCAT(users.last_name, ' ', users.second_last_name)),'-') as \"#{I18n.t('backend.users_last_name')}\",
    CASE users.role WHEN '0' THEN 'Gestor' WHEN '1' THEN 'Administrador' WHEN '2' THEN 'Lobby' ELSE '-' END as \"#{I18n.t('backend.role')}\",
    COALESCE(users.positions,'-') as \"#{I18n.t('backend.users_position')}\",
    COALESCE(users.email,'-') as \"#{I18n.t('backend.holder_csv.email')}\",
    COALESCE((select string_agg(UPPER(CONCAT(holders.last_name, ', ', holders.first_name)), ' || ') from holders, manages WHERE holders.id = manages.holder_id AND manages.user_id = users.id),'-') as \"#{I18n.t('backend.managed_holders')}\",
    COALESCE('-') as \"#{I18n.t('backend.holder_csv.area')}\"
    FROM users, manages WHERE users.role = 0 AND  manages.user_id = users.id AND users.user_key not in (Select holders.user_key FROM holders)
    ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  
end
