module MenuHelper
  def current_class(active_path)
    return 'active' if request.path == active_path
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def admin_current_class
    return 'active' if request.path.include?("/admin") || request.path.include?("/events")
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
