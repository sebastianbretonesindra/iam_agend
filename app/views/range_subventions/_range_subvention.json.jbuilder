json.extract! range_subvention, :id, :created_at, :updated_at
json.url range_subvention_url(range_subvention, format: :json)
