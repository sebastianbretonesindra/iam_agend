json.extract! notification_effect, :id, :created_at, :updated_at
json.url notification_effect_url(notification_effect, format: :json)
