xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'
xml.tag! "soap:Envelope", "xmlns:soap" => 'http://schemas.xmlsoap.org/soap/envelope/', "xmlns:xsd" => "http://www.w3.org/2001/XMLSchema", "xmlns:tns"=>"http://wsintbrg.bareg.iam.es" do
  xml.tag! "soap:Body" do
    xml.tag! "soap:Fault" do
      xml.faultcode error_code
      xml.faultstring error_message
    end
  end
end