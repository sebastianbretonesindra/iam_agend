class EventMailer < ApplicationMailer

  def cancel_by_lobby(event)
    # pending titular email
    @holder_name = event.holder_name
    @to = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email#event.position.holder.users.collect(&:email).join(",")
    @lobby_name = event.lobby_user_name.present? ? event.lobby_user_name : event.organization.user.first_name
    @reasons = event.canceled_reasons
    @name = event.user.full_name
    @event_title = event.title
    @event_location = event.location
    @event_scheduled = l event.scheduled, format: :long if event.scheduled
    @event_description = event.description
    @event_reference = event.id
    manages_emails = event.position.holder.users.collect(&:email).join(",")
    to = manages_emails
    subject = t('mailers.cancel_event.subject', event_reference: @event_reference)
    mail(to: to, bcc: "registrodelobbies@madrid.es", subject: subject)
  end

  def accept(event)
    @holder_name = event.holder_name
    @event_title = event.title
    @event_description = event.description
    @event_reference = event.id
    @event_location = event.location
    @accepted_day = l event.scheduled, format: :long if event.scheduled
    @accepted_at = l event.scheduled, format: :time if event.scheduled
    to = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email
    subject = t('mailers.accept_event.subject', event_reference: @event_reference)
    mail(to: to, bcc: "registrodelobbies@madrid.es", subject: subject)
  end

  def decline(event)
    manages_emails = event.position.holder.users.collect(&:email).join(",")
    lobby_email = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email
    bbc =  "registrodelobbies@madrid.es, " + manages_emails
    @holder_name = event.holder_name
    @reasons = event.declined_reasons
    @event_title = event.title
    @event_description = event.description
    @event_reference = event.id
    subject = t('mailers.decline_event.subject', event_reference: @event_reference)
    mail(to: lobby_email, bcc: bbc, subject: subject)
  end

  def create(event)
    manages_emails = event.position.holder.users.collect(&:email).join(",")
    holder = User.find_by(user_key: event.position.holder.user_key).try(:email).to_s
    bcc = "registrodelobbies@madrid.es"
    if !holder.blank?
      manages_emails = manages_emails + "," + holder
    end
    @to = manages_emails
    @lobby_contact_full_name = event.lobby_user_name.present? ? event.lobby_user_name : "#{event.organization.user.first_name} #{event.organization.user.last_name}"
    @event_title = event.title
    @event_description = event.description
    @event_reference = event.id
    @event_scheduled = l event.scheduled, format: :long if event.scheduled
    @lobby_name = event.organization_name.present? ? event.organization_name : "#{event.organization.name} #{event.organization.first_surname} #{event.organization.second_surname}"
    @holder_name = event.holder_name
    @lobby_contact_email = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email
    subject = t('mailers.create_event.subject', event_reference: @event_reference)
    mail(to: @to, bcc: bcc, subject: subject)
  end

  def cancel_by_holder(event)
    @holder_name = event.holder_name
    @to = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email#event.position.holder.users.collect(&:email).join(",")
    @lobby_name = event.lobby_user_name.present? ? event.lobby_user_name : event.organization.user.first_name
    @name = event.user.full_name
    @reasons = event.canceled_reasons
    @event_title = event.title
    @event_location = event.location
    @event_scheduled = l event.scheduled, format: :long if event.scheduled
    @event_description = event.description
    @event_reference = event.id
    to = event.lobby_contact_email.present? ? event.lobby_contact_email : event.organization.user.email
    subject = t('mailers.cancel_event_by_holder.subject', event_reference: @event_reference)
    mail(to: to, bcc: "registrodelobbies@madrid.es", subject: subject)
  end

end
