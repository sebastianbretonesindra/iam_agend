class ManageMailer < ApplicationMailer
    def sender(emails, mail)
        @body = mail.try(:email_body)
        
       mail(to: emails, bcc: mail.try(:fields_cco), cc: mail.try(:fields_cc),from: mail.try(:sender), subject: mail.try(:subject))
       #mail(to: emails, from: mail.try(:sender), subject: mail.try(:subject))
    end  
end
