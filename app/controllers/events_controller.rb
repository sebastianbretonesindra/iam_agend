class EventsController < AdminController
  
  load_and_authorize_resource
  before_action :set_holders, only: [:new, :edit, :create, :clone ]
  before_action :set_event, only: [:edit, :update, :clone ]
  after_action :remove_attendees, only: [:update, :create]

  include AuthHelper
  # require 'google/api_client/client_secrets.rb'
  # require 'google/apis/calendar_v3'

  
  def index
    @events = current_user.admin? || current_user.lobby? ? list_admin_events : list_user_events

    unless !current_user.blank? && current_user.admin?
      @events = @events.where("cast(current_date as date) >= cast(to_date(?,'YYYY-MM-DD') as date)",DataPreference.find_by(title: 'limit_events').content_data) if !DataPreference.find_by(title: 'limit_events').try(:content_data).blank?
    else
      if !DataPreference.find_by(title: 'show_admin_limit').try(:content_data).blank? && DataPreference.find_by(title: 'show_admin_limit').try(:content_data) == "2"
        @events = @events.where("cast(current_date as date) >= cast(to_date(?,'YYYY-MM-DD') as date)",DataPreference.find_by(title: 'limit_events').content_data) if !DataPreference.find_by(title: 'limit_events').try(:content_data).blank?
      end
    end

    @events_paginated = @events.order(scheduled: :desc).page(params[:page]).per(100)
    respond_to do |format|
      format.csv { stream_csv_report_private(@events, true).delay  }
      format.html
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @event = Event.new(event_params)
    @event.user = current_user
    @event.old_event_lobby = false
    if @event.save
      begin
      EventMailer.create(@event).deliver_now if @event.status == "requested"
      rescue => e
        begin
          Rails.logger.error("SMS-ERROR: #{e}")
        rescue
        end
      end
      clone_attachments
      redirect_to events_home_path(current_user, false),
                  notice: t('backend.successfully_created_record')
    else
      flash[:alert] = t('backend.review_errors')
      render :new
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def new
    if current_user.lobby? && !current_user.organization.blank? && current_user.try(:organization).try(:agents).blank?      
      redirect_to admin_organization_agents_path(current_user.organization), alert: t('backend.event.add_agents')      
    elsif current_user.lobby? && current_user.organization.blank?     
      redirect_to events_home_path(current_user, false), alert: t('backend.event.add_agents')
    end
    @event.lobby_activity = current_user.lobby?
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def clone
    clone_event = @event.dup
    clone_event.scheduled = nil
    clone_event.published_at = nil

    @event.participants.each do |p|
      clone_participant = p.dup
      clone_participant.participants_event = clone_event
      clone_event.participants << clone_participant
    end

    @event.attendees.each do |p|
      clone_attendees = p.dup
      clone_attendees.event = clone_event
      clone_event.attendees << clone_attendees
    end

    @event.event_agents.each do |p|
      clone_participant = p.dup
      clone_participant.event = clone_event
      clone_event.event_agents << clone_participant
    end

    @event.event_represented_entities.each do |p|
      
      clone_event_represented_entities = p.dup
      clone_event_represented_entities.event = clone_event
      clone_event.event_represented_entities << clone_event_represented_entities
    end
    @event = clone_event
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update
    @event.user = current_user
    if @event.update_attributes(event_params) && @event.save
      if !current_user.lobby?
        if @event.decline == 'true'
          begin
            EventMailer.decline(@event).deliver_now
          rescue =>e
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        elsif @event.accept == 'true'
          begin
            EventMailer.accept(@event).deliver_now
          rescue =>e
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        elsif @event.cancel == 'true'
          begin
            EventMailer.cancel_by_holder(@event).deliver_now
          rescue =>e
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        end
      elsif @event.cancel == 'true'
        begin
          EventMailer.cancel_by_lobby(@event).deliver_now
        rescue =>e
          begin
            Rails.logger.error("SMS-ERROR: #{e}")
          rescue
          end
        end
      end
      clone_attachments
      redirect_to events_home_path(current_user, false),
                  notice: t('backend.successfully_updated_record')
    else
      # ExportOutlook.new(@event.id, token).create if !session[:azure_token].blank?
      # ExportGoogle.new(session[:event_id],session[:authorization]).create_event if !session[:authorization].blank?
      set_holders
      flash[:alert] = t('backend.review_errors')
      render :edit
    end
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def destroy
    @event.destroy
    redirect_to events_home_path(current_user, true),
                notice: t('backend.successfully_destroyed_record')
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def get_title
    Event.find(params[:id]).title if params[:action] == 'destroy'
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def calendar_view
    @events = Event.all
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  
  def download
    formato = params[:formato]
    if formato.blank? && (formato.to_s == "xlsx" || formato.to_s == "csv" || formato.to_s == "json")
      redirect_to holders_path, alert: t('backend.no_format_errors')
    elsif File.file?("#{Rails.root}/public/export/agendas.#{formato}")
      send_file(
        "#{Rails.root}/public/export/agendas.#{formato}",
        filename: "agendas.#{formato}",
        type: "application/#{formato}"
      )
    else
      redirect_to holders_path, alert: t('backend.no_file_errors')
    end
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def export_outlook
      require 'icalendar'
      event=Event.find(params[:id])
      cal = Icalendar::Calendar.new
      cal.event do |e|
        e.dtstart     = Icalendar::Values::DateTime.new(event.try(:scheduled))
        e.summary     = event.try(:title)
        e.description = ActionController::Base.helpers.strip_tags(event.try(:description))
        e.url         = event.try(:slug)
        e.location    = event.try(:location)
      end
      
      cal.prodid = '-//Acme Widgets, Inc.//NONSGML ExportToCalendar//EN'
      cal.version = '2.0'

      send_data cal.to_ical, type: 'text/calendar', disposition: 'attachment', filename: "evento.ics"

  rescue => e
    begin
      Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end

    redirect_to event_path(event), alert: t('backend.event_not_exported_date')
  end



  private

  def remove_attendees
    if @event.multi_attendees.to_s == 'true'
      @event.attendees.each {|item| item.destroy}
      @event.participants.each {|item| item.destroy}
    end
  end

  def client_options
    {
      client_id: Rails.application.secrets.google_client_id,
      client_secret: Rails.application.secrets.google_client_secret,
      authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      scope: Google::Apis::CalendarV3::AUTH_CALENDAR,
      redirect_uri: config.action_mailer.asset_host.to_s
    }
  end

  def clone_attachments
    @event.event_agents.each do |agent|
      real=agent.get_real_agent
      begin
        agent.attachments = real.attachments
        agent.save
      rescue
      end
    end
  end

  def event_params
    params.require(:event).permit(:event_id,:title, :description, :location, :scheduled, :position_id, :search_title, :search_person,
                                  :search_start_date, :search_end_date,
                                  :lobby_activity, :notes, :status, :canceled_reasons, :published_at, :cancel, :decline,
                                  :declined_reasons, :organization_id, :organization_name, :lobby_scheduled, :general_remarks,
                                  :lobby_contact_firstname, :accept, :lobby_contact_lastname,
                                  :lobby_contact_phone, :lobby_contact_email, :manager_general_remarks, :multi_attendees,
                                  event_represented_entities_attributes: [:id, :name, :_destroy],
                                  event_agents_attributes: [:id, :name, :_destroy],
                                  attendees_attributes: permit_attendees,  
                                  participants_attributes: permit_participants,                           
                                  attachments_attributes: [:id, :title, :file, :public, :description, :_destroy])
  end

  def permit_attendees
    if params[:event][:multi_attendees].to_s != 'true'
      [:id, :name, :position, :company, :_destroy]
    end
  end

  def permit_participants
    if params[:event][:multi_attendees].to_s != 'true'
      [:id, :position_id, :_destroy]
    end
  end

  def set_holders
    pos = Position.find(@event.position_id) unless @event.position_id.blank?
    if !@event.position_id.blank? && !pos.to.blank?
      all_participants = Position.joins(:holder).where("positions.to IS NULL OR positions.id = #{pos.id}").order("CONCAT(holders.last_name,', ',holders.first_name)")
    else
      all_participants = Position.joins(:holder).where("positions.to IS NULL").order("CONCAT(holders.last_name,', ',holders.first_name)")
    end
    @participants = []
    all_participants.each do |position|
      @participants.push([position.holder.full_name_comma+' - '+position.title, position.id])
    end
    @holders = current_user.admin? ? all_participants : current_user.holders
    if current_user.admin? || current_user.lobby?
      all_position_permited = all_participants
    else
      all_position_permited = Position.joins(:holder).where("positions.to IS NULL").holders(current_user.id).order("CONCAT(holders.last_name,', ',holders.first_name)")
    end
    @positions = []
    all_position_permited.each do |position|
      @positions.push([position.holder.full_name_comma+' - '+position.title, position.id])
    end
  end

  def list_admin_events
    @events_unpagined = search(params)
    
  end

  def list_user_events
    params[:status] = ["requested", "declined"] unless params[:status].present?
    @events_unpagined = search(params).managed_by(current_user)
                   .includes(:position, :attachments, position: [:holder])
  end

  def set_event
    @event = Event.find(params[:id])
  end

  def find_holder_id_by_name(name)
    holder_ids = Holder.by_name(name).pluck(:id)
    Position.where("positions.holder_id IN (?)", holder_ids)
  end

  def enum_status(array_status)
    @status = array_status.map { |status| Event.statuses[status] }.join(' , ') if array_status.present?
  end

  def search(params)
    events = Event.all

    unless params[:search_person].blank?
      holders = Event.joins(:position => [:holder]).where("TRANSLATE(UPPER(CONCAT(holders.first_name,' ',holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')", "%#{params[:search_person]}%")
      participants =  Event.joins(:participants => [:position => [:holder]]).where("TRANSLATE(UPPER(CONCAT(holders.first_name,' ',holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')", "%#{params[:search_person]}%")

      events = events.where("events.id IN (?) OR events.id IN (?)", holders.select("events.id"),participants.select("events.id"))
    end

    events = events.where("events.status IN (?)", params[:status].map { |status| Event.statuses[status].to_i }) unless params[:status].blank?
    # unless params[:expired].blank?
    #   case params[:expired].to_s 
    #   when "old_event"
    #     events = events.joins(:organization).where("events.lobby_activity = true AND organizations.entity_type = 2 AND events.old_event_lobby = true")
    #   when "new_event"
    #     events = events.joins(:organization).where("events.lobby_activity = true AND organizations.entity_type = 2 AND organizations.renovation_date + interval '#{data_year} year' >= NOW() AND events.old_event_lobby = false")
    #   when "new_event_expired"
    #     events = events.joins(:organization).where("events.lobby_activity = true AND organizations.entity_type = 2 AND organizations.renovation_date + interval '#{data_year} year' < NOW() AND events.old_event_lobby = false")
    #   end
    # end
    
    if params[:search_start_date].present? && params[:search_end_date].present?
      events = events.where("cast(events.scheduled as date) BETWEEN cast(? as date)  AND cast(? as date)",Time.zone.parse(params[:search_start_date]),Time.zone.parse(params[:search_end_date]))
    elsif params[:search_start_date].present?
      events = events.where("cast(events.scheduled as date) >= cast(? as date) ",Time.zone.parse(params[:search_start_date]))
    elsif params[:search_end_date].present?
      events = events.where("cast(events.scheduled as date)  <= cast(? as date) ",Time.zone.parse(params[:search_end_date]))
    end
    
    if params[:search_start_date_published].present? && params[:search_end_date_published].present?
      events = events.where("cast(events.published_at as date) BETWEEN cast(? as date)  AND cast(? as date)",Date.parse(params[:search_start_date_published]),Date.parse(params[:search_end_date_published]))
    elsif params[:search_start_date_published].present?
      events = events.where("cast(events.published_at as date) >= cast(? as date) ",Date.parse(params[:search_start_date_published]))
    elsif params[:search_end_date_published].present?
      events = events.where("cast(events.published_at as date)  <= cast(? as date) ",Date.parse(params[:search_end_date_published]))
    end
    events = events.where("TRANSLATE(UPPER(events.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')", "%#{params[:search_title]}%") unless params[:search_title].blank?
    events = events.where("events.lobby_activity = ?", "1") unless params[:lobby_activity].blank?
    events = events.where("events.organization_id = ?", current_user.organization_id) if current_user.role == "lobby"

    events
  end

  def search_params(params)
    person = params[:search_person]
    title = params[:search_title]
    status = enum_status(params[:status])
    start_date= params[:search_start_date]
    end_date= params[:search_end_date]
    params_searches = {}
    params_searches[:title] = title unless title.blank?
    params_searches[:lobby_activity] = "1" unless params[:lobby_activity].blank?
    params_searches[:status] = status if status.present?
    params_searches[:start_date]= start_date unless start_date.blank?
    params_searches[:end_date]= end_date unless end_date.blank?
    params_searches[:position_id] = find_holder_id_by_name(person) if person.present?
    params_searches[:organization_id] = current_user.organization_id if current_user.role == "lobby"
    params_searches
  end

end
