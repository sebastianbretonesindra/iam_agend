class VisitorsController < ApplicationController
  require 'ext/string'
  before_filter :set_holder, only: [:index, :agenda]
  #before_action :get_calendar, only: [:index, :agenda]

  def index
    unless params[:reset].blank?
      params[:area]=""
      params[:keyword]=""
      params[:holder] = ""
      params[:lobby_activity] = ""
      params[:order] = ""
      params[:from] = ""
      params[:to] = ""
      @holder = nil
    end
    get_events_array
    @holders = get_holders_by_area(params[:area])
    begin
        respond_to do |format|
          format.csv { stream_csv_report.delay  }
          format.html
          format.atom
        end
    rescue
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @event = Event.friendly.find(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to visitors_path, alert: "No se puede acceder a esta información"
  end

  def agenda
    if @holder.blank?
      redirect_to visitors_path, alert: t('activerecord.models.holder.not_found')
    else
      index
      render :index
    end
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update_holders
    @holders = get_holders_by_area(params[:id])
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

 
  def stream_csv_report
    stream_file("Agendas", "csv") do |stream|
      Event.stream_query_rows(@unpaginated_events, request.base_url) do |row_from_db|
        stream.write row_from_db
      end
    end
  end

  def set_holder
    @holder = Holder.where(id: params[:holder]).first
  end

  def get_events_array
    events = Event.where("events.status in (1,2) AND (cast(events.published_at as date) <= cast(? as date) OR events.published_at IS NULL)", ActiveRecord::Base::sanitize(Time.zone.now.strftime("%Y-%m-%d").to_s))
    if !DataPreference.find_by(title: 'limit_events').try(:content_data).blank? && (current_user.blank? || !current_user.blank? && !current_user.admin? || !DataPreference.find_by(title: 'show_admin_limit').try(:content_data).blank? && DataPreference.find_by(title: 'show_admin_limit').try(:content_data) == "2")
      events = events.where("cast(events.scheduled as date) >= cast(? as date)", ActiveRecord::Base::sanitize(Time.zone.parse(DataPreference.find_by(title: 'limit_events').content_data)))
    end

    unless params[:holder].blank? 
      events = events.where("events.position_id in (?) OR events.id in (?)", Position.where(holder_id: params[:holder].to_i).select(:id),
        Participant.joins(:position).where("positions.holder_id = '?'", params[:holder].to_i).select(:event_id).select(:event_id))       
    end

    events = events.where(lobby_activity: true) unless params[:lobby_activity].blank?

    unless params[:keyword].to_s.strip.blank?
      events = events.where("
        TRANSLATE(UPPER(events.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') 
        OR TRANSLATE(UPPER(events.description),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
        OR events.position_id in (SELECT positions.id from positions where TRANSLATE(UPPER(positions.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU'))
        OR events.position_id in (SELECT positions.id from positions, holders where positions.holder_id=holders.id AND TRANSLATE(UPPER(CONCAT(holders.first_name, ' ', holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU'))
        OR events.position_id in (SELECT positions.id from positions, areas where positions.area_id=areas.id AND TRANSLATE(UPPER(areas.title),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU'))
        OR events.id in (SELECT participants.event_id from positions, holders, participants WHERE positions.id = participants.position_id AND holders.id = positions.holder_id AND TRANSLATE(UPPER(CONCAT(holders.first_name, ' ', holders.last_name)),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU'))
        OR events.id in (SELECT attendees.event_id from attendees WHERE  TRANSLATE(UPPER(attendees.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU'))
      ", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%", "%#{params[:keyword].to_s.strip}%")
    end

    if !params[:from].blank? && !params[:to].blank?
      events = events.where("CAST(events.scheduled as date) BETWEEN CAST(? as date) AND  CAST(? as date)", Time.zone.parse(params[:from]), Time.zone.parse(params[:to]))
    elsif !params[:from].blank?
      events = events.where("CAST(events.scheduled as date) >= CAST(? as date) ", Time.zone.parse(params[:from]))
    elsif !params[:to].blank?
      events = events.where("CAST(events.scheduled as date) <= CAST(? as date) ", Time.zone.parse(params[:to]))
    end
    

    if params[:order].blank? || params[:order] != 'score'
      events = events.order(scheduled: :desc) 
    elsif params[:order] == 'score'
      events = events.order(id: :desc)
    end

    @unpaginated_events = events
    @paginated_events = events.page(params[:page] || 1).per(100) 
  end


  def get_holders_by_area(area)
    if area.blank?
      holders_ids = Position.includes(:holder)
      .map {|position| position.holder }
    else
      holders_ids = Position.includes(:holder)
                            .area_filtered(area)
                            .map {|position| position.holder }
    end    
    Holder.where(id: holders_ids).order(last_name: :asc)
  end

  def ancestry_options(items)
    result = []
    items.map do |item, sub_items|
      result << [yield(item), item.id]
      result += ancestry_options(sub_items) {|i| "#{'-' * i.depth} #{i.title}" }
    end
    result
  end

  # def get_calendar
  #   @current_agend_calendar = !params[:set_date_calendar].blank? ? Time.zone.parse(params[:set_date_calendar]).to_date : Time.zone.now.to_date 
  #   @current_date_calendar = Time.zone.now.to_date
  #   if !params[:select].blank? && !params[:select][:month].blank? 
  #     @current_date_calendar = @current_date_calendar.change(month: params[:select][:month].to_i)
  #   end
  #   if !params[:select].blank? && !params[:select][:year].blank? 
  #     @current_date_calendar = @current_date_calendar.change(year: params[:select][:year].to_i)
  #   end

  #   if !params[:select_calendar].blank? && !params[:select_calendar][:month].blank? 
  #     @current_agend_calendar = @current_agend_calendar.change(month: params[:select_calendar][:month].to_i)
  #   end
  #   if !params[:select_calendar].blank? && !params[:select_calendar][:year].blank? 
  #     @current_agend_calendar = @current_agend_calendar.change(year: params[:select_calendar][:year].to_i)
  #   end
  #   if !DataPreference.find_by(title: "show_calendar").try(:content_data).blank? && params[:tab_calendar].blank?
  #     params.merge!({:tab_calendar => DataPreference.find_by(title: "show_calendar").try(:content_data), :set_date =>  @current_date_calendar.to_s})
  #   end
  # end

  
end
