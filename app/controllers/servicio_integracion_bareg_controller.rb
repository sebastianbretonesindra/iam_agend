
class ServicioIntegracionBaregController < ApplicationController

  def authenticate
  end

  soap_service namespace: "http://wsintbrg.bareg.iam.es", description: 'ServicioIntegracionBareg'
 
    soap_action 'inicioExpediente',
      args: {peticionIniciarExpediente: PeticionIniciarExpediente},
      args_description: { value: 'Servicio de peticion iniciar expediente' },
      return: {resultadoIniciarExpediente: ResultadoIniciarExpediente},
      response_tag: "inicioExpedienteResponse"
    def inicioExpediente
      Rails.logger.error("SOAP-INFO: #{params}")
      if params[:peticionIniciarExpediente].blank?
        #Sin parametros necesarios
        begin
          Rails.logger.error("SOAP: COD-2, Faltan parámetros")
        rescue
        end
        render soap: {resultadoIniciarExpediente: {codRetorno: "2", descError: "ERROR: Faltan parámetros", idExpediente: '', refExpediente: ''}}
      else
        sin_params = false;
        params[:peticionIniciarExpediente].each do |k,v|
          if v.blank? && k.to_s == ["codTipoExpediente", "xmlDatosEntrada","usuario"].include?(k.to_s)
            sin_params= true
            break
          end
        end
        if sin_params
          #Sin parámetros obligatorios
          begin
            Rails.logger.error("SOAP: COD-2, Faltan parámetros")
          rescue
          end
          render soap: {resultadoIniciarExpediente: {codRetorno: "2", descError: "ERROR: Faltan parámetros", idExpediente: '', refExpediente: ''}}
        else
          require 'json'
          require 'active_support/core_ext'

          data = Hash.from_xml(params[:peticionIniciarExpediente][:xmlDatosEntrada].blank? ? '' : params[:peticionIniciarExpediente][:xmlDatosEntrada].gsub("<![CDATA[",'').gsub("]]>",''))
          
          if !data.blank?      
            idformularios = data["borrador"].try {|x| x["formulario"]}
            idformularios = data["intercambioIAM"].try {|x| x["formulario"]} if idformularios.blank?
          else
            idformularios = []
          end

          verificacion_aux= false
          aux_tipo = Notification.get_cod_tipos_array[0][1]
          begin
            idformularios.each do |formulario|
              idformulario = formulario.try {|x| x["nombre"]}
              if !verificacion_aux && (Notification.get_cod_tipos[:insert].include?(params[:peticionIniciarExpediente][:codTipoExpediente].to_s) || Notification.get_cod_tipos[:insert].include?(idformulario.to_s))
                aux_tipo = Notification.get_cod_tipos_array[0][1]
                verificacion_aux = true 
              end
              if !verificacion_aux && (Notification.get_cod_tipos[:update].include?(params[:peticionIniciarExpediente][:codTipoExpediente].to_s) || Notification.get_cod_tipos[:update].include?(idformulario.to_s))
                aux_tipo = Notification.get_cod_tipos_array[1][1]   
                verificacion_aux = true 
              end
              if !verificacion_aux && (Notification.get_cod_tipos[:delete].include?(params[:peticionIniciarExpediente][:codTipoExpediente].to_s) || Notification.get_cod_tipos[:delete].include?(idformulario.to_s))
                aux_tipo = Notification.get_cod_tipos_array[2][1]
                verificacion_aux = true 
              end
              if !verificacion_aux && (Notification.get_cod_tipos[:reneue].include?(params[:peticionIniciarExpediente][:codTipoExpediente].to_s) || Notification.get_cod_tipos[:reneue].include?(idformulario.to_s))
                aux_tipo = Notification.get_cod_tipos_array[3][1]
                verificacion_aux = true 
              end
              
              if verificacion_aux
                break;
              end
            end
          rescue
          end
         
          notification = Notification.create!(status: Notification.get_statuses[:pending], change_status_at: Time.zone.now, origin_data: params,
            type_anotation: aux_tipo, modification_data: params)             
          begin
            Notification.set_errors_detected(notification)
          rescue
          end
          render soap: {resultadoIniciarExpediente: {codRetorno: "0", descError: "OK", idExpediente: notification.try(:id).to_s, refExpediente: notification.try(:id).to_s}}
        end
      end
    rescue => e
      begin
        Rails.logger.error("SOAP: COD-4, El servicio ha tenido el error: #{e}")
      rescue
      end
      render soap: {resultadoIniciarExpediente: {codRetorno: "4", descError: "ERROR: El servicio ha tenido el error: #{e}", idExpediente: '', refExpediente: ''}}
    end
    

    soap_action 'aporteDocumentacionExpediente',
      args: {peticionAportarDocumentacion: PeticionAportarDocumentacion}, 
      args_description: { value: 'Servicio de peticion iniciar expediente' },
      return: {resultadoAportarDocumentacion: ResultadoAportarDocumentacion},
      response_tag: "aporteDocumentacionExpedienteResponse"
    def aporteDocumentacionExpediente     
      if params[:PeticionAportarDocumentacion].blank?
        #Sin parametros necesarios
        render soap: {resultadoAportarDocumentacion: {codRetorno: "2", descError: "ERROR: Faltan parámetros"}}
      else
        sin_params = false;
        params[:PeticionAportarDocumentacion].each do |k,v|
          if v.blank? && k.to_s == ["idExpediente","refExpediente", "xmlDatosEntrada","usuario"].include?(k.to_s)
            sin_params= true
            break
          end
        end
        if sin_params
          #Sin parámetros obligatorios
          render soap: {resultadoAportarDocumentacion: {codRetorno: "2", descError: "ERROR: Faltan parámetros"}}
        else
          require 'json'
          require 'active_support/core_ext'


          Notification.create(status: Notification.get_statuses[:pending], change_status_at: Time.zone.now, origin_data: params)
          render soap: {resultadoIniciarExpediente: {codRetorno: "0", descError: "OK", idExpediente: '0', refExpediente: '0'}}          
        end
      end
    rescue => e
      render soap: {resultadoIniciarExpediente: {codRetorno: "4", descError: "ERROR: El servicio ha tenido el error: #{e}"}}
    end 

end
