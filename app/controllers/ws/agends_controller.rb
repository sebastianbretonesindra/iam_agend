class Ws::AgendsController < ApplicationController
    respond_to :json
    protect_from_forgery unless: -> { request.format.json? }

    before_action :checktokenUser
    before_action :authenticate!, unless: 'user_authenticated?', except: [:generic, :holder]

    # POST /ws/agends
    # ENTRADA => {api, api_password, start_date, end_date, page, elements_per_page}
    # SALIDA => [{id,slug,title,description,scheduled,lobby_activity,holder_id,participants_ids,position_title,area_id,holder_name,personal_code,participants_name,attendees_name,area_title}]
    def generic
        if @data_permit.to_s == "true"
            permit_params = params.permit(:start_date,:end_date,:page, :elements_per_page)
            
            events = get_events(permit_params)  
            success({count: get_count_events(permit_params), page: permit_params[:page], elements_page: permit_params[:elements_per_page], events: events })
        else
            error("error01")
        end
    rescue => e
        error("error02", e)
    end

    # POST /ws/agends/:personal_code
    # ENTRADA => {api, api_password, personal_code, start_date, end_date, page, elements_per_page}
    # SALIDA => [{id,slug,title,description,scheduled,lobby_activity,holder_id,participants_ids,position_title,area_id,holder_name,personal_code,participants_name,attendees_name,area_title}]
    def holder
        if @data_permit.to_s == "true"
            permit_params = params.permit(:start_date,:end_date,:personal_code, :page, :elements_per_page)

            if permit_params[:personal_code].try{|x| x.strip}.blank?
                error("error03")
            else
                events = get_events(permit_params)                
                success({personal_code: permit_params[:personal_code],count: get_count_events(permit_params), page: permit_params[:page], elements_page: permit_params[:elements_per_page], events: events })
            end
        else
            error("error01")
        end
    rescue => e
        error("error02", e)
    end

    protected

    def error(error, message="", status = :error)
        json_response({:error => {mensaje: "#{t("ws_agends.errors.#{error}")}: #{message}", codigo: t("ws_agends.cod_errors.#{error}")}}, status)
    end

    def success(message)
        json_response(message)
    end

    def json_response(message, status = :ok)
        begin
            if status.to_s != "ok"
                Rails.logger.error("#{status.to_s == "ok" ? "INFO-WS: " : "ERROR-WS: "}#{message}")
            end
        rescue
        end
        render json: message, status: status
    end

    def checktokenUser
        permit_params = params.permit(:api,:api_password)
        if !permit_params[:api].blank? && !permit_params[:api_password].blank?
            if Rails.application.secrets.user_trape == permit_params[:api] && Rails.application.secrets.pass_trape == permit_params[:api_password]
                @data_permit = true
            else
                @data_permit = false
                error("error01")
            end
        else
            error("error01")
        end
    end

    def get_events(parametrize)       
        events = ActiveRecord::Base.connection.exec_query("
            SELECT events.id as \"id\", events.slug as \"slug\", events.title as \"title\", events.description as \"description\",
              events.scheduled as \"scheduled\", events.lobby_activity as \"lobby_activity\",
              (SELECT positions.holder_id FROM positions WHERE positions.id = events.position_id) as \"holder_id\",
              (SELECT string_agg(CAST(positions.holder_id as varchar),';') FROM positions, participants WHERE positions.id = participants.position_id AND participants.event_id = events.id) as \"participants_ids\",
              (SELECT positions.title FROM positions WHERE positions.id = events.position_id) as \"position_title\",
              (SELECT positions.area_id FROM positions WHERE positions.id = events.position_id) as \"area_id\",
              (SELECT UPPER(CONCAT(holders.first_name, ' ', holders.last_name)) FROM positions, holders WHERE positions.id = events.position_id AND holders.id = positions.holder_id) as \"holder_name\",
              (SELECT holders.personal_code FROM positions, holders WHERE positions.id = events.position_id AND holders.id = positions.holder_id) as \"personal_code\",
              (SELECT string_agg(UPPER(CONCAT(holders.first_name, ' ', holders.last_name)),';') FROM positions, holders, participants WHERE positions.id = participants.position_id AND participants.event_id = events.id AND holders.id = positions.holder_id) as \"participants_name\",
              (SELECT string_agg(UPPER(attendees.name),';') FROM attendees WHERE attendees.event_id = events.id) as \"attendees_name\",
              (SELECT areas.title FROM positions, areas WHERE positions.id = events.position_id AND areas.id=positions.area_id) as \"area_title\"
              from events #{get_sql_common(parametrize) } 
              #{" limit #{parametrize[:elements_per_page].to_i} offset #{((parametrize[:page].blank? ? 1 : parametrize[:page].to_i) - 1) * parametrize[:elements_per_page].to_i}" if !parametrize[:elements_per_page].blank?} "
            ) 
        events.to_a
    end

    def get_count_events(parametrize)
        events = ActiveRecord::Base.connection.exec_query(" SELECT count(events.id) as \"count\" from events #{get_sql_common(parametrize)}") 
        events.to_a.first
    end

    def get_sql_common(parametrize)
        sql_common = ""
        if !parametrize[:start_date].blank? && !parametrize[:end_date].blank?
            sql_common = sql_common + (sql_common.blank? ? " WHERE " : " AND ") + " CAST(events.scheduled as date) BETWEEN CAST(#{ActiveRecord::Base::sanitize(Time.zone.parse(parametrize[:start_date]))} as date) AND  CAST(#{ActiveRecord::Base::sanitize(Time.zone.parse(parametrize[:end_date]))} as date)"
        elsif !parametrize[:start_date].blank?
            sql_common = sql_common + (sql_common.blank? ? " WHERE " : " AND ") + " CAST(events.scheduled as date) >= CAST(#{ActiveRecord::Base::sanitize(Time.zone.parse(parametrize[:start_date]))} as date) "
        elsif !parametrize[:end_date].blank?
            sql_common = sql_common + (sql_common.blank? ? " WHERE " : " AND ") + " CAST(events.scheduled as date) <= CAST(#{ActiveRecord::Base::sanitize(Time.zone.parse(parametrize[:end_date]))} as date) "
        end

        sql_common = sql_common + (sql_common.blank? ? " WHERE " : " AND ") + " events.position_id in (SELECT positions.id FROM positions, holders WHERE holders.id = positions.holder_id AND holders.personal_code = #{ActiveRecord::Base::sanitize(parametrize[:personal_code])})" if !parametrize[:personal_code].blank?

        sql_common
    end
end