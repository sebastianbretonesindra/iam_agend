class QuestionsController < ApplicationController

  layout 'faq'

  def index
    @questions = Question.all.order(:position)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
