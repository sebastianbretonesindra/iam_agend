class ActivitiesController < AdminController
  #load_and_authorize_resource
  #before_filter(:only => :index) { authorize! if can? :index, :activities }

  def index
    authorize! :index, :activities
    @activities = PublicActivity::Activity.order(updated_at: :desc).page(params[:page]).per(100)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
