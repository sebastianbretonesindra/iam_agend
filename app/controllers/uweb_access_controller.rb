class UwebAccessController < ApplicationController

  def uweb_sign_in
    uweb_api = UwebApi.new
    app_status =  uweb_api.get_application_status
    baja_logica = uweb_api.get_user_status(params[:clave_usuario], params[:fecha_conexion])


    if valid_referer?(request.referer) && valid_params?(params) &&
      !app_status.blank? && app_status.zero?  &&
      !baja_logica.blank? && baja_logica.zero?  &&
      user = User.find_by(user_key: params[:clave_usuario])

      sign_in(:user, user)
      redirect_to events_home_path(user, true)
    else
      redirect_to root_path
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

    def valid_params?(params)
      params['fecha_conexion'].present? &&
      params['clave_usuario'].present? &&
      params['clave_aplica'].present?
    end

    def valid_referer?(referer)
      #TODO: remove in order to ensure request is comming from Aire
      return true
      #referer.include? Rails.application.secrets.uweb_referer_url
    end

end