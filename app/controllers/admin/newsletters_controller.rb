module Admin
  class NewslettersController < AdminController
    load_and_authorize_resource
    before_action :load_options
    before_action :load_newsletter, only: [:show,:edit,:update,:destroy,:deliver]


    def index
      @newsletters = Newsletter.all
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show
      @recipients_count = @newsletter.list_of_recipient_emails.count
      @destinataries = @newsletter.hash_of_recipient_emails
      @title= @newsletter.draft? ? I18n.t("backend.newsletters.show.title") : I18n.t("backend.newsletters.show.title_after")
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def new
      @newsletter = Newsletter.new
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def create
      aux = newsletter_params
      aux[:interest_ids] = aux[:interest_ids].reject(&:blank?)

    
      @newsletter = Newsletter.new(aux)
      @newsletter.interest_id = aux[:interest_ids].try(:first)

      if @newsletter.save
        notice = t("backend.newsletters.create_success")
        redirect_to [:admin, @newsletter], notice: notice
      else
        render :new
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def edit
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update   
      aux = newsletter_params      
      aux[:interest_ids] = aux[:interest_ids].reject(&:blank?)
      @newsletter.interest_id = aux[:interest_ids].try(:first) if @newsletter.interest_id.blank?
      if @newsletter.update(aux)
        redirect_to [:admin, @newsletter], notice: t("backend.newsletters.update_success")
      else
        render :edit
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def destroy
      @newsletter.destroy

      redirect_to admin_newsletters_path, notice: t("backend.newsletters.delete_success")
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def deliver
      if @newsletter.valid?
        @newsletter.deliver
        @newsletter.update(sent_at: Time.current)
        flash[:notice] = t("backend.newsletters.send_success")
      else
        flash[:error] = t("admin.segment_recipient.invalid_recipients_segment")
      end

      redirect_to [:admin, @newsletter]
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    private
      def load_newsletter
        @newsletter = Newsletter.find(params[:id])
      end

      def newsletter_params
        params.require(:newsletter).permit(:subject, :segment_recipient, :from, :body, { interest_ids: [] },)
      end

      def load_options
        @hash_interest={}
        Interest.all.where("name != 'Otros'").order(:name).each do |interest|
          @hash_interest.merge!({interest.name=>interest.id}) if interest.id != 0
        end
        @hash_interest.merge!({Interest.find_by(name: "Otros").name=>Interest.find_by(name: "Otros").id})
      end
  end
end
