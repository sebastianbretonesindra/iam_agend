class Admin::ParametrizationsController < AdminController
        load_and_authorize_resource
        before_action :load_data, only: [:index]

        def index
            @mails = ManageEmail.all
            @data_preferences = DataPreference.all.order(created_at: :asc)            
            @dp = SendEmail.all.first
            @dp = SendEmail.new() if @dp.blank?
            @errors = params[:errors] if !params[:errors].blank?
            params.delete :errors         
        rescue => e
            begin
              Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end  
        end

        def change_view
            if !params[:user].blank? && params[:user][:role_view] == "Gestor"
                current_user.role = 'user'
                current_user.role_view = 0
            end
            if !params[:user].blank? && params[:user][:role_view] == "Lobby"
                current_user.role = 'lobby'
                current_user.role_view = 2
                current_user.organization = Organization.lobbies.first
            end
            current_user.save
            
            redirect_to admin_path, notice: t('backend.change_view_success')
        rescue => e
            begin
              Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def length_event_description
            dp = DataPreference.all
            params[:data_preference] ||={}
            params[:data_preference].each do |k,v|
                d = dp.find_by(title: k.to_s)
                if !d.blank? && d.content_data.to_s != v.to_s
                    d.content_data = v.to_s
                    if d.save
                        `whenever -i cellar`
                    end
                end                
            end
            
            redirect_to admin_parametrizations_path(data_preferences: true), notice: "Parametros actualizados"
        rescue => e
            begin
              Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def send_email            
            @dp = SendEmail.all.first
            @dp = SendEmail.new() if @dp.blank?

            if @dp.update(send_email_valid_params)
                redirect_to admin_parametrizations_path(send_emails: true), notice: "Parametros actualizados"
            else
                flash[:alert] = t('backend.review_errors')
                redirect_to admin_parametrizations_path(send_emails: true, errors: @dp.errors.full_messages), method: :post
            end
        rescue => e
            begin
              Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        
        private

        def send_email_valid_params
            params.require(:send_email).permit(:auto_send, :auto_date, :auto_hour, :auto_frecuence, :auto_user_type,
                                                 :auto_excluded_emails, :manual_send, :manual_from_date, :manual_to_date,
                                                  :manual_hour, :manual_user_type, :manual_excluded_emails)
        end

        def load_data
            @roles = ["Gestor", "Lobby"]
            @time_frames =[["Mensual", 1],["Trimestral", 2],["Anual", 3]]
            @selected_calendar =[["Día", 'day'],["Semana", 'week'],["Mes", 'month'],["Todo", 'all']]
            @limit_values = {"Todos" => "1", "Limitados" => "2"}
            @frecuences = [["Mensual", 1],["Bimestral", 2],["Trimestral", 3],["Semestral", 4], ["Anual", 5]]
            @user_types = {"Titulares" => 1, "Gestores" => 2, "Ambos" => 3}
        end
end
