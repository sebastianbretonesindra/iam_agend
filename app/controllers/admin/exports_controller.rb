class Admin::ExportsController < AdminController

        load_and_authorize_resource :export

        def index
            @exports = Export.all.order(id: :desc)
        rescue => e
            begin
              Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
        
        def new
            @export = Export.new
        rescue => e
            begin
              Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def create 
            col_valid = false
            export_params[:fields].each do |k,v|
                col_valid = true if v.to_s == "1"
            end
            
            if col_valid == true
                @export = Export.new(export_params) 
                if @export.save
                redirect_to admin_exports_path,
                            notice: t('backend.successfully_created_export')
                else
                    flash[:alert] = t('backend.review_fields')
                    render :new
                end
            else
                flash[:alert] = t('backend.review_fields')
                render :new
            end
        rescue => e
            begin
              Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def show 
            @export = Export.find(params[:id])
            respond_to do |format|
                format.csv { stream_csv_report  }
                format.html
            end
        rescue => e
            begin
              Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def edit
        rescue => e
            begin
              Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def update
            if @export.update(export_params)
              redirect_to admin_exports_path, notice: t('backend.successfully_updated_record')
            else
              flash[:alert] = t('backend.review_errors')
              render :edit
            end
        rescue => e
            begin
              Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        def destroy
            @export = Export.find(params[:id])
            if @export.destroy
                redirect_to admin_exports_path,
                            notice: t('backend.successfully_destroyed_export')
            else
                flash[:alert] = t('backend.unable_to_perform_operation')
                redirect_to admin_exports_path
            end
        rescue => e
            begin
              Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end

        

        private
            def stream_csv_report
                query = @export
                filename = !@export.name.blank? ? @export.name.gsub(" ", "_") : t('backend.exports.filename')
                stream_file(filename, "csv") do |stream|
                    Export.stream_query_rows_specific(query) do |row_from_db|
                        stream.write row_from_db
                    end
                end
            end

            def export_params
                params.require(:export).permit(:name,
                {
                :fields => [
                    :scheduled_field, :title_field, :position_id_field, :job_field, :location_field, :description_field,
                    :status_field, :holders_field, :other_attendees_field, :created_at_field, :published_at_field, 
                    :slug_field, :accepted_at_field, :canceled_at_field, :canceled_reasons_field, :declined_at_field,
                    :declined_reasons_field, :organization_name_field, :lobby_activity_field, :represented_entities_field, 
                    :lobby_agent_field, :lobby_contact_firstname_field, :lobby_contact_lastname_field, :lobby_contact_email_field, 
                    :lobby_contact_phone_field, :lobby_scheduled_field, :notes_field, :user_id_field, :updated_at_field,
                    :general_remarks_field, :lobby_expired_field
                    ],
                :filter_fields => [
                    :title, :people, :start_date, :end_date, :start_date_published, :end_date_published, status: []
                    ]}
                )
            end
 
end
