module Admin
  class PasswordsController < AdminController

    layout "admin"

    def edit
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update
      if current_user.update(password_params)
        current_user.frist_init = false
        current_user.changed_password = false
        current_user.save
        bypass_sign_in(current_user)
        flash.now[:notice] = t("admin.passwords.update.notice")
      else
        flash.now[:alert] = t("admin.passwords.update.alert")
      end
      render :edit
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    protected

      def password_params
        params.require(:user).permit(:password, :password_confirmation)
      end

  end
end
