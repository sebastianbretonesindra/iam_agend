class RepresentedEntitiesController < AdminController
  respond_to :json

  def index
    @represented_entities = RepresentedEntity.select('id', 'name', 'first_surname', 'second_surname').where(to: nil).by_organization(params[:organization_id]).order(:name)
    render :json => @represented_entities
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
