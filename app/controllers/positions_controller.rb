class PositionsController < AdminController

  autocomplete :position, :title, display_value: :full_name

  def get_autocomplete_items(parameters)
    Position.current.full_like("%#{parameters[:term]}%")
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

end
