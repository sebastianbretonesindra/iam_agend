class AgentsController < AdminController
  respond_to :json

  def index
    @agents = Agent.active.select('id', 'name', 'first_surname', 'second_surname').by_organization(params[:organization_id]).order(:name)
    render :json => @agents
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
