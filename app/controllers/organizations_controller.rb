class OrganizationsController < ApplicationController

  before_action :set_organization, only: [:show,:edit,:destroy]
  before_action :load_options
  before_action :browser

  autocomplete :organization, :name, :display_value => :fullname,:extra_data => [:id]

  def index
    search(params)
   # @organizations = Organization.where(id: @organizations.hits.map(&:primary_key))
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: [@organization.category.name, @organization.entity_type] }
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def new
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def destroy
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private
  
    def search(params)
      @organizations = Organization.lobbies

      if params[:interests].present?
        @organizations = @organizations.where("id in (?)", OrganizationInterest.where("interest_id in (?)", params[:interests]).select(:organization_id))
      end

      if params[:category].present?
        @organizations = @organizations.where(category_id: params[:category])
      end

      if params[:lobby_activity].present?
        @organizations = @organizations.where("id in (?)", Event.where(status: 2, lobby_activity: true).select(:organization_id))
      end

      if params[:keyword].present?
        @organizations = @organizations.where(
          "TRANSLATE(UPPER(organizations.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU') 
          OR TRANSLATE(UPPER(organizations.business_name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(organizations.first_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(organizations.second_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(organizations.description),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR organizations.id in (?)",
          "%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%","%#{params[:keyword]}%",
          Agent.where("TRANSLATE(UPPER(agents.name),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(agents.first_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')
          OR TRANSLATE(UPPER(agents.second_surname),'ÁÉÍÓÚ','AEIOU') LIKE TRANSLATE(UPPER(?),'ÁÉÍÓÚ','AEIOU')",
          "%#{params[:keyword]}%", "%#{params[:keyword]}%", "%#{params[:keyword]}%").select(:organization_id)
        )
      end

      begin
        if !params[:status_type].blank?
          aux_status = ""
          time = Time.zone.now - data_year.year
          case params[:status_type]
          when '1'
            @organizations = @organizations.select{ |organization| (organization.expired_date.blank? || !organization.expired_date.blank? && organization.expired_date >= Time.zone.now ) && !organization.canceled? && !organization.invalidated?}
          when '3'
            @organizations = @organizations.select{ |organization| (organization.expired_date.blank? || !organization.expired_date.blank? && organization.expired_date >= Time.zone.now ) && organization.canceled? || organization.invalidated?}
          when '4'
            @organizations = @organizations.select{ |organization| !organization.expired_date.blank? && organization.expired_date < Time.zone.now && !organization.canceled? && !organization.invalidated?}    
          end

        end
      rescue
      end


      @paginated_organizations = Kaminari.paginate_array(sorting_option(params[:order].blank? ? '4' : params[:order], @organizations)).page(params[:format].present? ? 1 : params[:page] || 1).per(params[:format].present? ? 1000 : 100)
    end

    def set_organization
      @organization = Organization.find(params[:id])
    end

    def get_autocomplete_items(parameters)
      Organization.where(entity_type: 2).full_like("%#{parameters[:term]}%")
    end

    def sorting_option(option, organizations)
      case option
      when '1'
        @organizations.sort_by{|o| o.fullname}
      when '2'
        @organizations.sort_by{|o| o.fullname}.reverse
      when '3'
        @organizations.sort_by{|o| o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}
      when '4'
        @organizations.sort_by{|o|  o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date}.reverse
      when '5'
        @organizations.sort_by{|o| o.try(:renovation_date).blank? ? Time.zone.parse('1970-01-01') : o.renovation_date}
      when '6'
        @organizations.sort_by{|o| o.try(:renovation_date).blank? ? Time.zone.parse('1970-01-01') : o.renovation_date}.reverse
      else
        @organizations.sort_by{|o| [o.fullname, o.description,  o.try(:inscription_date).blank? ? Time.zone.parse('1970-01-01') : o.inscription_date]}.reverse
      end
    rescue
      @organizations
    end

    def load_options
      @hash_interest={}
      Interest.all.where("name != 'Otros'").order(:name).each do |interest|
        @hash_interest.merge!({interest.name.mb_chars=>interest.id})
      end
      @hash_interest.merge!({Interest.find_by(name: "Otros").name.mb_chars=>Interest.find_by(name: "Otros").id})
    end

end
